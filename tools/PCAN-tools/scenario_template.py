'''
Use this file as a skeleton to create new test scenarios.
'''
import importlib
import signal

from general import get_test_case_settings


def descriptive_scenario_name(version_arg):
    # Initializes test scenario. Selects correct mapping and DBC files. Also sets exit handler.
    test_info = get_test_case_settings(sys.argv[1])
    RT = importlib.import_module(test_info['utils'])
    tester = RT.ReliabilityTester(test_info['DBC'])
    signal.signal(signal.SIGINT, tester.signal_handler)

    # Uses Yoctopuce to UNLOCK then VPM ON
    tester.vehicle_power_management_on()

    # Begin writing scenario here...
    """
    If looping values in the background of the test scenario, such as speed, odometer, and range, create a dictionary of 
    keys (function names) and values (function parameters) then pass the dictionary to the multithread function.
    
    Example:
    primary_loops = {tester.loop_odometer: (0, 1000000, 1000, 8,), etc..}
    tester.multithread(primary_loops)
    """

    # Continue scenario after loops if required
    """
    NOTE: To execute commands in this section, 'join' argument in multithread function above must be set to False.
    
    Create a while loop dependent on the tester.stop_device_test value and run any commands or signals needed 
    for specific scenarios.
    
    Example:
    while not tester.stop_device_test:
        tester.flashing_left_turn_signal(10)
    """


if __name__ == '__main__':
    import sys

    descriptive_scenario_name(sys.argv[1])
