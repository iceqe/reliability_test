import logging
import random
import sys
import threading
import time
from binascii import hexlify
from time import sleep

import cantools

import pcanoperations as pcan

sys.path.append('../utility/Keyfob')
import keyfob_utils as kf


class ReliabilityTester:
    def __init__(self, path):
        logging.basicConfig(filename='logs/APReliabilityTester_' + time.strftime("%Y%m%d-%H%M%S") + '.log',
                            level=logging.DEBUG)
        self.failed = False
        self.stop_device_test = False
        self.pcan_bus = pcan.PCANKeywords()
        self.pcan_bus.pcan_create('usbbus1', '500k', 'message_filter')

        # self.body2 = cantools.database.load_file(path + 'Body2.dbc')
        # self.thermal = cantools.database.load_file(path + 'Thermal.dbc')
        # self.edrive = cantools.database.load_file(path + 'eDrive.dbc')

        self.body1 = cantools.database.load_file(path + 'Body1.dbc')
        self.chassis1 = cantools.database.load_file(path + 'Chassis1.dbc')
        self.chassis2 = cantools.database.load_file(path + 'Chassis2.dbc')

        logging.info('Tester successfully initialized.')

    def signal_handler(self, signal, frame):
        """
        Exit handler.
        :param signal:
        :param frame:
        :return:
        """
        print('Ctrl+C, exiting...')
        self.stop_device_test = True
        sleep(3)
        exit(1)

    def select_dbc(self, domain):
        """
        Select correct DBC file based on given domain.

        :param domain: String representing the name of a valid domain (ie. body1, BODY2, ChasSiS2, etc.)

        :return: Appropriate database loaded for PCAN message execution
        """
        if domain.upper() == 'BODY1':
            return self.body1
        elif domain.upper() == 'BODY2':
            return self.body2
        elif domain.upper() == 'CHASSIS1':
            return self.chassis1
        elif domain.upper() == 'CHASSIS2':
            return self.chassis2
        elif domain.upper() == 'THERMAL':
            return self.thermal
        elif domain.upper() == 'EDRIVE':
            return self.edrive
        else:
            print "Not a valid domain"
            return None

    def get_pcan_message(self, domain, message_name, **kwargs):
        """
        Generates PCAN message with given values to send to bus.

        :param domain: String representing domain of message.
        :param message_name: Name of the desired message as it appears in the DBC files.
        :param kwargs: A dictionary of singal name to set value (ie. {'turnIndicator_Lt_FBCM': 1}

        :return: Encoded PCAN message, ready to send.
        """
        can_msg_tmpl = {}
        dbc = self.select_dbc(domain)
        can_msg = dbc.get_message_by_name(message_name)
        for sig in can_msg.signals:
            try:
                choice_key = sig.choices.keys()[-1]
                if sig.unit == '-' and sig.minimum < choice_key < sig.maximum:
                    can_msg_tmpl[sig.name] = choice_key
                else:
                    if sig.minimum is None:
                        raise ValueError
                    can_msg_tmpl[sig.name] = sig.minimum
            except Exception:
                if not sig.minimum:
                    if isinstance(sig.scale, float):
                        can_msg_tmpl[sig.name] = 0.0
                    else:
                        can_msg_tmpl[sig.name] = 0
                else:
                    can_msg_tmpl[sig.name] = sig.minimum
        for key, val in kwargs.items():
            target_type = type(can_msg_tmpl[key])
            can_msg_tmpl[key] = target_type(val)
        encoded_can_message = hexlify(dbc.encode_message(message_name, can_msg_tmpl))
        arb_id = hex(can_msg.frame_id)[2:].upper()
        message = ['0x' + arb_id, "PCAN_MESSAGE_STANDARD", str(len(encoded_can_message) / 2)]
        data = ["0x" + encoded_can_message[i:i + 2] for i in range(0, len(encoded_can_message), 2)]
        message.extend(data)
        return message

    def check_failure(self):
        """
        Check to see if any messages failed
        :return: Boolean
        """
        return self.failed

    def multithread(self, function_dict, join=True):  # {function_name: (arg1, arg2, agr3,)}
        """
        Multithread a set of given functions with their parameters. Avoid multithreading with the same signals/functions.

        :param function_dict: Dictionary of function name to arguments (ie. {'loop_speedometer': (0, 360, 1, 0.1,)
        :param join: Set to FALSE if parent thread should continue to execute while child threads run in background. (Default: True)

        :return: No return, just starts threads.
        """
        threads = []
        for func, vals in function_dict.iteritems():
            logging.info('Adding thread')
            t = threading.Thread(target=func, args=vals)
            threads.append(t)
        for t in threads:
            logging.info('Starting Thread')
            t.daemon = True
            t.start()
        if join:
            for t in threads:
                t.join()
            logging.info('All Threads Joined')

    def vehicle_power_management_on(self):
        """
        Set VPH to ON mode.

        :return: No return, just turns VPH to ON.
        """
        logging.info('Beginning VPM ON sequence.')
        try:
            kf.keyfob_button("unlock")
            self.set_front_doors(1)
            sleep(3)
            self.set_hv_enable(1)
            sleep(3)
            self.set_gear('Drive')
            sleep(3)
            self.set_front_doors(0)
            sleep(3)
            self.set_speedometer(5)
            logging.info('VPM ON Completed')
        except AttributeError:
            logging.warning(
                "Yoctopuce not connected, please check library is installed and board is connected to machine.")
            self.failed = True

    def set_hv_enable(self, on_off):
        """
        Enable or Disable High Voltage Status.

        :param on_off: Integer (1 or 0) to set HV status on or off.

        :return: No return, just sends the message.
        """
        logging.info('Setting HV value to %d' % on_off)
        data = {}
        if on_off:
            data['highVoltageStatus_CDI'] = 4
        else:
            data['highVoltageStatus_CDI'] = 1
        try:
            hv_msg = self.get_pcan_message('Chassis2', 'CDI_HVsystemActiveStatus', **data)
            self.pcan_bus.pcan_write(hv_msg)
            logging.info('HV enabled successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_gear(self, gear):
        """
        Set vehicle gear

        :param gear: STRING input to select vehicle gear. (* Not handled)
        * = Initializing
        P = Park
        R = Reverse
        N = Neutral
        D = Drive
        * = Reserved
        * =  Error


        :return: No return, just sends the message.
        """
        logging.info('Setting gear value to %s' % gear.upper())
        data = {}
        if gear.upper() == 'PARK' or gear.upper() == 'P':
            data['powerTrainActualGearStatus_CDI'] = 1
        elif gear.upper() == 'REVERSE' or gear.upper() == 'R':
            data['powerTrainActualGearStatus_CDI'] = 2
        elif gear.upper() == 'NEUTRAL' or gear.upper() == 'N':
            data['powerTrainActualGearStatus_CDI'] = 3
        elif gear.upper() == 'DRIVE' or gear.upper() == 'D':
            data['powerTrainActualGearStatus_CDI'] = 4
        else:
            print 'Invalid selection, choose from Park, Reverse, Neutral, Drive.'
        try:
            gear_msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainDriveStatus', **data)
            self.pcan_bus.pcan_write(gear_msg)
            logging.info('Speed set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_turn_signal(self, side, on_off):
        """
        Activates turn signal for left or right side.

        :param side: STRING representing which side to manipulate. (ie. 'Left' or 'Right')
        :param on_off: INTEGER to turn ON (1) or OFF (0).

        :return: No return, just sends the message.
        """
        logging.info('Setting %s turn signal value to %d' % (side.upper(), on_off))
        data = {}
        if side.upper() == 'LEFT' and on_off:
            data['turnIndicatorLt_FBCM'] = 1
        elif side.upper() == 'LEFT' and not on_off:
            data['turnIndicatorLt_FBCM'] = 0
        elif side.upper() == 'RIGHT' and on_off:
            data['turnIndicatorRt_FBCM'] = 1
        elif side.upper() == 'RIGHT' and not on_off:
            data['turnIndicatorRt_FBCM'] = 0
        else:
            print "Invalid choice, Choose Left or Right and 1 or 0"
        try:
            turn_msg = self.get_pcan_message('Body1', 'FBCM_VehInfo1', **data)
            self.pcan_bus.pcan_write(turn_msg)
            logging.info('Turn Signal set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def flashing_left_turn_signal(self, count):
        """
        Flash LEFT turn signal with standard delay of 400ms on, 400ms off.

        :param count: INTEGER representing how many times to have the turn signal flash.

        :return: No return, just sends the message.
        """
        logging.info('Flashing left turn signal for %d time(s)' % count)
        for i in xrange(count):
            self.set_turn_signal('left', 1)
            sleep(0.4)
            self.set_turn_signal('left', 0)
            sleep(0.4)

    def flashing_right_turn_signal(self, count):
        """
        Flash RIGHT turn signal with standard delay of 400ms on, 400ms off.

        :param count: INTEGER representing how many times to have the turn signal flash.

        :return: No return, just sends the message.
        """
        logging.info('Flashing right turn signal for %d time(s)' % count)
        for i in xrange(count):
            self.set_turn_signal('right', 1)
            sleep(0.4)
            self.set_turn_signal('right', 0)
            sleep(0.4)

    def set_headlights(self, h_or_l, on_off):
        """
        Turn on or off the high or low beam headlights.

        :param h_or_l: STRING to select HIGH or LOW beam headlight.
        :param on_off: INTEGER to turn selected headlight ON (1) or OFF (0).

        :return: No return, just sends the message.
        """
        logging.info('Setting %s headlight value to %d' % (h_or_l.upper(), on_off))
        sig = ''
        if h_or_l.upper() == "H" or h_or_l.upper() == "HIGH":
            sig = 'highBeamOn_FBCM'
        elif h_or_l.upper() == "L" or h_or_l.upper() == "LOW":
            sig = 'lowBeamOn_FBCM'
        data = {sig: on_off}
        try:
            headlight_msg = self.get_pcan_message('Body1', 'FBCM_VehInfo1', **data)
            self.pcan_bus.pcan_write(headlight_msg)
            logging.info('Headlight set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_speedometer(self, km_val):
        """
        Set speedometer value.

        :param km_val: Pass speedometer value in KPH, gets converted to M/S.

        :return: No return, just sends the message.
        """
        logging.info('Setting speed value to %d' % km_val)
        data = {'vehSpeed_ESP': min(km_val * 0.277777, 100)}
        try:
            speedometer_msg = self.get_pcan_message('Chassis2', 'ESP_iBoostData1', **data)
            self.pcan_bus.pcan_write(speedometer_msg)
            logging.info('Speed set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_odometer(self, km_val):
        """
        Set odometer value.

        :param km_val: Pass odometer value in KM.

        :return: No return, just sends the message.
        """
        logging.info('Setting odometer value to %d' % km_val)
        data = {'powerTrainHMIOdometerInfo_CDI': km_val}
        try:
            odo_msg = self.get_pcan_message('Chassis2', 'CDI_OdometerInfo', **data)
            self.pcan_bus.pcan_write(odo_msg)
            logging.info('Odometer set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_percent_charged(self, charged):
        """
        Set battery percent charged value.

        :param charged: Percent charged value.

        :return: No return, just sends the message.
        """
        logging.info('Setting percent charged value to %d' % charged)
        data = {'bat12vStateOfChargeIBSInfo_CDI': charged}
        try:
            range_msg = self.get_pcan_message('Chassis2', 'CDI_IBSstatus2', **data)
            self.pcan_bus.pcan_write(range_msg)
            logging.info('Range set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_range(self, veh_range):
        """
        Set battery range value.

        :param veh_range: Pass range value in KM.

        :return: No return, just sends the message.
        """
        logging.info('Setting range value to %d' % veh_range)
        data = {'powerTrainHMIAvaDrvRange_CDI': veh_range}
        try:
            range_msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHVbattStatus1', **data)
            self.pcan_bus.pcan_write(range_msg)
            logging.info('Range set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_battery(self, battery_percent):
        """
        Set battery charge percentage icon.

        :param battery_percent: Pass battery life INTEGER value in factor of 100.
        (ie. empty is 0, quarter is 25, half full is 50, full is 100)

        :return: No return, just sends the message.
        """
        logging.info('Setting battery value to %d' % battery_percent)
        data = {'bat12vStateOfHealthIBSInfo_CDI': battery_percent}
        try:
            range_msg = self.get_pcan_message('Chassis2', 'CDI_IBSstatus2', **data)
            self.pcan_bus.pcan_write(range_msg)
            logging.info('Battery set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_range_and_battery(self, veh_range):
        """
        Combination of battery and range functions to tie the two elements together.

        :param veh_range: Specify range in KM, it will use this value as a percentage of 1000 to set the battery icon as well.

        :return: No return, just sends the message.
        """
        logging.info('Setting range and battery to %d' % veh_range)
        data1 = {'powerTrainHMIAvaDrvRange_CDI': veh_range}
        data2 = {'bat12vStateOfHealthIBSInfo_CDI': (veh_range/10)}
        try:
            range_msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHVbattStatus1', **data1)
            self.pcan_bus.pcan_write(range_msg)
            sleep(0.1)
            batt_msg = self.get_pcan_message('Chassis2', 'CDI_IBSstatus2', **data2)
            self.pcan_bus.pcan_write(batt_msg)
            logging.info('Range and Battery set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_front_doors(self, door_val):
        """
        Sets front door ajar values based on integer passed.

        :param door_val: INTEGER value representing state of doors.
        0: Driver Door CLOSED , Passenger Door CLOSED
        1: Driver Door OPEN, Passenger Door CLOSED
        2: Driver Door CLOSED, Passenger Door OPEN
        3: Driver Door OPEN, Passenger Door OPEN

        :return: No return, just sends the message.
        """
        logging.info('Setting front doors values to %d' % door_val)
        data = {}
        if door_val == 0:
            data = {'doorAjarD_FBCM': 0, 'doorAjarP_FBCM': 0}
        elif door_val == 1:
            data = {'doorAjarD_FBCM': 1, 'doorAjarP_FBCM': 0}
        elif door_val == 2:
            data = {'doorAjarD_FBCM': 0, 'doorAjarP_FBCM': 1}
        elif door_val == 3:
            data = {'doorAjarD_FBCM': 1, 'doorAjarP_FBCM': 1}
        else:
            print "Invalid selection, choose from 0-3."
        try:
            front_door_msg = self.get_pcan_message('Body1', 'FBCM_VehInfo1', **data)
            self.pcan_bus.pcan_write(front_door_msg)
            logging.info('Front Doors set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_rear_doors(self, door_val):
        """
        Sets rear door ajar values based on integer passed.

        :param door_val: INTEGER value representing state of doors.
        0: Rear Driver Door CLOSED , Rear Passenger Door CLOSED
        1: Rear Driver Door OPEN, Rear Passenger Door CLOSED
        2: Rear Driver Door CLOSED, Rear Passenger Door OPEN
        3: Rear Driver Door OPEN, Rear Passenger Door OPEN

        :return: No return, just sends the message.
        """
        logging.info('Setting rear doors values to %d' % door_val)
        data = {}
        if door_val == 0:
            data = {'doorAjarRR_RBCM': 0, 'doorAjarRL_RBCM': 0}
        elif door_val == 1:
            data = {'doorAjarRR_RBCM': 1, 'doorAjarRL_RBCM': 0}
        elif door_val == 2:
            data = {'doorAjarRR_RBCM': 0, 'doorAjarRL_RBCM': 1}
        elif door_val == 3:
            data = {'doorAjarRR_RBCM': 1, 'doorAjarRL_RBCM': 1}
        else:
            print "Invalid selection, choose from 0-3."
        try:
            rear_door_msg = self.get_pcan_message('Body1', 'RBCM_VehInfo1', **data)
            self.pcan_bus.pcan_write(rear_door_msg)
            logging.info('Rear Doors set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_hood(self, on_off):
        """
        Set value for hood latch.
        :param on_off: INTEGER value for ON (1) or OFF (0)
        :return: No return
        """
        logging.info('Setting hood value to %d' % on_off)
        data = {'hoodAjar_FBCM': on_off}
        try:
            msg = self.get_pcan_message('Body1', 'FBCM_VehInfo1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Hood set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_trunk(self, on_off):
        """
        Set value for trunk latch.
        :param on_off: INTEGER value for ON (1) or OFF (0)
        :return: No return
        """
        logging.info('Setting trunk value to %d' % on_off)
        data = {'trunkAjar_RBCM': on_off}
        try:
            msg = self.get_pcan_message('Body1', 'RBCM_VehInfo1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Trunk set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_seat_belts(self, seat_vals):  # TODO: Add seat belt logic for other seats later
        """
        Sets the state of seat belts within the vehicle.

        :param seat_vals: INTEGER value representing state of seat belts for all seats.

        :return: No return, just sends the message.
        """
        logging.info('Setting seat belt values to %d' % seat_vals)
        data = {'drivSeatBeltBcklState_ACM': seat_vals}
        try:
            msg = self.get_pcan_message('Body1', 'ACM_AirbagInfo1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Seat belts set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_abs_warning(self, on_off):  # 1 ok 2 bad, Moved to Chassis 1
        """
        Sets the state of ABS warning.

        :param on_off: INTEGER value, ON (2) or OFF (1).

        :return: No return, just sends the message.
        """
        logging.info('Setting ABS value to %d' % on_off)
        data = {'statusABS_ESP': on_off}
        try:
            msg = self.get_pcan_message('Chassis1', 'ESP_BrakeSystemStatus1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('ABS set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_brake_failure(self, on_off):  # CHANGED
        """
        Sets state of brake system failure warning.

        :param on_off: INTEGER value, ON (1) or OFF (0).

        :return: No return, just sends the message.
        """
        logging.info('Setting brake failure value to %d' % on_off)
        data = {'chksmBrakeSystemStatus1_ESP': on_off}
        try:
            msg = self.get_pcan_message('Chassis1', 'ESP_BrakeSystemStatus1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Brake Failure set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_parking_brake(self, brake_val):
        """
        Sets various states of parking brake.

        :param brake_val: INTEGER value to set on of the following 8 states, (1 and 2 primarily used).
        0 = Available Unknown
        1 = Available Applied
        2 = Available Released
        3 = Request in Progress
        4 = Unavailable Unknown
        5 = Unavailable Applied
        6 = Unavilable Applied
        7 = Dynamic Apply

        :return: No return, just sends the message.
        """
        logging.info('Setting parking brake value to %d' % brake_val)
        data = {'parkBrakeStatus_ESP': brake_val}
        try:
            msg = self.get_pcan_message('Chassis1', 'ESP_BrakeSystemStatus1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Parking brake value set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_electric_motor_failure(self, on_off):  # CHANGED
        """
        Sets value for electric motor failure warning.

        :param on_off: INTEGER value to set warning ON (1) or OFF (0).

        :return: No return, just sends the message.
        """
        logging.info('Setting electric motor failure value to %d' % on_off)
        data = {'powerTrainHMIDriveMotOverSpd_CDI': on_off}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Electric motor set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_hazard(self, on_off):
        """
        Sets value for hazard signal.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        logging.info('Setting hazard light value to %d' % on_off)
        data = {'hazardModeStatus_FBCM': on_off}
        try:
            msg = self.get_pcan_message('Body1', 'FBCM_VehInfo1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Hazard lights set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_fog_light(self, on_off):
        """
        Sets value for fog lights.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        logging.info('Setting fog light value to %d' % on_off)
        data = {'fogLampsOnRR_RBCM': on_off}
        try:
            msg = self.get_pcan_message('Body1', 'RBCM_VehInfo1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Fog lights set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_battery_coolant_temp_over(self, on_off):
        """
        Sets value for coolant temp warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        logging.info('Setting coolant over temp value to %d' % on_off)
        data = {'powTrainHMIHVBatCoolOvrTemp_CDI': on_off}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Coolant over temp value set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_battery_failure(self, on_off):  # CHANGED
        """
        Sets value for battery failure warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        logging.info('Setting battery failure value to %d' % on_off)
        data = {'powerTrainHMIHVBatCriFail_CDI': on_off}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Battery failure set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_charge_status(self, charge_val): # TODO: Fix charging values ties to range soc
        """
        Sets value for various states of charging.

        :param charge_val: INTEGER representing state of vehicle charge.
        0 = Initializing
        1 = Off Plug
        2 = Plug Detected
        3 = Charging Standby
        4 = AC Charging
        5 = DC Charging
        6 = Charging Completed
        7 = Fault

        :return: No return, just sends the message.
        """
        logging.info('Setting charge status to %d' % charge_val)
        data = {'chrgSystemStatus_CDI': charge_val}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PlugInChrgInfo1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Charge status set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_is_charging(self, on_off): # TODO: Find new messages
        """
        Sets value for charging indicator.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        logging.info('Setting Is Charging value to %d' % on_off)
        data = {'chksmPwrtrainHVbattStatus1_CDI': on_off}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHVbattStatus1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Is Charging set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_airbag_deployed(self, airbag_val):  # NOT Working
        """
        Set value for airbag deployed incident.

        :param airbag_val: INTEGER value representing state of airbag deployment.
        0 = No Deployment on Front Airbags
        1 = Only Driver Airbag Deployed
        2 = Only Passenger Airbag Deployed
        3 = Driver and Passenger Airbags Deployed

        :return: No return, just sends the message.
        """
        logging.info('Setting airbag deployed value to %d' % airbag_val)
        data = {'crashAirbagStatus_ACM': airbag_val}
        try:
            msg = self.get_pcan_message('Chassis2', 'ACM_AirbagInfo4', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Airbag deployed set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_airbag_failure(self, airbag_val):  # NOT Working
        """
        Sets value for airbag system failure warnings.

        :param airbag_val:  INTEGER value representing state of airbag failure.
        0 = RIL Request Off
        1 = Reserved
        2 = RIL Request Plant Mode
        3 = RIL Requested ON

        :return: No return, just sends the message.
        """
        logging.info('Setting airbag failure value to %d' % airbag_val)
        data = {'requestRILStatus_ACM': airbag_val}
        try:
            msg = self.get_pcan_message('Chassis2', 'ACM_AirbagInfo2', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Airbag failure set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_stability_control(self, dsc_val):  # 1 ok 2 bad
        """
        Sets value for DSC system.

        :param dsc_val: INTEGER value for different states.
        0 = Available
        1 = Active
        2 = Unavailable

        :return: No return, just sends the message.
        """
        logging.info('Setting DSC value to %d' % dsc_val)
        data = {'statusDSC_ESP': dsc_val}
        try:
            msg = self.get_pcan_message('Chassis1', 'ESP_BrakeSystemStatus1', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('DSC set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_steering_wheel_failure(self, on_off):  # NOT Working
        """
        Sets value for steering wheel failure warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends PCAN message.
        """
        logging.info('Setting steering wheel failure to %d' % on_off)
        data1 = {'primSysFault_EPAS1': on_off}
        data2 = {'secSysFault_EPAS2': on_off}
        try:
            msg = self.get_pcan_message('Chassis1', 'EPAS1_PwrSteeringInfo1', **data1)
            self.pcan_bus.pcan_write(msg)
            sleep(0.5)
            msg = self.get_pcan_message('Chassis2', 'EPAS2_PwrSteeringInfo2', **data2)
            self.pcan_bus.pcan_write(msg)
            logging.info('Steering wheel failure set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_vehicle_ready(self, ready_val): # NOT Working (???)
        """
        Sets value for READY signal.

        :param ready_val: INTEGER for different vehicle ready states.
        0 = Initializing
        1 = Drive Inhibited
        2 = Drive Ready
        3 = Error

        :return: No return, just sends the message.
        """
        logging.info('Setting READY state to %d' % ready_val)
        data = {'powerTrainDriveReadiness_CDI': ready_val}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainDriveStatus', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('READY state set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_limp_mode(self, on_off):
        """
        Sets value for vehicle limp mode warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        logging.info('Setting limp mode to %d' % on_off)
        data = {'powerTrainHMILimpHome_CDI': on_off}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Limp mode set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_insul_res_fault(self, on_off):
        """
        Set value for batter failure warning.
        :param on_off: INTEGER value for ON (1) or OFF (0)
        :return: No return
        """
        logging.info('Setting InsulRes Fault to %d' % on_off)
        data = {'PowerTrainHMIHVInsulResFault_CDI': on_off}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('InsulRes set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_system_failure(self, on_off):
        """
        Set value for system failure warning
        :param on_off: INTEGER value for ON (1) or OFF (0)
        :return: No return
        """
        logging.info('Setting system failure to %d' % on_off)
        data = {'powerTrainHMICriSysFailure_CDI': on_off}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('System failure set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_battery_low_warning(self, on_off):
        """
        Set value for low battery warning
        :param on_off: INTEGER value for ON (1) or OFF (0)
        :return: No return
        """
        logging.info('Setting battery low warning to %d' % on_off)
        data = {'powerTrainHMIHVBatLowSocWar_CDI': on_off}
        try:
            msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Low Battery Warning set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_tpms(self, wheel, tpms_val):  # NOT Working
        """
        Sets value for each TPMS warning state.

        :param wheel: INTEGER representing wheel to set.
        1 = FL wheel
        2 = FR wheel
        3 = RL wheel
        4 = RR wheel
        :param tpms_val: INTEGER value representing following TPMS states.
        0 = Normal
        1 = Low
        2 = Reserved
        3 = High
        4 = Not Available
        5, 6, 7 = Reserved

        :return: No return, just sends the message.
        """
        logging.info('Setting wheel %d TPMS state to %d' % (wheel, tpms_val))
        data = {}
        if wheel == 1:
            data = {'singleTyrePressStateFL_TPMS': tpms_val}
        elif wheel == 2:
            data = {'singleTyrePressStateFR_TPMS': tpms_val}
        elif wheel == 3:
            data = {'singleTyrePressStateRL_TPMS': tpms_val}
        elif wheel == 4:
            data = {'singleTyrePressStateRR_TPMS': tpms_val}
        else:
            print 'Invalid wheel selection'
        try:
            msg = self.get_pcan_message('Body1', 'TPMS_StatusTPMS', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('TPMS state set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def set_washer_fluid_low(self, on_off):  # NOT Working
        """
        Sets value for window washer fluid low warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        logging.info('Setting washer fluid warning value to %d' % on_off)
        data = {'washerFluidLow_FBCM': on_off}
        try:
            msg = self.get_pcan_message('Body1', 'FBCM_DiagInfoInputs', **data)
            self.pcan_bus.pcan_write(msg)
            logging.info('Washer fluid warning set successfully')
        except KeyError:
            logging.warning('Invalid PCAN message')
            self.failed = True

    def loop_speedometer(self, min_val=0, max_val=360, step=1, delay=0.1):
        """
        Uses set_speedometer function to increment and decrement speedometer value indefinitely.

        :param min_val: Minimum speedometer value to display (Default: 0)
        :param max_val: Maximum speedometer value to display (Default: 360)
        :param step: Gap between two message values (Default: 1)
        :param delay: How often to send next PCAN message (Default: 0.1)

        :return: No return, just sends the message.
        """
        logging.info('Beginning speedometer continuous loop')
        while not self.stop_device_test:
            for i in xrange(min_val, max_val, step):
                self.set_speedometer(i)
                sleep(delay)
            for i in xrange(max_val, min_val, -step):
                self.set_speedometer(i)
                sleep(delay)

    def loop_odometer(self, min_val=0, max_val=1000000, step=1000, delay=0.1):
        """
        Uses set_odometer function to increment and decrement odometer value indefinitely.

        :param min_val: Minimum odometer value to display (Default: 0)
        :param max_val: Maximum odometer value to display (Default: 1000000)
        :param step: Gap between two message values (Default: 1000)
        :param delay: How often to send next PCAN message (Default: 0.1)

        :return: No return, just sends the message
        """
        logging.info('Beginning odometer continuous loop')
        while not self.stop_device_test:
            for i in xrange(min_val, max_val, step):
                self.set_odometer(i)
                sleep(delay)
            for i in xrange(max_val, min_val, -step):
                self.set_odometer(i)
                sleep(delay)

    def loop_range_and_battery(self, min_val=0, max_val=1000, step=10, delay=0.1):
        """
        Uses set_range_and_battery function to increment and decrement range value and battery icon indefinitely.

        :param min_val: Minimum range value to display (Default: 0)
        :param max_val: Maximum range value to display (Default: 1000)
        :param step: Gap between two message values (Default: 10)
        :param delay: How often to send next PCAN message (Default: 0.1)

        :return: No return, just sends the message
        """
        logging.info('Beginning range and battery continuous loop')
        while not self.stop_device_test:
            for i in xrange(min_val, max_val, step):
                self.set_range_and_battery(i)
                sleep(delay)
            for i in xrange(max_val, min_val, -step):
                self.set_range_and_battery(i)
                sleep(delay)

    def random_warnings(self, signal_list, delay=3):
        """
        Randomly selects a warning given a list of warning functions.

        :param signal_list: A list containing functions that call warnings.
        :param delay: Delay between on and off time.

        :return: No return.
        """
        func_call = random.choice(signal_list)
        sleep(delay)
        if func_call == self.set_headlights:
            light = random.choice(['H', 'L'])
            self.set_headlights(light, 1)
            sleep(delay)
            self.set_headlights(light, 0)
        elif func_call == self.flashing_left_turn_signal or func_call == self.flashing_right_turn_signal:
            func_call(5)
        elif func_call == self.set_tpms:
            self.set_tpms(1, 1)
            sleep(delay)
            self.set_tpms(1, 0)
        elif func_call == self.set_parking_brake:
            func_call(1)
            sleep(delay)
            func_call(2)
        elif func_call == self.set_abs_warning:
            func_call(2)
            sleep(delay)
            func_call(1)
        else:
            func_call(1)
            sleep(delay)
            func_call(0)

    def loop_warnings(self, delay=3):
        """
        Loop through all system warnings on IC randomly.

        :param delay: Time delay between each signal.

        :return: No return.
        """
        logging.info('Beginning random warning continuous loop')
        signal_list = [self.set_stability_control, self.set_steering_wheel_failure, self.set_parking_brake,
                       self.set_washer_fluid_low, self.set_limp_mode, self.set_battery_coolant_temp_over,
                       self.set_electric_motor_failure, self.set_hazard, self.set_brake_failure, self.set_fog_light,
                       self.set_headlights, self.set_abs_warning, self.flashing_left_turn_signal,
                       self.flashing_right_turn_signal, self.set_tpms, self.set_battery_low_warning, self.set_system_failure,
                       self.set_insul_res_fault]
        while not self.stop_device_test:
            self.random_warnings(signal_list, delay)


if __name__ == '__main__':
    import signal

    test = ReliabilityTester('DBC/F35.04/')
    test.vehicle_power_management_on()
    signal.signal(signal.SIGINT, test.signal_handler)
    # test.loop_warnings(3)
    # print "Gear"
    # test.set_gear('park')
    # sleep(1)
    # test.set_gear('reverse')
    # sleep(1)
    # test.set_gear('neutral')
    # sleep(1)
    # test.set_gear('drive')
    # sleep(1)
    # print "Front Door"
    # test.set_front_doors(3)
    # sleep(2)
    # print "HV Enable"
    # test.set_hv_enable(1)
    # sleep(1)
    # print "Rear Door"
    # test.set_rear_doors(3)
    # sleep(2)
    # print "Hood"
    # test.set_hood(1)
    # sleep(2)
    # print "Trunk"
    # test.set_trunk(1)
    # sleep(2)
    # test.set_trunk(0)
    # test.set_hood(0)
    # print "Speed"
    # test.set_speedometer(5)
    # sleep(1)
    # print " Headlights"
    # test.set_headlights('h', 1)
    # sleep(2)
    # test.set_headlights('l', 1)
    # sleep(1)
    # print "Fog Lights"
    # test.set_fog_light(1)
    # sleep(2)
    # print "Odometer"
    # test.set_odometer(35600)
    # sleep(1)
    # print "Not Charging"
    # test.set_is_charging(0)
    # sleep(2)
    # print "Range and Battery"
    # test.set_range_and_battery(478)
    # sleep(4)
    print "Is Charging"
    test.set_is_charging(1)
    sleep(2)
    print "Charge Percent"
    test.set_percent_charged(80)
    sleep(1)
    test.set_percent_charged(20)
    sleep(1)
    print "Not Charging"
    test.set_is_charging(0)
    sleep(2)
    test.set_battery(40)
    sleep(3)
    print "Range and Battery"
    test.set_range_and_battery(90)
    sleep(2)
    test.set_range_and_battery(190)
    sleep(2)
    test.set_range_and_battery(390)
    sleep(2)
    test.set_range_and_battery(590)
    sleep(2)
    test.set_range_and_battery(790)
    sleep(2)
    # # print "ABS"
    # test.set_abs_warning(2)
    # sleep(2)
    # test.set_abs_warning(1)
    # sleep(2)
    # print "Battery Fail"
    # test.set_battery_failure(0)
    # sleep(2)
    # test.set_battery_failure(1)
    # sleep(2)
    # test.set_battery_failure(0)
    # print "Brake Fail"
    # test.set_brake_failure(0)
    # sleep(2)
    # test.set_brake_failure(1)
    # sleep(2)
    # test.set_brake_failure(0)
    # sleep(2)
    # print "Airbag Deploy"
    # test.set_airbag_deployed(1)
    # sleep(1)
    # print "Airbag Fail"
    # test.set_airbag_failure(1)
    # sleep(1)
    # print "Charge Status"
    # test.set_charge_status(2)
    # sleep(1)
    # print "Is Charging"
    # test.set_is_charging(1)
    # sleep(1)
    # print "Hazards"
    # test.set_hazard(1)
    # sleep(1)
    # print "Motor Fail"
    # test.set_electric_motor_failure(0)
    # sleep(2)
    # test.set_electric_motor_failure(1)
    # sleep(2)
    # test.set_electric_motor_failure(0)
    # sleep(2)
    # print "TPMS"
    # test.set_tpms(1, 1)
    # sleep(2)
    # test.set_tpms(1, 0)
    # sleep(2)
    # test.set_tpms(2, 1)
    # sleep(2)
    # test.set_tpms(2, 0)
    # sleep(2)
    # test.set_tpms(3, 1)
    # sleep(2)
    # test.set_tpms(3, 0)
    # sleep(2)
    # test.set_tpms(4, 1)
    # sleep(2)
    # test.set_tpms(4, 0)
    # sleep(2)
    # print "Seat Belts"
    # test.set_seat_belts(0)
    # sleep(2)
    # test.set_seat_belts(1)
    # sleep(2)
    # test.set_seat_belts(0)
    # sleep(2)
    # print "Limp Mode"
    # test.set_limp_mode(1)
    # sleep(2)
    # test.set_limp_mode(0)
    # sleep(2)
    # print "Battery Coolant Temp"
    # test.set_battery_coolant_temp_over(1)
    # sleep(1)
    # test.set_battery_coolant_temp_over(0)
    # sleep(1)
    # print "Parking Brake"
    # test.set_parking_brake(2)
    # sleep(1)
    # test.set_parking_brake(1)
    # sleep(2)
    # test.set_parking_brake(2)
    # sleep(1)
    # print "Washer Fluid"
    # test.set_washer_fluid_low(1)
    # sleep(1)
    # test.set_washer_fluid_low(0)
    # sleep(1)
    # print "READY"
    # test.set_vehicle_ready(2)
    # sleep(1)
    # print "Steering Fail"
    # test.set_steering_wheel_failure(1)
    # sleep(1)
    # test.set_steering_wheel_failure(0)
    # sleep(1)
    # print "DSC"
    # test.set_stability_control(0)
    # sleep(1)
    # test.set_stability_control(1)
    # sleep(2)
    # test.set_stability_control(0)
    # sleep(1)
    # print 'System Fault'
    # test.set_insul_res_fault(1)
    # sleep(3)
    # test.set_insul_res_fault(0)
    # sleep(1)
    # print 'System Failue'
    # test.set_system_failure(1)
    # sleep(3)
    # test.set_system_failure(0)
    # sleep(1)
    # print 'Low Battery'
    # test.set_battery_low_warning(1)
    # sleep(3)
    # test.set_battery_low_warning(0)
    #
    # print "Multithread"
    # funcs = {test.loop_odometer: (0,1000, 1, 0.1,), test.loop_speedometer: (0,360,1,0.1,)}
    # test.multithread(funcs)
