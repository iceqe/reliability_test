import importlib
import signal
from time import sleep

from general import get_test_case_settings


def extended_basic_scenario2(version):
    test_info = get_test_case_settings(version)
    RT = importlib.import_module(test_info['utils'])
    tester = RT.ReliabilityTester(test_info['DBC'])
    signal.signal(signal.SIGINT, tester.signal_handler)

    # Unlock then Power Mode ON
    tester.vehicle_power_management_on()

    primary_loops = {tester.loop_odometer: (0, 1000000, 1000, 8,),
                     tester.loop_speedometer: (0, 360, 1, 3,),
                     tester.loop_range_and_battery: (0, 1000, 10, 10,)}

    tester.multithread(primary_loops, join=False)

    while not tester.stop_device_test:
        tester.set_front_doors(3)
        sleep(2)
        tester.set_front_doors(2)
        sleep(2)
        tester.set_front_doors(1)
        sleep(2)
        tester.set_front_doors(0)
        sleep(2)
        tester.set_hood(1)
        # tester.bt_call(60)
        tester.flashing_left_turn_signal(10)
        sleep(5)
        tester.set_rear_doors(3)
        sleep(2)
        tester.set_rear_doors(2)
        sleep(2)
        tester.set_rear_doors(1)
        sleep(2)
        tester.set_rear_doors(0)
        sleep(2)
        tester.set_trunk(1)
        # tester.navigate('home', 'work')
        # tester.radio_start('FM')
        tester.set_headlights('H', 1)
        sleep(5)
        tester.set_headlights('H', 0)
        sleep(1)
        tester.set_trunk(0)
        sleep(1)
        tester.flashing_right_turn_signal(6)
        sleep(5)


if __name__ == '__main__':
    import sys

    extended_basic_scenario2(sys.argv[1])
