import yaml
from time import sleep
import threading
import sys
from binascii import hexlify
import imp


def get_test_case_settings(version):
    with open("settings.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
    if version.upper() == 'VP':
        return cfg['VP']
    elif version.upper() == 'PVP':
        return cfg['PVP']
    elif version.upper() == 'AP':
        return cfg['AP']
    else:
        print 'Invalid milestone'
        return None


def signal_handler(signal, frame):
    print('Ctrl+C, exiting...')
    global stop_device_test
    stop_device_test = True
    sleep(3)
    exit(1)

# signal.signal(signal.SIGINT, signal_handler)
