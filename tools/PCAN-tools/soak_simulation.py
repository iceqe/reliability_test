import pcanoperations as pcan
from time import sleep
import threading
import keyfob_utils as kf
import sys


def power_mode_on_sequence(pcan_kw, waittime):
    try:
        kf.keyfob_button("unlock")
    except AttributeError:
        return "Yoctopuce not connected."
    drivers_door_open = ['0x500', "PCAN_MESSAGE_STANDARD", "8", '0x00', '0x80', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00']
    pcan_kw.pcan_write(drivers_door_open)
    sleep(waittime)
    HV_en = ['0x023', "PCAN_MESSAGE_STANDARD", "4", '0x40', '0x00', '0x00', '0x00']
    pcan_kw.pcan_write(HV_en)
    sleep(waittime)
    drive_gear = ['0x024', "PCAN_MESSAGE_STANDARD", "6", '0x80', '0x00', '0x00', '0x00', '0x00', '0x00']
    pcan_kw.pcan_write(drive_gear)
    sleep(waittime)
    drivers_door_close = ['0x500', "PCAN_MESSAGE_STANDARD", "8", '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00']
    pcan_kw.pcan_write(drivers_door_close)
    sleep(waittime)


def send_diff_speeds(pcan_kw, max_val, waittime):
    while True:
        for x in xrange(max_val):
            speed_msg = ['0x38B', "PCAN_MESSAGE_STANDARD", "4", '0x00', '0x00', hex(x), '0x00']
            pcan_kw.pcan_write(speed_msg)
            sleep(waittime)
        left_turn = ['0x500', "PCAN_MESSAGE_STANDARD", "8", '0x40', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00']
        pcan_kw.pcan_write(left_turn)
        sleep(waittime)
        for x in xrange(max_val, -1, -1):
            speed_msg = ['0x38B', "PCAN_MESSAGE_STANDARD", "4", '0x00', '0x00', hex(x), '0x00']
            pcan_kw.pcan_write(speed_msg)
            sleep(waittime)
        right_turn = ['0x500', "PCAN_MESSAGE_STANDARD", "8", '0x80', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00']
        pcan_kw.pcan_write(right_turn)
        sleep(waittime)


def odo_hex_list(val):
    line = hex(val)[2:]
    if len(line) % 2:
        line = '0000000' + line
    else:
        line = '000000' + line

    hex_list = ['0x'+line[i:i + 2] for i in range(0, len(line), 2)]
    return hex_list[-3:]


def send_diff_odo(pcan_kw, max_val, waittime):
    while True:
        for x in xrange(0, max_val, 1000):
            hex_list = odo_hex_list(x)
            odo_msg = ['0x602', "PCAN_MESSAGE_STANDARD", "3"] + hex_list
            pcan_kw.pcan_write(odo_msg)
            sleep(waittime)
        high_beam = ['0x500', "PCAN_MESSAGE_STANDARD", "8", '0x02', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00']
        pcan_kw.pcan_write(high_beam)
        sleep(waittime)
        for x in xrange(max_val, -1, -1000):
            hex_list = odo_hex_list(x)
            odo_msg = ['0x602', "PCAN_MESSAGE_STANDARD", "3"] + hex_list
            pcan_kw.pcan_write(odo_msg)
            sleep(waittime)
        low_beam = ['0x500', "PCAN_MESSAGE_STANDARD", "8", '0x01', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00', '0x00']
        pcan_kw.pcan_write(low_beam)
        sleep(waittime)


def range_hex_list(val):
    line = hex(val)[2:]
    if len(line) % 2:
        line = '0000000' + line
    else:
        line = '000000' + line

    hex_list = ['0x'+line[i:i + 2] for i in range(0, len(line), 2)]
    return hex_list[-6:]


def set_bat_charge(val):
    if 0 <= val <= 0.20:
        return '0x00'
    if 0.20 < val <= 0.50:
        return '0x3E'
    if 0.50 < val <= 0.75:
        return '0x7D'
    if 0.75 < val < 0.90:
        return '0xBB'
    if 0.90 <= val <= 1:
        return '0xFA'


def send_diff_range(pcan_kw, waittime):
    while True:
        for x in xrange(65536, 65536000, 65536):
            hex_list = range_hex_list(x)
            range_msg = ['0x403', "PCAN_MESSAGE_STANDARD", "6"] + hex_list
            range_msg[4] = set_bat_charge(float(x) / 65536000)
            print range_msg
            pcan_kw.pcan_write(range_msg)
            sleep(waittime)
        for x in xrange(65536000, 65535, -65536):
            hex_list = range_hex_list(x)
            range_msg = ['0x403', "PCAN_MESSAGE_STANDARD", "6"] + hex_list
            range_msg[4] = set_bat_charge(float(x) / 65536000)
            print range_msg
            pcan_kw.pcan_write(range_msg)
            sleep(waittime)


pcan_kw_bus01 = pcan.PCANKeywords()
pcan_kw_bus01.pcan_create('usbbus1', '500k', 'message_filter')

if __name__ == '__main__':
    import logging

    logging.basicConfig(filename='soak_sim.log', level=logging.INFO)
    logging.info('Starting Power Mode ON Sequence')
    power_mode_on_sequence(pcan_kw_bus01, 0.1)
    logging.info('Power Mode ON Complete')

    threads = []

    logging.info('Starting Thread 1: Speed Variation')
    t1 = threading.Thread(target=send_diff_speeds, args=(pcan_kw_bus01, 255, 0.1,))
    threads.append(t1)

    logging.info('Starting Thread 1: Odometer Variation')
    t2 = threading.Thread(target=send_diff_odo, args=(pcan_kw_bus01, 1000000, 0.1,))
    threads.append(t2)

    logging.info('Starting Thread 1: Range Variation')
    t3 = threading.Thread(target=send_diff_range, args=(pcan_kw_bus01, 0.1,))
    threads.append(t3)
    for t in threads:
        t.daemon = True
        t.start()
    for t in threads:
        t.join()
    logging.info('All Threads Joined.')


