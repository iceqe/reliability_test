import importlib
import signal
from time import sleep

from general import get_test_case_settings


def basic_scenario_functional(version):
    test_info = get_test_case_settings(version)
    RT = importlib.import_module(test_info['utils'])
    tester = RT.ReliabilityTester(test_info['DBC'])
    signal.signal(signal.SIGINT, tester.signal_handler)

    # Unlock then Power Mode ON
    tester.vehicle_power_management_on()

    tester.multithread({tester.accelerate: (0, 60, 2)}, join=False)

    primary_loops = {tester.loop_odometer: (0, 10000, 1000, 7,),
                     tester.loop_range_and_battery: (0, 1000, 10, 10,),
                     tester.loop_speedometer: (65, 85, 1, 4)}

    tester.multithread(primary_loops, join=False)

    while not tester.stop_device_test:
        tester.execute_functional('../../../ice-test/fic/', '-i ReliabilityANDDoor=FL', 5)
        sleep(2)
        tester.flashing_right_turn_signal(5)
        sleep(2)
        tester.execute_functional('../../../ice-test/fic/', '-i ReliabilityANDDoor=ALL', 5)
        sleep(2)
        tester.flashing_left_turn_signal(5)
        sleep(2)
        tester.execute_functional('../../../ice-test/fic/', '-i ReliabilityANDDoor=FR', 5)
        sleep(2)
        tester.set_limp_mode(1)
        sleep(2)
        tester.set_limp_mode(0)
        sleep(2)
        tester.execute_functional('../../../ice-test/fic/', '-i ReliabilityANDDoor=ALL', 5)
        sleep(2)
        tester.set_headlights('h', 1)
        sleep(2)
        tester.execute_functional('../../../ice-test/fic/', '-i ReliabilityANDDoor=RL', 5)
        sleep(2)
        tester.set_trunk(1)
        sleep(2)
        tester.execute_functional('../../../ice-test/fic/', '-i ReliabilityANDDoor=ALL', 5)
        sleep(2)
        tester.set_hood(1)
        sleep(2)
        tester.execute_functional('../../../ice-test/fic/', '-i ReliabilityANDDoor=RR', 5)
        sleep(2)
        tester.set_stability_control(1)
        sleep(2)
        tester.set_stability_control(0)
        sleep(2)
        tester.execute_functional('../../../ice-test/fic/', '-i ReliabilityANDDoor=ALL', 5)
        sleep(2)
        tester.set_system_failure(1)
        sleep(2)


if __name__ == '__main__':
    import sys

    basic_scenario_functional(sys.argv[1])
