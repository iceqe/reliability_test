# Copyright 2018-     BYTON
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Author: Sheng Zhao <sheng.zhao@byton.com>
import time

from robot.api import logger
from PCANBasic import *


class PCAN(object):
    def __init__(self, bus, baud, message_filter=None):
        self.pcan_obj = PCANBasic()
        self.pcan_msg = TPCANMsg()
        self.bus = bus
        self.baud = baud
        self.message_filter = message_filter

    def init_pcan_usbbus(self):
        result = self.pcan_obj.Initialize(self.bus, self.baud)
        if result != PCAN_ERROR_OK:
            # An error occurred, get a text describing the error and show it
            #
            result = self.pcan_obj.GetErrorText(result)
            # print result[1]
            logger.error(result[1])
        else:
            # print "PCAN-PCAN_USBBUS1 (Ch-1) was initialized"
            logger.info("PCAN channel[%s] has been initialized" % self.bus, also_console=True)

        result = self.pcan_obj.GetValue(self.bus, self.message_filter)
        if result[0] != PCAN_ERROR_OK:
            # An error occurred, get a text describing the error and show it
            #
            result = self.pcan_obj.GetErrorText(result)
            # print result
            logger.error(result)
        else:
            # A text is shown giving information about the current status of the filter
            #
            if result[1] == PCAN_FILTER_OPEN:
                # print "The message filter for the PCAN-USB, channel 1, is completely opened."
                logger.info("The message filter for the PCAN-USB, channel[%s], is completely opened." % self.bus,
                            also_console=True)
            elif result[1] == PCAN_FILTER_CLOSE:
                # print "The message filter for the PCAN-USB, channel 1, is closed."
                logger.info("The message filter for the PCAN-USB, channel[%s], is closed." % self.bus,
                            also_console=True)
            elif result[1] == PCAN_FILTER_CUSTOM:
                # print "The message filter for the PCAN-USB, channel 1, is custom configured."
                logger.info("The message filter for the PCAN-USB, channel[%s], is custom configured." % self.bus,
                            also_console=True)

    def reset_pcan(self):
        cur_status = self.pcan_obj.GetStatus(self.bus)
        if cur_status != PCAN_ERROR_OK:
            res_status = self.pcan_obj.Reset(self.bus)
            if res_status != PCAN_ERROR_OK:
                logger.error('Please reset the PCAN manually!')
            else:
                logger.info("PCAN channel[%s] has been reset successfully." % self.bus, also_console=True)

    def write_request_messages(self, msg):
        self.pcan_msg.ID = eval(msg[0])
        self.pcan_msg.MSGTYPE = eval(msg[1])
        self.pcan_msg.LEN = int(msg[2])
        len_msg = len(msg)
        index_data = 0
        for i in range(3, len_msg):
            self.pcan_msg.DATA[index_data] = eval(msg[i])
            index_data = index_data + 1
        # self.pcan_msg.DATA[0] = eval(msg[3])
        # self.pcan_msg.DATA[1] = eval(msg[4])
        # self.pcan_msg.DATA[2] = eval(msg[5])
        # self.pcan_msg.DATA[3] = eval(msg[6])
        # self.pcan_msg.DATA[4] = eval(msg[7])
        # self.pcan_msg.DATA[5] = eval(msg[8])
        # self.pcan_msg.DATA[6] = eval(msg[9])
        # self.pcan_msg.DATA[7] = eval(msg[10])

        result = self.pcan_obj.Write(self.bus, self.pcan_msg)
        # time.sleep(2)
        if result != PCAN_ERROR_OK:
            # An error occurred, get a text describing the error and show it
            #
            result = self.pcan_obj.GetErrorText(result)
            # print result
            logger.error(result)
        else:
            # logging.info("Write can message(id{0}) successfully".format(hex(self.pcan_msg.ID)))
            # print ("Write CAN message(id{0}) successfully".format(hex(self.pcan_msg.ID)))
            logger.info("Write CAN message ID[{0}] successfully.".format(hex(self.pcan_msg.ID)), also_console=True)

    def read(self):
        msg_count = 0
        read_result = PCAN_ERROR_OK,
        while (read_result[0] & PCAN_ERROR_QRCVEMPTY) != PCAN_ERROR_QRCVEMPTY:
            # Check the receive queue for new messages
            read_result = self.pcan_obj.Read(self.bus)
            if read_result[0] != PCAN_ERROR_QRCVEMPTY:
                # Process the received message
                msg_count = msg_count + 1
                logger.info("A CAN message was received, message count: %s" % msg_count, also_console=True)
                self._process_message(read_result[1], read_result[2])
            else:
                # An error occurred, get a text describing the error and show it
                error_info = self.pcan_obj.GetErrorText(read_result[0])
                logger.info(error_info[1], also_console=True)
                # return error_info[1]
            time.sleep(1)

    def _process_message(self, msg=None, timestamp=None):
        if msg and timestamp:
            logger.info("Read Msg: ID[{0}] LEN[{1}] DATA[{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}]".format
                        ('0x' + hex(msg.ID)[2:].rjust(4, '0'), msg.LEN, hex(msg.DATA[0])[2:].rjust(2, '0'),
                         hex(msg.DATA[1])[2:].rjust(2, '0'), hex(msg.DATA[2])[2:].rjust(2, '0'),
                         hex(msg.DATA[3])[2:].rjust(2, '0'), hex(msg.DATA[4])[2:].rjust(2, '0'),
                         hex(msg.DATA[5])[2:].rjust(2, '0'), hex(msg.DATA[6])[2:].rjust(2, '0'),
                         hex(msg.DATA[7])[2:].rjust(2, '0')), also_console=True)
            converted_timestamp = timestamp.micros + 1000 * timestamp.millis + 0x100000000 * 1000 * timestamp.millis_overflow
            logger.info("Total timestamp: %s " % str(converted_timestamp), also_console=True)

            return msg, timestamp

    def read_respond_messages(self, logging_obj):
        a, b, c = self.pcan_obj.Read(self.bus)
        total_timestamp = 0
        if '0x' + hex(b.ID)[2:].rjust(4, '0') != '0x000L':  # not print the content if can_id = 0x000L
            logging_obj.info("Read Msg: ID[{0}] LEN[{1}] DATA[{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}]".format
                             ('0x' + hex(b.ID)[2:].rjust(4, '0'), b.LEN, hex(b.DATA[0])[2:].rjust(2, '0'),
                              hex(b.DATA[1])[2:].rjust(2, '0'), hex(b.DATA[2])[2:].rjust(2, '0'),
                              hex(b.DATA[3])[2:].rjust(2, '0'), hex(b.DATA[4])[2:].rjust(2, '0'),
                              hex(b.DATA[5])[2:].rjust(2, '0'), hex(b.DATA[6])[2:].rjust(2, '0'),
                              hex(b.DATA[7])[2:].rjust(2, '0')))
            # print "Read Timestamp: {0} {1} {2}".format(c.millis, c.millis_overflow, c.micros)
            total_timestamp = c.micros + 1000 * c.millis + 0x100000000 * 1000 * c.millis_overflow
            logging_obj.info("Total timestamp: %s " % str(total_timestamp))
        time.sleep(0.01)
        return hex(b.ID), total_timestamp

    def uninit_usbbus(self):
        try:
            self.pcan_obj.Uninitialize(self.bus)
        except:
            logger.error("Exception on PCANBasic.Uninitialize")
            raise
        else:
            logger.info("PCAN Uninitialize Succeeded.", also_console=True)

    def get_canstatus(self):
        can_status = self.pcan_obj.GetStatus(self.bus)
        if can_status == PCAN_ERROR_OK:
            # print 'status all right'
            logger.info("Status of PCAN channel [%s] is OK" % self.bus, also_console=True)
        return can_status
