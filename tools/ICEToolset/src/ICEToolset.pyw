# -*- coding: utf-8 -*-
'''
Created on 2018-8-22

@author: cavin.rui
'''

import wx
import ConfigParser
import time
import SSHLibrary 
import threading
import re
import os
from wx.lib.wordwrap import wordwrap
import platform
import copy
import tarfile
import wx.lib.masked as masked
from jenkinsapi import jenkins
from artifactory import ArtifactoryPath
import traceback
import paramiko
import requests
import cantools

paramiko.util.log_to_file ('paramiko.log') # fix a exception "no handlers could be found for logger paramiko.transport", 

try:
    Pylog = wx.PyLog  
except:
    Pylog = wx.Log
    
    
ID_UPLOAD_FIRMWARE = 100
ID_CLSOE = 101
ID_ABOUT = 102

_ListOption = [ 
                 'Dynamics',
                 'Warnings',                 
                 'Battery',
                 'Driving Lights',
                 'Wipers',
                 'Doors',
                 'Seat Belts',                 
                 'Airbags Set',
                 'Fluids',
                 'Drive Warnings',
                 'System Failure',
                 'Preferences'
                ]
             
class MyLog(Pylog):
    def __init__(self, textCtrl, logTime=0):
        Pylog.__init__(self)
        self.tc = textCtrl
        self.logTime = logTime

    def DoLogString(self, message, timeStamp):
        if self.tc:
            self.tc.AppendText(message + '\n')

class Configure():
    def __init__(self):
        self.config = ConfigParser.RawConfigParser()
        self.config.read('settings.ini')
    
    def GetConfigValue(self, section, option):
        try:
            return self.config.get(section, option)
        except:
            return None
        
    def SetConfigValue(self, section, option, value):
        try:
            for item in self.config.sections():
                for subItem in self.config.options(item):
                        self.config.set(item, subItem, self.GetConfigValue(item, subItem))
            
            if not self.config.has_section(section):
                self.config.add_section(section)
    
            self.config.set(section, option, value)
            
            with open('settings.ini', 'w') as configfile:    
                self.config.write(configfile)
        except:
            print "saving configuration failed"

class CANSignalParser(wx.Panel):
    def __init__(self, parent, notebook):
        wx.Panel.__init__(self, notebook)
        self.parent = parent
        self.objConfigure = parent.objConfigure
        self.server = self.objConfigure.GetConfigValue('DB', 'host')
        self.user = 'byton'
        self.password = self.objConfigure.GetConfigValue('DB', 'pwd')
        self.db = None
        self.flagDisplay = True
        self.InitUI()
        self.BindEvent()
        self.parent.RunAsThread(self.DownloadDBC)
        
    
    def LoginLinux(self, ip, user, pwd):
        try:
            objSSHLibrary = SSHLibrary.SSHLibrary()
            objSSHLibrary.open_connection(host=ip, port=22)
            objSSHLibrary.login(user, pwd)
            return objSSHLibrary
        except:
            return False
        
    def DownloadDBC(self):
        objSSHLibrary = self.LoginLinux(self.server, self.user, self.password)
        if objSSHLibrary is False:
            dlg = wx.MessageDialog(self, 'Can not connect to server:\n%s' % self.server,
                               'Error',
                               wx.OK | wx.ICON_INFORMATION
                               )
            dlg.ShowModal()
            dlg.Destroy()
            return
        
        objSSHLibrary.get_file('/data/DBC/Thermal.dbc', "Thermal.dbc")
        objSSHLibrary.close_all_connections()
        
        if not os.path.exists("Thermal.dbc"):
            dlg = wx.MessageDialog(self, 'No DBC download, try re-open tool',
                               'Error',
                               wx.OK | wx.ICON_INFORMATION
                               )
            dlg.ShowModal()
            dlg.Destroy()   
            return
        
        self.db = cantools.database.load_file('Thermal.dbc')
        listMessages = [repr(item) for item in self.db.messages]
        self.comboboxFrameLeft.SetItems(listMessages)
        self.comboboxFrameRight.SetItems(listMessages)
        self.comboboxFrameLeft.Select(16)
        self.comboboxFrameRight.Select(len(listMessages)-14)
        
    def InitUI(self):
        vbox = wx.BoxSizer(wx.VERTICAL)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.comboboxFrameLeft = wx.ComboBox(self, 200, "", (40, 50), (400, -1), [], 
                         wx.CB_DROPDOWN|wx.CB_READONLY)
        
        self.comboboxFrameRight = wx.ComboBox(self, 200, "", (40, 50), (400, -1), [], 
                         wx.CB_DROPDOWN|wx.CB_READONLY)
        
        self.btnShow = wx.Button(self, -1, "Inspect")
        self.btnInspectAll = wx.Button(self, -1, "Inspect All")
        
        self.labelLeft = wx.StaticText(self, -1, "")
        self.labelRight = wx.StaticText(self, -1, "")
        
        vbox.Add(self.comboboxFrameLeft)
        vbox.AddSpacer(50)
        vbox.Add(self.labelLeft)
        hbox.AddSizer(vbox)
        
        hbox.AddSpacer(100)
        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(self.btnShow)
        vbox.AddSpacer(20)
        vbox.Add(self.btnInspectAll)
        hbox.AddSizer(vbox)
        
        hbox.AddSpacer(100)
        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(self.comboboxFrameRight)
        vbox.AddSpacer(50)
        vbox.Add(self.labelRight)
        hbox.AddSizer(vbox)
        
        
        self.SetSizer(hbox)
        self.SetAutoLayout(1)
        return
    
    def BindEvent(self):
        self.Bind(wx.EVT_BUTTON, self.OnDisplay, self.btnShow)
        self.Bind(wx.EVT_BUTTON, self.OnInspectAll, self.btnInspectAll)
    
    def IsCGWConnected(self):
        objSSHLibrary = self.LoginLinux('192.168.111.2', 'byton', 'byton!')
        if objSSHLibrary is False:
            dlg = wx.MessageDialog(self, 'Can not connect to CGW:\n192.168.111.2',
                               'Error',
                               wx.OK | wx.ICON_INFORMATION
                               )
            dlg.ShowModal()
            dlg.Destroy()
            self.btnShow.SetLabel('Inspect')
            return 0
        
        objSSHLibrary.set_client_configuration(timeout=0.5)
        return objSSHLibrary
    
    def ThreadForDispaly(self):
        objSSHLibrary = self.IsCGWConnected()
        if objSSHLibrary == 0:
            return
        
        messageLeft = eval(self.comboboxFrameLeft.GetStringSelection().replace("message", ''))
        messageRight = eval(self.comboboxFrameRight.GetStringSelection().replace("message", ''))
        nameLeft = messageLeft[0]
        nameRight = messageRight[0]
        
        if not os.path.exists('CAN_parsed'):
            os.mkdir('CAN_parsed')
       
        f = open('CAN_parsed/%s_%s%s.log' % (nameLeft, nameRight, time.strftime('%Y-%m-%d %H%M%S', time.localtime())),'w')
        
        while self.flagDisplay:
            label = self.ParseSingleCANMessage(self.comboboxFrameLeft.GetStringSelection(), objSSHLibrary)
            f.write(label)
            self.labelLeft.SetLabel(label)
            label = self.ParseSingleCANMessage(self.comboboxFrameRight.GetStringSelection(), objSSHLibrary)
            f.write(label)
            self.labelRight.SetLabel(label)
        
        f.close()
    
    def ParseSingleCANMessage(self, item, objSSHLibrary):
        try:
            message= eval(item.replace("message", ''))
            name = message[0]
            strHex = str(hex(message[1])).replace('0x', '').upper()
            objSSHLibrary.write_bare(chr(int(3)))
            objSSHLibrary.write("candump any | grep %s" % strHex)
            output = objSSHLibrary.read_until_regexp("%s\s+\[8\]\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+" % strHex)
            label = repr(self.db.decode_message(name, str(output))).replace(',', ',\n')
            return "%s:\n%s\n" % (name, label)
        except:
            return "%s:\n{no data found}\n" % name
    
    def ThreadForInspectAll(self):
        objSSHLibrary = self.IsCGWConnected()
        if objSSHLibrary == 0:
            return
        if not os.path.exists('CAN_parsed'):
            os.mkdir('CAN_parsed')
       
        listMessage = self.comboboxFrameLeft.GetItems()
        if len(listMessage) == 0:
            dlg = wx.MessageDialog(self, 'No message found',
                               'Error',
                               wx.OK | wx.ICON_INFORMATION
                               )
            dlg.ShowModal()
            dlg.Destroy()
            return
        
        f = open('CAN_parsed/all_%s.log' % time.strftime('%Y-%m-%d %H%M%S', time.localtime()),'w')
        
        while self.flagDisplay:
            for item in listMessage:
                if not self.flagDisplay:
                    break
                f.write(self.ParseSingleCANMessage(item, objSSHLibrary))
                
        f.close()
        
    def OnInspectAll(self, event):
        label = self.btnShow.GetLabelText()
        if label == 'Stop':
            dlg = wx.MessageDialog(self, 'Stop inspect first',
                               'Error',
                               wx.OK | wx.ICON_INFORMATION
                               )
            dlg.ShowModal()
            dlg.Destroy()
            return
        
        label = self.btnInspectAll.GetLabelText()
        if label == 'Inspect All':
            self.flagDisplay = True
            self.btnInspectAll.SetLabel('Stop')
            self.parent.RunAsThread(self.ThreadForInspectAll)
        else:
            self.flagDisplay = False
            self.btnInspectAll.SetLabel('Inspect All')
    
       
    def OnDisplay(self, event):
        label = self.btnInspectAll.GetLabelText()
        if label == 'Stop':
            dlg = wx.MessageDialog(self, 'Stop inspect all first',
                               'Error',
                               wx.OK | wx.ICON_INFORMATION
                               )
            dlg.ShowModal()
            dlg.Destroy()
            return
        
        label = self.btnShow.GetLabelText()
        if label == 'Inspect':
            self.flagDisplay = True
            self.btnShow.SetLabel('Stop')
            self.parent.RunAsThread(self.ThreadForDispaly)
        else:
            self.flagDisplay = False
            self.btnShow.SetLabel('Inspect')
            
        
    
class FlashCGWAndATBA(wx.Panel):
    def __init__(self, parent, notebook):
        wx.Panel.__init__(self, notebook)
        self.parent = parent
        self.objConfigure = parent.objConfigure
        self.server = self.objConfigure.GetConfigValue('server', 'host')
        self.port = self.objConfigure.GetConfigValue('server', 'port')
        self.urlDownload = self.objConfigure.GetConfigValue('server', 'urlDownload')
        self.owner = None
        self.InitUI()
        self.BindEvent()
        
    def InitUI(self):
        self.log = wx.TextCtrl(self, -1,
                              style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        
        if self.parent.IsMacOS():
            wx.Log.SetActiveTarget(MyLog(self.log))
        else:
            wx.Log_SetActiveTarget(MyLog(self.log))
        
        vbox = wx.BoxSizer(wx.VERTICAL)   
        
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        if 'windows' in self.parent.strPlatform:
            witdh = 100
        elif 'linux' in self.parent.strPlatform:
            witdh = 120    
        else:
            witdh = 100 
       
        self.radioLocal = wx.RadioButton(self, -1, "local    ", style=wx.RB_GROUP)
        self.textLocalPackage = wx.TextCtrl(self, -1, "", size=(4*witdh, -1))
        self.btnLocalBrowser = wx.Button(self, -1, "Browse")#, size=(-1, -1))
        hbox.Add(self.radioLocal)
        hbox.AddSpacer(20)
        hbox.Add(self.textLocalPackage)
        hbox.AddSpacer(20)
        hbox.Add(self.btnLocalBrowser)
        vbox.AddSpacer(20)
        vbox.AddSizer(hbox)
        
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.radioArtifactory = wx.RadioButton(self, -1, "artifactory")
        self.comboboxArtifactory = wx.ComboBox(self, 200, "", (40, 50), (4*witdh, -1), [], 
                         wx.CB_DROPDOWN|wx.CB_READONLY)
        
#        self.comboboxMilestone = wx.ComboBox(self, 200, "AP", (40, 50), (witdh, -1), ['AP'], 
#                         wx.CB_DROPDOWN|wx.CB_READONLY)
        
        hbox.Add(self.radioArtifactory)
        hbox.AddSpacer(6)
        hbox.Add(self.comboboxArtifactory)
#        hbox.AddSpacer(20)
#        hbox.Add(self.comboboxMilestone)
        vbox.AddSpacer(20)
        vbox.AddSizer(hbox)
        
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.btnFlash = wx.Button(self, -1, "Start Update")#, size=(-1, -1))
        
        hbox.AddSpacer(50)
        hbox.Add(self.btnFlash)
        
        vbox.AddSpacer(20)
        vbox.AddSizer(hbox)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        vbox.AddSizer(hbox)
        vbox.AddSpacer(10)
        vbox.Add(self.log, 2, wx.EXPAND,10)  
        
        self.SetSizer(vbox)
        self.SetAutoLayout(1)
        self.parent.RunAsThread(self.InitArtifactoryComboBox)
        
        
    def BindEvent(self):
        self.Bind(wx.EVT_BUTTON, self.OnFlash, self.btnFlash)
        self.Bind(wx.EVT_BUTTON, self.OnBrowser, self.btnLocalBrowser)
        self.Bind(wx.EVT_RADIOBUTTON, self.OnArtifactorySelect, self.radioArtifactory)
        self.Bind(wx.EVT_RADIOBUTTON, self.OnLocalSelect, self.radioLocal)
        self.Bind(wx.EVT_COMBOBOX, self.OnComboBoxArtifactory, self.comboboxArtifactory)
        self.OnLocalSelect(None)
    
    def OnComboBoxArtifactory(self, event):
        self.WriteLog("Select build: %s" % self.comboboxArtifactory.GetStringSelection())
    
    def OnBrowser(self, event):
        wildcard = "tar file (*.swu)|*.swu|"     \
           "All files (*.*)|*.*"
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR
            )
        if dlg.ShowModal() == wx.ID_OK:
            self.textLocalPackage.SetValue(dlg.GetPaths()[0])
            self.WriteLog("Choose file: %s" % dlg.GetPaths()[0])
        dlg.Destroy()
        
    def OnArtifactorySelect(self, event):
        self.comboboxArtifactory.Enable(True)
        self.btnLocalBrowser.Enable(False)
        self.textLocalPackage.Enable(False)
    
    def ThreadArtifactory(self):
        buildNumber = self.comboboxArtifactory.GetStringSelection()
        try:
            pathBuild = self.GetPackageFromArtifactory(buildNumber)
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("get jenkins build failed")
            self.btnFlash.Enable(True)
            return
        try:
            self.PackageUpdate(pathBuild, 'CGW')
            self.PackageUpdate(pathBuild, 'ATBA')
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("update failed")
        self.btnFlash.Enable(True)
    
    def ThreadLocal(self):
        try:
            pathBuild = self.textLocalPackage.GetValue()
            self.PackageUpdate(pathBuild, 'CGW')
            self.PackageUpdate(pathBuild, 'ATBA')
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("update failed")
        self.btnFlash.Enable(True)
        
    def OnFlash(self, event):
        self.btnFlash.Enable(False)
        if self.comboboxArtifactory.IsEnabled():
            self.parent.RunAsThread(self.ThreadArtifactory)
        else:
            self.parent.RunAsThread(self.ThreadLocal)
        

    def OnLocalSelect(self, event):
        self.comboboxArtifactory.Enable(False)
#        self.comboboxMilestone.Enable(False)
        self.btnLocalBrowser.Enable(True)
        self.textLocalPackage.Enable(True)  
    
    def InitArtifactoryComboBox(self):
        try:
            listBuild = []
            url ='https://btnussfoart01.byton.com/artifactory/ICE-firmware/doubleDutchman-AP/build'
            objpathPackage = ArtifactoryPath(url, auth=('cavin_rui', 'cavin_rui'))
            for p in objpathPackage:
                listBuild.append(str(p).replace('https://btnussfoart01.byton.com/artifactory/ICE-firmware/doubleDutchman-AP/build/', ''))
            listBuild.reverse()
            self.comboboxArtifactory.SetItems(listBuild)
            self.comboboxArtifactory.SetSelection(0)
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("get artifacts builds failed")
    
    def GetPackageFromArtifactory(self, url):
        url ='https://btnussfoart01.byton.com/artifactory/ICE-firmware/doubleDutchman-AP/build/%s/images/package.swu' % url
        objpathPackage = ArtifactoryPath(url, auth=('cavin_rui', 'cavin_rui'))
        pathPackage = os.path.join(os.getcwd(), os.path.basename(url))
        #return pathPackage
        self.WriteLog("start downloading package.swu from artifactory")
        with objpathPackage.open() as fd:
            with open(pathPackage, "wb") as out:
                out.write(fd.read())
        return pathPackage
    
    def Login(self, host):
        try:
            user = 'byton'
            password = 'byton!'
            objSSHLibrary = SSHLibrary.SSHLibrary()
            objSSHLibrary.open_connection(host=host, port=22)
            objSSHLibrary.login(user, password)
            return objSSHLibrary
        except:
            return None


    def PackageUpdate(self, pathBuild, updateType):
        if updateType == 'CGW':
            host = '192.168.111.2'
        elif updateType == 'ATBA':
            host = '192.168.111.1'
            
        objSSHLibrary = self.Login(host)
        if objSSHLibrary is None:
            self.WriteLog("%s is down" % updateType)
            exit()
        if not os.path.exists(pathBuild):
            self.WriteLog('local file %s does not exist' % pathBuild)
            return
        
        objSSHLibrary.execute_command("rm -rf /tmp/package.swu")   
        output = objSSHLibrary.execute_command('cat /etc/issue')
        currentBuildNum = re.search(r'Linux Byton\s+(\S+)\s+', output).group(1)
        self.WriteLog("start %s update" % updateType)
        self.WriteLog("current Build Number is: %s" % currentBuildNum)
        
        sizeLocal = os.stat(pathBuild).st_size
        
        self.WriteLog("upload build package to %s, please wait for a while" % updateType)        
        objSSHLibrary.put_file(pathBuild, '/tmp/package.swu')
        
        if not str(sizeLocal) in objSSHLibrary.execute_command("ls -l /tmp/package.swu"):
            self.WriteLog("upload build package to %s failed, please check the size both of local and remote" % updateType) 
            return
        
        self.WriteLog("execute updating: sudo dutch-update.sh -u /tmp/package.swu") 
        objSSHLibrary.write("sudo dutch-update.sh -u /tmp/package.swu")
        objSSHLibrary.set_client_configuration(timeout="180 seconds")
        objSSHLibrary.read_until("MD5s match")
        self.WriteLog("Check md5 over")
        self.WriteLog("Write image to /dev/mmcblk0p1 partition")
        objSSHLibrary.read_until("Rebooting the system")
        self.WriteLog("Rebooting the system")
        objSSHLibrary.execute_command('sudo reboot')
        objSSHLibrary.close_all_connections()
        time.sleep(5)
        timeOut = 500
        while timeOut:
            objSSHLibrary = self.Login(host)
            if objSSHLibrary:
                break
            else:
                self.WriteLog("waiting for %s rebooting" % updateType)
                timeOut -= 1
                time.sleep(5)
                
               
        output = objSSHLibrary.execute_command('cat /etc/issue')
        realBuildNum = re.search(r'Linux Byton\s+(\S+)\s+', output).group(1)
        self.WriteLog("Build Number after updating is: %s" % realBuildNum)
        objSSHLibrary.close_all_connections()
        if currentBuildNum == realBuildNum:
            self.WriteLog("%s Update failed, build number not changed, current version is %s, expect is %s" % (updateType, currentBuildNum, realBuildNum))
            return
        else:
            self.WriteLog("%s update is successful" % updateType)
        
    def WriteLog(self, strLog, showDate=True):
        try:
            if showDate:
                self.log.AppendText(time.strftime('%Y-%m-%d %H:%M:%S INFO: ', time.localtime()) + str(strLog) + "\n")
            else:
                self.log.AppendText(str(strLog) + "\n")
        except:
            pass
  

class FlashFIC(wx.Panel):
    def __init__(self, parent, notebook):
        wx.Panel.__init__(self, notebook)
        self.parent = parent
        self.objConfigure = parent.objConfigure
        self.server = self.objConfigure.GetConfigValue('server', 'host')
        self.port = self.objConfigure.GetConfigValue('server', 'port')
        self.urlDownload = self.objConfigure.GetConfigValue('server', 'urlDownload')
        self.owner = None
        self.InitUI()
        self.BindEvent()
        
    def InitUI(self):
        self.log = wx.TextCtrl(self, -1,
                              style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        
        if self.parent.IsMacOS():
            wx.Log.SetActiveTarget(MyLog(self.log))
        else:
            wx.Log_SetActiveTarget(MyLog(self.log))
        
        vbox = wx.BoxSizer(wx.VERTICAL)   
        
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        if 'windows' in self.parent.strPlatform:
            witdh = 100
        elif 'linux' in self.parent.strPlatform:
            witdh = 120    
        else:
            witdh = 100 
       
        self.radioLocal = wx.RadioButton(self, -1, "local", style=wx.RB_GROUP)
        self.textLocalPackage = wx.TextCtrl(self, -1, "", size=(4*witdh, -1))
        self.btnLocalBrowser = wx.Button(self, -1, "Browse")#, size=(-1, -1))
        hbox.Add(self.radioLocal)
        hbox.AddSpacer(20)
        hbox.Add(self.textLocalPackage)
        hbox.AddSpacer(20)
        hbox.Add(self.btnLocalBrowser)
        vbox.AddSpacer(20)
        vbox.AddSizer(hbox)
        
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.radioJenkins = wx.RadioButton(self, -1, "jenkins")
        self.comboboxJenkins = wx.ComboBox(self, 200, "", (40, 50), (witdh, -1), [], 
                         wx.CB_DROPDOWN|wx.CB_READONLY)
        
        self.comboboxMilestone = wx.ComboBox(self, 200, "AP", (40, 50), (witdh/2, -1), ['AP', 'VP'], 
                         wx.CB_DROPDOWN|wx.CB_READONLY)
        
        hbox.Add(self.radioJenkins)
        hbox.AddSpacer(6)
        hbox.Add(self.comboboxMilestone)
        hbox.AddSpacer(6)
        hbox.Add(self.comboboxJenkins)

        vbox.AddSpacer(20)
        vbox.AddSizer(hbox)
        
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.btnFlash = wx.Button(self, -1, "Start Update")#, size=(-1, -1))
        
        hbox.AddSpacer(50)
        hbox.Add(self.btnFlash)
        
        vbox.AddSpacer(20)
        vbox.AddSizer(hbox)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        vbox.AddSizer(hbox)
        vbox.AddSpacer(10)
        vbox.Add(self.log, 2, wx.EXPAND,10)  
        
        self.SetSizer(vbox)
        self.SetAutoLayout(1)
        self.parent.RunAsThread(self.InitJenkinsComboBox)
        
    def BindEvent(self):
        self.Bind(wx.EVT_BUTTON, self.OnFlash, self.btnFlash)
        self.Bind(wx.EVT_BUTTON, self.OnBrowser, self.btnLocalBrowser)
        self.Bind(wx.EVT_RADIOBUTTON, self.OnJenkinsSelect, self.radioJenkins)
        self.Bind(wx.EVT_RADIOBUTTON, self.OnLocalSelect, self.radioLocal)
        self.comboboxJenkins.Bind(wx.EVT_COMBOBOX, self.OnComboBoxJenkins)
        self.comboboxMilestone.Bind(wx.EVT_COMBOBOX, self.OnComboBoxMilestone)
        self.OnLocalSelect(None)
    
    def OnComboBoxJenkins(self, event):
        self.WriteLog("Select build: %s" % self.comboboxJenkins.GetStringSelection())
        
    def OnComboBoxMilestone(self, event):
        self.WriteLog("Select milestone: %s, wait for loading builds" % self.comboboxMilestone.GetStringSelection())
        self.parent.RunAsThread(self.InitJenkinsComboBox)
    
    def OnBrowser(self, event):
        wildcard = "tar file (*.gz)|*.gz|"     \
           "All files (*.*)|*.*"
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR | wx.MULTIPLE
            )
        if dlg.ShowModal() == wx.ID_OK:
            if len(dlg.GetPaths()) == 1:
                self.textLocalPackage.SetValue(dlg.GetPaths()[0])
                self.WriteLog("Choose file: %s" % dlg.GetPaths()[0])
            elif len(dlg.GetPaths()) == 2:
                self.textLocalPackage.SetValue(dlg.GetPaths()[0] + ";" + dlg.GetPaths()[1])
                self.WriteLog("Choose files: %s" % dlg.GetPaths()[0] + " and " + dlg.GetPaths()[1])
        dlg.Destroy()
        
    def OnJenkinsSelect(self, event):
        self.comboboxJenkins.Enable(True)
        self.comboboxMilestone.Enable(True)
        self.btnLocalBrowser.Enable(False)
        self.textLocalPackage.Enable(False)
    
    def ThreadJenkins(self):
        buildNumber = self.comboboxJenkins.GetStringSelection()
        mileStone = self.comboboxMilestone.GetStringSelection()
        try:
            pathBuild = self.GetPackageFromJenkins(buildNumber, mileStone)
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("get jenkins build failed")
            self.btnFlash.Enable(True)
            return
        
        try:
            self.EthernetUpdate(pathBuild)
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("ethernet update failed")
        self.btnFlash.Enable(True)
    
    def ThreadLocal(self):
        try:
            for pathBuild in self.textLocalPackage.GetValue().split(';'):
                if not self.CheckPackage(pathBuild):
                    self.WriteLog("package %s checking failed, abort update, please check" % pathBuild)
                    self.btnFlash.Enable(True)
                    return
            pathBuild = pathBuild.replace('DRT_OTA_BASE_', 'DRT_OTA_')
            self.EthernetUpdate(pathBuild)
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("ethernet update failed")
        self.btnFlash.Enable(True)
        
    def OnFlash(self, event):
        self.btnFlash.Enable(False)
        if self.comboboxJenkins.IsEnabled():
            self.parent.RunAsThread(self.ThreadJenkins)
        else:
            self.parent.RunAsThread(self.ThreadLocal)
    
    def OnLocalSelect(self, event):
        self.comboboxJenkins.Enable(False)
        self.comboboxMilestone.Enable(False)
        self.btnLocalBrowser.Enable(True)
        self.textLocalPackage.Enable(True)  
    
    def GetJenkinsJobByMileStone(self):
        mileStone = self.comboboxMilestone.GetStringSelection()
        project = None
        if mileStone == 'AP':
            project = "ice-build-ap-stable"
        elif mileStone == 'VP':
            project = "ice-build-5.2"
        return project
    
    def InitJenkinsComboBox(self):
        try:
            objJenkins = jenkins.Jenkins("http://10.11.3.125:8080/", username="qa", password="Bytoncar")
            project = self.GetJenkinsJobByMileStone()
            job = objJenkins.get_job(project)
            listBuild = sorted([str(item) for item in job.get_build_dict().keys()])
            listBuild.reverse()
            self.comboboxJenkins.SetItems(listBuild)
            self.comboboxJenkins.SetSelection(0)
            self.WriteLog("get jenkins builds successful")
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("get jenkins builds failed")
        
    def DownloadJenkinsPakage(self, artifact, text_artifact, buildNum, mileStone):
        pathArtifact = os.path.join(os.getcwd(), artifact)
        if not os.path.exists(pathArtifact):
            self.WriteLog("downloading package from jenkins, please wait")
            text_artifact._do_download('%s.downloading' % pathArtifact)
            os.rename('%s.downloading' % pathArtifact, pathArtifact)
            sizeLocal = os.stat(pathArtifact).st_size
            sizeRemote = self.GetPackageSize(text_artifact.url)
            if not int(sizeRemote) == int(sizeLocal):
                self.WriteLog("size on jenkins: %s, size download: %s" % (sizeRemote, sizeLocal))
                self.WriteLog("package %s downloading failed, please check" % artifact)
                raise
        else:
            self.WriteLog("package %s exist, starting size checking" % artifact)
            sizeLocal = os.stat(pathArtifact).st_size
            sizeRemote = self.GetPackageSize(text_artifact.url)
            if int(sizeRemote) == int(sizeLocal):
                self.WriteLog("package %s is integrity, skip downloading" % artifact)
            else:
                self.WriteLog("package %s size on jenkins: %s, size exists: %s, check failed, re-download" % (artifact, sizeRemote, sizeLocal))
                os.remove(pathArtifact)
                return self.GetPackageFromJenkins(buildNum, mileStone)
        return pathArtifact
    
    
    def GetPackageFromJenkins(self, buildNum, mileStone):
        objJenkins = jenkins.Jenkins("http://10.11.3.125:8080/", username="qa", password="Bytoncar")
        project = self.GetJenkinsJobByMileStone()
        job = objJenkins.get_job(project)
        try:
            build = job.get_build(int(buildNum))
        except:
            self.WriteLog("build #%s not found in job '%s'" % (buildNum, mileStone))
            raise
        
        artifacts = build.get_artifact_dict()
        for artifact in artifacts.keys():
            if "OTA" in artifact and mileStone in artifact:
                text_artifact = artifacts[artifact]
                break
            
            if "OTA" in artifact and "DRT" in artifact and "BASE" not in artifact:
                text_artifact = artifacts[artifact]
                break

        pathArtifact = self.DownloadJenkinsPakage(artifact, text_artifact, buildNum, mileStone)
        
        if mileStone == 'VP':
            artifact = artifact.replace('DRT_OTA_', 'DRT_OTA_BASE_')
            text_artifact = artifacts[artifact]
            self.DownloadJenkinsPakage(artifact, text_artifact, buildNum, mileStone)
                
        return pathArtifact
    
    def GetPackageSize(self, url):
        auth = ('qa', 'Bytoncar')
        response = requests.head(url, auth=auth)
        return response.headers['Content-Length']
    
    def CheckPackage(self, filePackage):
        self.WriteLog("package exist, start checking")
        BLOCK_SIZE = 1024
        try:
            tardude = tarfile.open(filePackage)
        except:
            return False
        
        try:
            members = tardude.getmembers()
        except:
            return False
        
        for member_info in members:
            try:            
                check = tardude.extractfile(member_info.name)
                if check is None:
                    continue
                while 1:
                    data = check.read(BLOCK_SIZE)
                    if not data:
                        break
            except:
                return False
        
        tardude.close()
        return True
    
    def LoginQNX(self):
        try:
            objSSHLibrary = SSHLibrary.SSHLibrary()
            objSSHLibrary.open_connection(host='192.168.111.10', port=22)
            objSSHLibrary.set_client_configuration(timeout="20 seconds")
            objSSHLibrary.login('root', '')
            return objSSHLibrary
        except:
            return None
    
    def CheckQNXSpace(self, objSSHLibrary, retry=0):
        output = objSSHLibrary.execute_command("df -h | grep 'uda0.1B81'")
        spaceUnuse = float(re.split('\s+', output)[3].replace('G', ''))
        self.WriteLog("Unused disk:%s G" % spaceUnuse)
        if spaceUnuse < 1.5:
            self.WriteLog("Unused disk < 1.5G, remove logs")
            objSSHLibrary.execute_command("rm -rf /var/log/*/*")
            if retry == 2:
                self.WriteLog("after erase /var/log for 3 time, the unused disk < 1.5G, abort OTA update")
                raise 
            retry += 1
            self.CheckQNXSpace(objSSHLibrary, retry)
            
        else:
            self.WriteLog("Unused disk > 1.5G, continue OTA update")
    
    def GetDestBuildInfo(self, pathBuild):
        mAP = re.search(r'ICE.*_S_(\d+)_(FIC.*)_OTA.tar.gz', pathBuild)
        mVP = re.search(r'ICE.*_M_(\d+)_(FIC.*)_OTA.*.tar.gz', pathBuild)
        if mAP:
            expectBuildNum = mAP.group(1)
            expectBuildInfo = mAP.group(2)
            mileStone = 'AP'
        elif mVP:
            expectBuildNum = mVP.group(1)
            expectBuildInfo = mVP.group(2)
            mileStone = 'VP'
        else:
            self.WriteLog("can not parse %s to get build information" % pathBuild)
            
        return mileStone, expectBuildNum, expectBuildInfo
    
    def GetBytonInfo(self):
        objSSHLibrary = self.LoginQNX()
        if objSSHLibrary is None:
            self.WriteLog("QNX is down")
            return
        
        output = objSSHLibrary.execute_command('byton_info.sh')
        objSSHLibrary.close_connection()
        mVP = re.search(r'Byton Build Name \[ICE[^\_]+_M_(\d+)_(FIC[^\_]+_CN)_.*\].*\n', output)
        mAP = re.search(r'Byton Build Name \[ICE[^\_]+_(FIC[^\_]+_CN)_.*_(\d+)\].*\n', output)
        if mVP:
            realBuildNum = mVP.group(1)
            realBuildInfo = mVP.group(2)
            mileStone = 'VP'
        elif mAP:
            realBuildNum = mAP.group(2)
            realBuildInfo = mAP.group(1)
            mileStone = 'AP'
        else:
            self.WriteLog("byton info:")
            self.WriteLog(output)
            self.WriteLog("can not get build information" )
            
        return mileStone, realBuildNum, realBuildInfo
    
    def UpdateFromAP2VP(self, objSSHLibrary, pathBuild):
        pathBuild = pathBuild.replace('DRT_OTA_', 'DRT_OTA_BASE_')
        if not os.path.exists(pathBuild):
            self.WriteLog("no package %s found for update from AP to VP" % pathBuild)
            raise
        
        sizeLocal = os.stat(pathBuild).st_size
        self.WriteLog("upload build package %s to FIC, please wait for a while" % pathBuild)
        try:      
            objSSHLibrary.put_file(pathBuild, '/data/ice-ota-base-image.tar.gz')
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("upload build package to FIC failed, lost connection to FIC, please check")
        
        if not str(sizeLocal) in objSSHLibrary.execute_command("ls -l /data/ice-ota-base-image.tar.gz"):
            self.WriteLog("upload build package to FIC failed, please check the size both of local and remote") 
            return
        
        objSSHLibrary.execute_command("chmod 777 /bin/ota-update-base.sh")
        self.WriteLog("execute updating: sh /bin/ota-update-base.sh, please wait") 
        objSSHLibrary.execute_command("sh /bin/ota-update-base.sh")
        self.WriteLog("base updating is completed")
        self.WriteLog("reboot FIC(using command reset)")
        objSSHLibrary.write('reset')
        
        timeOut = 500
        while timeOut:
            objSSHLibrary = self.LoginQNX()
            if objSSHLibrary:
                break
            else:
                self.WriteLog("waiting for QNX rebooting")
                timeOut -= 1
                time.sleep(5)
             
    def EthernetUpdate(self, pathBuild):
        objSSHLibrary = self.LoginQNX()
        if objSSHLibrary is None:
            self.WriteLog("QNX is down")
            return
        
        if not os.path.exists(pathBuild):
            self.WriteLog('local file %s does not exist' % pathBuild)
            return
        
        self.CheckQNXSpace(objSSHLibrary)
        
        originalVersion = self.GetBytonInfo()
        
        objSSHLibrary.execute_command("rm -rf /data/*")
        destMileStone, expectBuildNum, expectBuildInfo = self.GetDestBuildInfo(pathBuild)
        output = objSSHLibrary.execute_command('byton_info.sh')
        self.WriteLog("original byton info")
        self.WriteLog(output, False)
        updatePath = "%s %s -> %s %s" % (originalVersion[0], originalVersion[1], expectBuildNum, destMileStone)
        self.WriteLog("start Ethernet update: %s" % updatePath)
        
        if originalVersion[0] == 'AP' and destMileStone == 'VP':
            self.UpdateFromAP2VP(objSSHLibrary, pathBuild)
        
        sizeLocal = os.stat(pathBuild).st_size
        self.WriteLog("upload build package %s to FIC, please wait for a while" % pathBuild)
        try:      
            objSSHLibrary.put_file(pathBuild, '/data/ice-ota-ethernet.tar.gz')
        except:
            self.WriteLog(traceback.format_exc())
            self.WriteLog("upload build package to FIC failed, lost connection to FIC, please check")
            
        if not str(sizeLocal) in objSSHLibrary.execute_command("ls -l /data/ice-ota-ethernet.tar.gz"):
            self.WriteLog("upload build package to FIC failed, please check the size both of local and remote") 
            return
        
        self.WriteLog("execute updating: ota-update /data/ice-ota-ethernet.tar.gz") 
        objSSHLibrary.write("ota-update /data/ice-ota-ethernet.tar.gz &")
        
        objSSHLibrary.set_client_configuration(timeout="600 seconds")
        objSSHLibrary.write("slog2info -w | grep OTA")
        objSSHLibrary.read_until("Extracting OTA image tar file")
        self.WriteLog("Extracting OTA image tar file")
        objSSHLibrary.read_until("Extracting OTA image finished.")
        self.WriteLog("Extracting OTA image finished.")
        objSSHLibrary.read_until("cp /lib64/libspmi_client.so /data")
        self.WriteLog("cp /lib64/libspmi_client.so /data")
        objSSHLibrary.read_until("swdl-powersafe -d /data/ice-ota-images -g main=gpt_main0.bin,backup=gpt_backup0.bin")
        self.WriteLog("swdl-powersafe -d /data/ice-ota-images -g main=gpt_main0.bin,backup=gpt_backup0.bin")
        objSSHLibrary.close_all_connections()
        timeOut = 500
        while timeOut:
            objSSHLibrary = self.LoginQNX()
            if objSSHLibrary:
                objSSHLibrary.close_connection()
            else:
                timeOut -= 1
                time.sleep(5)
                break
                
        timeOut = 500
        while timeOut:
            objSSHLibrary = self.LoginQNX()
            if objSSHLibrary:
                break
            else:
                self.WriteLog("waiting for QNX rebooting")
                timeOut -= 1
                time.sleep(5)
               
        mileStone, realBuildNum, realBuildInfo = self.GetBytonInfo()
        output = objSSHLibrary.execute_command('byton_info.sh')
        self.WriteLog("updated byton info")
        self.WriteLog(output, False)
        
        if not expectBuildNum == realBuildNum  or not realBuildInfo == expectBuildInfo:
            self.WriteLog("Ethernet Update failed, current version is %s, expect is %s" % (realBuildNum, expectBuildNum))
        else:
            self.WriteLog("Ethernet update %s is successful" % updatePath)
    
    def WriteLog(self, strLog, showDate=True):
        try:
            if showDate:
                self.log.AppendText(time.strftime('%Y-%m-%d %H:%M:%S INFO: ', time.localtime()) + str(strLog) + "\n")
            else:
                self.log.AppendText(str(strLog) + "\n")
        except:
            pass
        
class LogProcess(wx.Panel):
    def __init__(self, parent, noteBook):
        wx.Panel.__init__(self, noteBook)
        self.parent = parent
        self.parent.objSSHLibrary = None
        self.pathSaved = None
        self.objConfigure = parent.objConfigure
        self.noteBook = noteBook
        self.InitUI()
        self.BindEvent()
        
        
    def InitUI(self):
        self.log = wx.TextCtrl(self, -1,
                              style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        
        if self.parent.IsMacOS():
            wx.Log.SetActiveTarget(MyLog(self.log))
        else:
            wx.Log_SetActiveTarget(MyLog(self.log))
        
        listLogType = ['criticallogs', 'debug1logs', 'debug2logs', 'errorlogs', 
                      'infologs', 'noticelogs', 'shutdownlogs', 'warninglogs']

        hbox = wx.BoxSizer(wx.HORIZONTAL)

        self.radioLogType = wx.RadioBox(
                self, -1, "Choose Log Type", wx.DefaultPosition, wx.DefaultSize,
                listLogType, 2, wx.RA_SPECIFY_COLS
                )
        
        hbox.Add(self.radioLogType, 0, wx.ALIGN_LEFT, 1)
        
        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        vbox = wx.BoxSizer(wx.VERTICAL)
        label = wx.StaticText(self, -1, "Choose Log Dir:")
        
        if 'windows' in self.parent.strPlatform:
            witdh = 100
        elif 'linux' in self.parent.strPlatform:
            witdh = 120    
        else:
            witdh = 100 
            
        hbox1.Add(label, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        
        self.comboBoxLogDir = wx.ComboBox(self, 200, "", (40, 50), (witdh, -1), [], 
                         wx.CB_DROPDOWN|wx.CB_READONLY)
        hbox1.Add(self.comboBoxLogDir, 0, wx.ALIGN_CENTER, 1)
        
        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.btnLocalLog = wx.Button(self, -1, "Local Log")#, size=(-1, -1))
        self.btnGrabber = wx.Button(self, -1, "Remote Log")#, size=(-1, -1))
        vbox.AddSpacer(20)
        vbox.AddSizer(hbox1)
        vbox.AddSpacer(20)
        hbox2.AddSpacer(20)
        hbox2.Add(self.btnLocalLog, 0, wx.ALIGN_CENTER, 1)
        hbox2.AddSpacer(10)
        hbox2.Add(self.btnGrabber, 0, wx.ALIGN_CENTER, 1)
        vbox.AddSizer(hbox2)
        hbox.AddSizer(vbox)
        
        vboxSearch = wx.BoxSizer(wx.VERTICAL)
        hboxSeciton = wx.BoxSizer(wx.HORIZONTAL)
        
        if 'windows' in self.parent.strPlatform:
            witdh = 84
        elif 'linux' in self.parent.strPlatform:
            witdh = 120  
        
        self.fromDateCtrl = wx.DatePickerCtrl(self, size=(witdh, -1),
                                style=wx.DP_DROPDOWN | wx.DP_SHOWCENTURY)
            
        hboxSeciton.Add(self.fromDateCtrl)
        self.spin1 = wx.SpinButton( self, -1, wx.DefaultPosition, (-1, 23), wx.SP_VERTICAL )
        self.fromTimeCtrl = masked.TimeCtrl(
                        self, -1, name="24 hour control", fmt24hr=True, display_seconds=True,
                        spinButton = self.spin1
                        )
        
        hboxSeciton.Add(self.fromTimeCtrl)
        hboxSeciton.Add(self.spin1)
        
        label = wx.StaticText(self, -1, "to")
        hboxSeciton.Add(label, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        
        self.toDateCtrl = wx.DatePickerCtrl(self, size=(witdh, -1),
                                style=wx.DP_DROPDOWN | wx.DP_SHOWCENTURY)
        
        hboxSeciton.Add(self.toDateCtrl)
        self.spin2 = wx.SpinButton( self, -1, wx.DefaultPosition, (-1, 23), wx.SP_VERTICAL )
        self.toTimeCtrl = masked.TimeCtrl(
                        self, -1, name="24 hour control", fmt24hr=True, display_seconds=True,
                        spinButton = self.spin2
                        )
        hboxSeciton.Add(self.toTimeCtrl)
        hboxSeciton.Add(self.spin2)
        
        
        if 'linux' in self.parent.strPlatform:
            self.fromTimeCtrl.SetMinSize((80,-1))
            self.toTimeCtrl.SetMinSize((80,-1))
            
            
        self.btnSectionCut = wx.Button(self, -1, "Cut")#, size=(-1, -1))
        hboxSeciton.AddSpacer(20)
        hboxSeciton.Add(self.btnSectionCut)
        
        hboxSearch = wx.BoxSizer(wx.HORIZONTAL)
#        self.search = wx.SearchCtrl(self, size=(200, self.comboBoxLogDir.GetSize()[1]), style=wx.TE_PROCESS_ENTER)
        self.searchCombox = wx.ComboBox(
            self, 500, "", (190, 50), 
            (195, -1), [], wx.CB_DROPDOWN #|wxTE_PROCESS_ENTER
            )
        hbox.AddSpacer(10)
        hboxSearch.Add(self.searchCombox)
        
        self.btnSearch = wx.Button(self, -1, "Search")#, size=(-1, -1)) 
        hboxSearch.AddSpacer(10)
        hboxSearch.Add(self.btnSearch)
        vboxSearch.AddSizer(hboxSearch)
        vboxSearch.AddSpacer(20)
        vboxSearch.AddSizer(hboxSeciton)
        hbox.AddSizer(vboxSearch, 0, wx.ALIGN_CENTER, 1)
        
        vbox = wx.BoxSizer(wx.VERTICAL) 
        vbox.AddSizer(hbox)
        vbox.Add(self.log, 2, wx.EXPAND,10)
        
        self.SetSizer(vbox)
        self.SetAutoLayout(1)
        self.searchCombox.SetItems(self.GetKeywordsLost())
        self.searchCombox.SetValue('Search Here')
        
    def BindEvent(self):
        self.Bind(wx.EVT_BUTTON, self.OnGrabLog, self.btnGrabber) 
        self.Bind(wx.EVT_BUTTON, self.OnLocalLog, self.btnLocalLog) 
        self.Bind(wx.EVT_RADIOBOX, self.OnChooseLogType, self.radioLogType)
        self.Bind(wx.EVT_COMBOBOX, self.OnComboBoxLogDir, self.comboBoxLogDir)
        self.Bind(wx.EVT_BUTTON, self.OnSectionCut, self.btnSectionCut) 
        self.Bind(wx.EVT_BUTTON, self.OnSearch, self.btnSearch) 
        self.log.Bind(wx.EVT_CONTEXT_MENU, self.OnShowPopup)
    
    def OnShowPopup(self, event):
        pos = event.GetPosition()
        pos = self.log.ScreenToClient(pos)
        popupmenu = wx.Menu()
        item = popupmenu.Append(-1, 'Clear')
        self.Bind(wx.EVT_MENU, self.OnClearLog, item) 
        popupmenu.AppendSeparator()
        item = popupmenu.Append(-1, 'Save')
        self.Bind(wx.EVT_MENU, self.OnSaveLog, item)    
        self.log.PopupMenu(popupmenu, pos)
    
    def OnClearLog(self, event):
        self.log.Clear() 
    
    def OnSaveLog(self, event):
        dlg = wx.FileDialog(
            self, message="Save file as ...", defaultDir=os.getcwd(), 
            defaultFile="", wildcard="All files (*.*)|*.*", style=wx.SAVE
            )

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            f = open(path, 'wb')
            f.write(self.log.GetValue())
            f.close()
            self.WriteLog('log saved: "%s"' % path)
        else:
            self.WriteLog('Abort log saving') 
    
    def GetKeywordsLost(self):
        values = self.objConfigure.GetConfigValue('LOG', 'keywords')
        
        if values is None:
            self.WriteLog("Can not get configuration from setting.ini for LOG - keywords ") 
            return []
        values = values.lstrip().rstrip()
        listCommons = [item.lstrip().rstrip() for item  in values.split(',')]
        
        return listCommons
    
    def SearchInLogFile(self, searchData, pathFile):
        NotEndWithEnter = None
        f = open(pathFile, 'rb')
        countLine = 0
        while True:
            Buffer = f.read(65536)
            if not Buffer: break
            LineList = Buffer.split('\n')
            if NotEndWithEnter is not None:
                LineList[0] = NotEndWithEnter + LineList[0]
            if not Buffer.endswith('\n'):
                NotEndWithEnter = LineList.pop(-1)
            else:
                NotEndWithEnter = None
            
            for line in LineList:
                countLine += 1
                if searchData.lower() in line.lower():
                    self.WriteLog("line %s: %s" % (countLine, line), False)
                            
        f.close()
        
    def CutSectionOnLog(self):
        if not self.pathSaved:
            self.WriteLog("Need grab log first")
            return
        
        strFilePath = "%s/%s/%s" % (self.pathSaved, self.comboBoxLogDir.GetStringSelection(), self.radioLogType.GetStringSelection())
#        
        if not os.path.exists(strFilePath):
            self.WriteLog("log file '%s' does not exist" % strFilePath)
            return
        self.WriteLog("Starting cut in file: %s" % strFilePath)
        
        
        if 'windows' in self.parent.strPlatform:
            strptimeFormat = "%m/%d/%y %H:%M:%S"
        elif 'linux' in self.parent.strPlatform:
            strptimeFormat = "%a %b %d %H:%M:%S %Y"
        fromStamp = int(time.mktime(time.strptime(str(self.fromDateCtrl.GetValue()).replace("00:00:00", self.fromTimeCtrl.GetValue()), strptimeFormat)))
        toStamp = int(time.mktime(time.strptime(str(self.toDateCtrl.GetValue()).replace("00:00:00", self.toTimeCtrl.GetValue()), strptimeFormat)))
        
        if not fromStamp < toStamp:
            self.WriteLog("Cut section is illegal, end date/time is ahead of start date/time")
            return
        
        self.btnSectionCut.Enable(False)
        listToSave = self.ProcessCuttingLog(strFilePath, fromStamp, toStamp)
        if len(listToSave) == 0:
            self.WriteLog("No data to save")
            self.btnSectionCut.Enable(True)
            return
        
        dlg = wx.FileDialog(
            self, message="Save file as ...", defaultDir=os.getcwd(), 
            defaultFile="", wildcard="All files (*.*)|*.*", style=wx.SAVE
            )

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            f = open(path, 'wb')
            f.write('\n'.join(listToSave))
            f.close()
            self.WriteLog('Section cut to save: "%s"' % path)
        else:
            self.WriteLog('Abort file saving')
        self.btnSectionCut.Enable(True)
    
    def ProcessCuttingLog(self, strFilePath, fromStamp, toStamp):
        NotEndWithEnter = None
        f = open(strFilePath, 'rb')
        listToSave = []
        isFinished = False
        
        while True:
            Buffer = f.read(65536)
            if not Buffer: break
            LineList = Buffer.split('\n')
            if NotEndWithEnter is not None:
                LineList[0] = NotEndWithEnter + LineList[0]
            if not Buffer.endswith('\n'):
                NotEndWithEnter = LineList.pop(-1)
            else:
                NotEndWithEnter = None
            
            for line in LineList:
                if str(fromStamp) in line:
                    listToSave.append(line)
                elif listToSave:
                    listToSave.append(line)
                
                if str(toStamp) in line:
                    isFinished = True
                    break
            
            if isFinished:
                break
        f.close()
        
        return listToSave
    
    def OnSectionCut(self, evt):
        self.parent.RunAsThread(self.CutSectionOnLog)
#        wx.CallAfter(self.CutSectionOnLog)
    
    def OnComboBoxLogDir(self, evt):
        self.WriteLog('Select log directory: %s' % self.comboBoxLogDir.GetStringSelection())
    
    def OnChooseLogType(self, evt):
        self.WriteLog('Select log type: %s' % self.radioLogType.GetStringSelection())
    
    def OnLocalLog(self, evt):
        dlg = wx.DirDialog(self, "Choose a directory:",
                          style=wx.DD_DEFAULT_STYLE
                           )

        if dlg.ShowModal() == wx.ID_OK:
            self.pathSaved = dlg.GetPath()
            dlg.Destroy()
        else:
            dlg.Destroy()
            self.WriteLog('Abort import log')
            return
        self.WriteLog('import log directory: %s' % self.pathSaved)
        listDirsLog = os.listdir(self.pathSaved)
        
        tempList = copy.deepcopy(listDirsLog)
        for item in tempList:
            if not re.search(r'\w{3}\d{5}', item):
                listDirsLog.remove(item)
        
        if not listDirsLog:
            self.WriteLog("No log directory found")
        
        self.comboBoxLogDir.SetItems(listDirsLog)
        self.comboBoxLogDir.SetSelection(0)
    
    def OnGrabLog(self, evt):
        self.btnGrabber.Enable(False)
        listDirsLog = self.SetQNXLogDirs()
        if listDirsLog:
            self.parent.RunAsThread(self.DownloadLogFromQNX, listDirsLog)
            #wx.CallAfter(self.DownloadLogFromQNX, listDirsLog)
        else:
            self.btnGrabber.Enable(True)
            
    def DownloadLogFromQNX(self, listDirsLog):
        try:
            dlg = wx.DirDialog(self, "Choose a directory:",
                              style=wx.DD_DEFAULT_STYLE
                               )
    
            if dlg.ShowModal() == wx.ID_OK:
                dirPath = os.path.join(dlg.GetPath(), 'qnxLog.tar.gz')
                dlg.Destroy()
            else:
                dlg.Destroy()
                self.btnGrabber.Enable(True)
                self.WriteLog('Abort file transfer')
                return
            self.WriteLog('processing log files, please waiting')
            self.objSSHLibrary.execute_command("cd /var/log; rm -rf qnxLog.tar.gz; tar czvf qnxLog.tar.gz %s" % " ".join(listDirsLog))
            self.objSSHLibrary.get_file('/var/log/qnxLog.tar.gz', dirPath)
            self.UnzipTar(dirPath, os.path.dirname(dirPath))
            os.remove(dirPath)
            self.WriteLog('Log download: %s' % os.path.dirname(dirPath))
            self.pathSaved = os.path.dirname(dirPath)
        except Exception, msg:
            self.WriteLog('exception catch: %s' % str(msg))
        
        self.btnGrabber.Enable(True)
            
    def UnzipTar(self, pathTar, pathTarget):
        try:
            tar = tarfile.open(pathTar, "r:gz")
            file_names = tar.getnames()
            for file_name in file_names:
                tar.extract(file_name, pathTarget)
            tar.close()
        except Exception, msg:
            self.WriteLog('unzip file failed: %s' % str(msg))
            raise 
    
    def SetQNXLogDirs(self):
        try:
            self.objSSHLibrary = SSHLibrary.SSHLibrary()
            self.objSSHLibrary.open_connection(host='192.168.111.10', port=22)
            self.objSSHLibrary.login('root', '')
            self.WriteLog("Login 192.168.111.10(QNX) successful")
            self.objSSHLibrary.write("ls /var/log")
            output = self.objSSHLibrary.read_until_regexp("#")
            listDirsLog = re.split(r'[\n\s\t]+', output)
            
            tempList = copy.deepcopy(listDirsLog)
            for item in tempList:
                if not re.search(r'\w{3}\d{5}', item):
                    listDirsLog.remove(item)
            
            self.comboBoxLogDir.SetItems(listDirsLog)
            self.comboBoxLogDir.SetSelection(0)
            return listDirsLog
        except:
            self.WriteLog("Can not connect QNX(192.168.111.10), please check.")
            return []
    
    def OnSearch(self, evt):
        searchValue = str(self.searchCombox.GetValue())
        if searchValue == "":
            self.WriteLog("No keyword input, please check.")
            return
        
        if not self.pathSaved:
            self.WriteLog("Need importing log first")
            return
        
        strFilePath = "%s/%s/%s" % (self.pathSaved, self.comboBoxLogDir.GetStringSelection(), self.radioLogType.GetStringSelection())
        
        if not os.path.exists(strFilePath):
            self.WriteLog("log file '%s' does not exist" % strFilePath)
            return
        self.WriteLog("Starting search in file: %s" % strFilePath)
        self.searchCombox.Enable(False)
        self.btnSearch.Enable(False)
        try:
            timeArray = time.strptime(searchValue, "%Y-%m-%d %H:%M")
            searchValue = int(time.mktime(timeArray))
            self.SearchByStamp(searchValue, strFilePath)
        except:
            try:
                timeArray = time.strptime(searchValue, "%Y/%m/%d %H:%M")
                searchValue = int(time.mktime(timeArray))
                self.SearchByStamp(searchValue, strFilePath)
            except:
                self.OnDoSearch(searchValue, strFilePath)
        
    def GetSearchResult(self, searchValue, strFilePath, isStamp=True):
        if isStamp:
            for i in xrange(60):
                self.SearchInLogFile(str(searchValue+i), strFilePath)
        else:
            self.SearchInLogFile(searchValue, strFilePath)

        self.WriteLog("Search over")
        self.searchCombox.Enable(True)
        self.btnSearch.Enable(True)
        
    def SearchByStamp(self, searchValue, strFilePath):
        self.parent.RunAsThread(self.GetSearchResult, searchValue, strFilePath)
#        wx.CallAfter(self.GetSearchResult, searchValue, strFilePath)
    
    def OnDoSearch(self, searchValue, strFilePath):
        self.parent.RunAsThread(self.GetSearchResult,  searchValue, strFilePath, False)   
#        wx.CallAfter(self.GetSearchResult, searchValue, strFilePath, False)   

    def WriteLog(self, strLog, showDate=True):
        if showDate:
            self.log.AppendText(time.strftime('%Y-%m-%d %H:%M:%S INFO: ', time.localtime()) + str(strLog) + "\n")
        else:
            self.log.AppendText(str(strLog) + "\n")
        

class GateSimulator(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, -1, title,
                          pos=(-1, -1), size=(800,600))
        self.strPlatform = platform.platform().lower()
        self.objConfigure = Configure()
        self.InitUI()
        self.Show()
        self.Center()
        self.Maximize()
        self.BindEvent()
        
        
    def InitUI(self):
        if self.IsMacOS():
            icon = wx.Icon(wx.Bitmap("favicon.ico", wx.BITMAP_TYPE_ANY))
        else:
            icon = wx.IconFromBitmap(wx.Bitmap("favicon.ico", wx.BITMAP_TYPE_ANY))
        self.SetIcon(icon)
        
        self.InitMenu()
        noteBook = wx.Notebook(self, id=wx.ID_ANY, style=wx.BK_DEFAULT)
        
        '''Add other tab to here'''
        noteBook.AddPage(LogProcess(self, noteBook), "Log Process")
        noteBook.AddPage(FlashFIC(self, noteBook), "Flash FIC")
#        noteBook.AddPage(FlashCGWAndATBA(self, noteBook), "Flash CGW/ATBA")
#        noteBook.AddPage(CANSignalParser(self, noteBook), "CAN Signal Parser")
        
        
        
        noteBook.SetSelection(1)
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(noteBook,1,wx.ALL|wx.EXPAND,5)
        self.SetSizer(sizer)
        self.Layout()
    
        
    def InitMenu(self):
        menuBar = wx.MenuBar()
        menu1 = wx.Menu()
#        menu1.Append(ID_UPLOAD_FIRMWARE, "&Upload CGW", )
#        menu1.AppendSeparator()
        menu1.Append(ID_CLSOE, "&Close")
        menuBar.Append(menu1, "&File")
        
        menu2 = wx.Menu()
        menu2.Append(ID_ABOUT, "&About", )
        
        menuBar.Append(menu2, "&Help")
        self.SetMenuBar(menuBar)
    
    def IsMacOS(self):
        if "darwin" in self.strPlatform:
            return True
        return False
    
    def BindEvent(self):
        self.Bind(wx.EVT_MENU, self.OnClose, id=ID_CLSOE)
        self.Bind(wx.EVT_MENU, self.OnAbout, id=ID_ABOUT)
    
    def RunAsThread(self, method, *argvs):
        thread = threading.Thread(target=method, args=argvs)
        thread.start()
        return thread
        
        
    def OnClose(self, event):  
        try:
            self.objSSHLibrary.close_all_connections()
        except:
            pass
        
        self.Close()
    
    def OnAbout(self, event):  
        info = None
        if self.IsMacOS():
            info = wx.adv.AboutDialogInfo()
        else:
            info = wx.AboutDialogInfo()
            
        info.Name = "Byton ICE toolset"
        info.Version = "1.0.0"
        info.Copyright = u"BYTON 2018"
        info.Description = wordwrap(
            "This is a toolset from byton ICE",
            350, wx.ClientDC(self))
        info.WebSite = ("https://www.byton.cn/", "Byton home page")
        #info.Developers = ["Cavin Rui   cavin.rui@byton.com"]

        if self.IsMacOS():
            wx.adv.AboutBox(info) 
        else:
            wx.AboutBox(info) 
    
        
        
if __name__ == "__main__":
    try:
        app = wx.App()
        frame = GateSimulator(None, "Byton ICE Toolset")
        app.MainLoop()
    except Exception, msg:
        print msg

