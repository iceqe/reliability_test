1. Log Process
	Short introduction:
	This tool could grab log files from QNX system and be given some search /cut operations.

	Setup Steps:
	1.  Setup and config FIC.
	2.  Make sure the PC/VM where this tool run can be connected directly to QNX(192.168.111.10)

	Function introduction:

	Remote log:  download log package to working PC/VM
	Local log:  import local log to working PC/VM
	Search: search keyword from log and display result on tool
	Cut: Capture log by time section  and save it to working PC/VM
  
2. Flash FIC
	Short introduction:	   
	This tool allows user to update FIC via OTA from local or remote Jenkins.
	
	Setup Steps:
	1.  Setup and config FIC.
	2.  Make sure the PC/VM where this tool run can be connected directly to QNX(192.168.111.10)

	Function introduction:	   
	1. local
	   > Choose the target package which you have downloaded to local then click "Start Update". 
		 The package integrity check will be performed by the tool automatically. There will be tips bounced out for users when any issues encountered during the check.

	2. Jenkins
	   > Make sure the PC/VM can access to Jenkins: http://10.11.3.125:8080
	     If you have the access to Jenkins, all the available FIC builds will be displayed on the tool.
	   > Choose your target build and then click "Start Update".      
		 If the target build package has been stored on local PC/VM, the tool will check the package integrity firstly. 
		 If the target package file size doesn't meet the exception, the tool will remove the incomplete package and then download the target package from Jenkins again.
		 If the target package file size is correct, the tool will upload the package to FIC and then perform OTA update by itself.
		 If there is no any package on local, the tool will get the package from Jenkins and OTA update.