# -*- coding: utf-8 -*-
'''
Created on Dec 5, 2018

@author: cavin.rui
'''
import time
from selenium import webdriver
import logger
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
import sys
import os


logging = logger.InitLogging()

def GetKibanaChart():
    now = time.strftime('%Y-%m-%d', time.localtime())
    filePath = 'kibana.png'
    try:
        driver = webdriver.Firefox()
        driver.maximize_window()
        driver.get('''http://10.11.3.132:5601/app/kibana#/dashboard/a6355cd0-ecde-11e8-bcfa-df4e2312e598?_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:'2018-11-19T16:00:00.000Z',mode:absolute,to:'%s'))&_a=(description:Soak_Dashboard,filters:!(),fullScreenMode:!f,options:(darkTheme:!f,hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(),gridData:(h:15,i:'1',w:24,x:0,y:0),id:'7f766c00-ec7b-11e8-bcfa-df4e2312e598',panelIndex:'1',type:visualization,version:'6.3.2'),(embeddableConfig:(),gridData:(h:15,i:'2',w:24,x:24,y:0),id:e3632d70-ecdf-11e8-bcfa-df4e2312e598,panelIndex:'2',type:visualization,version:'6.3.2'),(embeddableConfig:(),gridData:(h:7,i:'3',w:48,x:0,y:15),id:'84af7650-f205-11e8-a366-dd762821d7ac',panelIndex:'3',type:visualization,version:'6.3.2')),query:(language:lucene,query:''),timeRestore:!f,title:Soak_Dashboard,viewMode:view)''' % now)
        driver.implicitly_wait(60)
        driver.find_element_by_class_name('metric-container')
        driver.save_screenshot(filePath)
        driver.close()
        logging.info('saved %s successfully' % filePath)
        return filePath
    except:
        logging.info('saved %s failed' % filePath)
        return None
        

def MailTo(sender, to, subject):
    attachment = GetKibanaChart()
    if attachment is None:
        logging.info("no picture to send")
        exit()
    msgRoot = MIMEMultipart('related')
    msgRoot['Subject'] = subject
    msgRoot['From'] = sender
    msgRoot['To'] = to
    
    html = """\
        <p><br/>
            <img src="cid:image1">
        </p>
    """
    msgHtml = MIMEText(html, 'html')
    img = open(attachment, 'rb').read()
    msgImg = MIMEImage(img, 'png')
    msgImg.add_header('Content-ID', '<image1>')
    msgImg.add_header('Content-Disposition', 'inline', filename=attachment)
    msgRoot.attach(msgHtml)
    msgRoot.attach(msgImg)
    try:
        s = smtplib.SMTP('10.11.3.132', '25')
        s.sendmail(msgRoot['From'], msgRoot['From'], msgRoot.as_string())
        s.quit()
        logging.info('mail sent successfully')
        
    except Exception, msg:
        logging.info('Error:%s' % msg)
        logging.info('mail sent failed')
    os.remove(attachment)
    
if __name__ == "__main__":
    usage = '''Usage: %s <sender> <to> <subject>
       sender: who send this
       to: who receive this
       subject: mail subject
       For example: %s cavin.rui@byton.com cavin.rui@byton.com "Kibana Chart"
        ''' % (__file__, __file__)
        
    try:
        if len(sys.argv) == 4:
            MailTo(sys.argv[1], sys.argv[2], sys.argv[3])
        else:
            logging.info(usage)
    except Exception, msg:
        logging.info(msg)
        logging.info(usage)