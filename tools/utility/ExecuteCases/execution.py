# -*- coding: utf-8 -*-
'''
Created on Nov 28, 2018

@author: cavin.rui

'''
import traceback
import logging
import time
import os
import sys
import threading
isTimeout = True

def main(pathCase, caseExecution, durationTimes):
    try:
        durationTimes = int(durationTimes)
        ExecuteCaseByCount(pathCase, caseExecution, durationTimes)
    except:
        durationTimes = DurationConversion(durationTimes)
        ExecuteCaseByBuration(pathCase, caseExecution, durationTimes)
    
def ExecuteCaseByCount(pathCase, caseExecution, count): 
    needMergedOutput = "" 
    currentCount = 0
    while not currentCount == count:
        command = "robot -o output%s.xml -r None -l None %s %s " % (currentCount, caseExecution, pathCase)
        needMergedOutput += "output%s.xml " % currentCount
        os.system(command)
        currentCount += 1
    
    command = "rebot -R %s" % needMergedOutput
    os.system(command)
    logger.warn("report has generated, please see report.html")
    for i in xrange(count):
        os.remove("output%s.xml" % i)

def ThreadExecuteCaseByBuration(pathCase, caseExecution):
    global isTimeout
    currentCount = 0
    needMergedOutput = ""
    while isTimeout:
        command = "robot -o output%s.xml -r None -l None %s %s " % (currentCount, caseExecution, pathCase)
        needMergedOutput += "output%s.xml " % currentCount
        os.system(command)
        currentCount += 1
    
    command = "rebot -R %s" % needMergedOutput
    os.system(command)
    logger.warn("report has generated, please see report.html")
    for i in xrange(currentCount):
        os.remove("output%s.xml" % i)
    isTimeout = True
    
def ExecuteCaseByBuration(pathCase, caseExecution, durationTimes):   
    threading.Thread(target=ThreadExecuteCaseByBuration, args=(pathCase, caseExecution)).start()
    time.sleep(durationTimes)
    global isTimeout
    isTimeout = False

def DurationConversion(durationTimes):
    if 'ms' in durationTimes.lower():
        durationTimes = float(durationTimes.replace('ms', ''))
        durationTimes = durationTimes * 60
    elif 'hs' in durationTimes.lower():
        durationTimes = float(durationTimes.replace('hs', ''))
        durationTimes = durationTimes * 3600
    else:
        logger.error("Can not convert duration into second")
        raise
    
    return durationTimes

def InitLogging():
    logger = logging.getLogger()
    logger.setLevel(logging.WARN) 
    rq = time.strftime('%Y%m%d', time.localtime(time.time()))
    logPath = os.path.join(os.path.dirname(__file__), 'Logs')
    if not os.path.exists(logPath):
        os.mkdir(logPath)
    logFile = os.path.join(logPath, rq + '.log' )
        
    handler = logging.FileHandler(logFile, mode='a')
    handler.setLevel(logging.INFO)  
    formatter = logging.Formatter("%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logger.addHandler(console)
    
    return logger



logger = InitLogging()
if __name__ == '__main__':
    try:
        usage = '''Usage: %s <path_testcase> <testcase> <duration|times>
       path_testcase: where cases located
       testcase: testcase to execute, like "-i ICE_100", "testSuite.test1"
       duration: execution duration, like 10hs(10 hours), 600ms(600 minutes)
       times: execution times, lime 1000, 2000
       For example: %s C:/Users/cavin.rui/Documents/03Code/git/ice-test/fic/test_suites "-i DRT" 100
                    %s C:/Users/cavin.rui/Documents/03Code/git/ice-test/fic/test_suites "-t stf.stf01" 10ms
        ''' % (__file__, __file__, __file__)
        if len(sys.argv) == 4:
            pathCase = sys.argv[1]
            CaseExecution = sys.argv[2]
            durationTimes = sys.argv[3]
        else:
            raise
        main(pathCase, CaseExecution, durationTimes)
        
    except Exception, msg:
        logger.error(traceback.format_exc())
        logger.warn(msg)
        logger.warn(usage)