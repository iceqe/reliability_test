# -*- coding: utf-8 -*-
'''
Created on 2018-9-27

@author: cavin.rui
'''

from elasticsearch import Elasticsearch
from elasticsearch import helpers
import re
import time
import sys

url = 'http://10.11.3.132:9200/'
#url = 'http://10.13.132.15:9200/'
INDEX = 'qnx_info_cavin'
TYPE_DOC = "QNX_Record"

es = Elasticsearch([url])
try:
    countExist = es.count(INDEX, TYPE_DOC)['count']
except:
    countExist = 0

def InitIndex():
    mapping = {
        "properties": {  
            'DateTime.DateTime': {
                    "type":"date",
                    "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
            },
            #'DateTime.Time': {
            #        "type":"text"
            #},
            'TestSetup.Number': {
                    "type":"integer"
            },
            'TestSetup.ControlIP': {
                    "type":"ip"
            },
            'TestSetup.DeviceType': {
                    "type":"text"
            },
            'TestSetup.DeviceIP': {
                    "type":"ip"
            },
            'TestSetup.RemoteAccessPort': {
                    "type":"integer"
            },
            'TestSetup.Data1': {
                    "type":"text"
            },
            'TestSetup.Data2': {
                    "type":"text"
            },
            'TestBuild.Branch': {
                    "type":"text"
            },
            'TestBuild.Number': {
                    "type":"integer"
            },
            'TestBuild.GitHash': {
                    "type":"text"
            }, 
            'VNT.Number': {
                    "type":"integer"
            },
            'TestBuild.HardwareVersion': {
                    "type":"text"
            }, 
            'TestBuild.Milestone': {
                    "type":"text"
            },
            'TestBuild.ReleaseInfo': {
                    "type":"text"
            }, 
            'Fstep': {
                    "type":"text"
            },
            'TestType.Type': {
                    "type":"text"
            }, 
            'TestType.RuntimeHours': {
                    "type":"integer"
            },
            'TestType.Scenario': {
                    "type":"text"
            },
            'TestType.Data1': {
                    "type":"text"
            },
            'System.LinuxTimestamp': {
                    "type":"integer"
            }, 
            'System.OverallCPU': {
                    "type":"integer"
            },
            'System.OverallMemory': {
                    "type":"integer"
            },
            'System.FreeMemory': {
                    "type":"integer"
            },
            'System.IdleCPU': {
                    "type":"integer"
            }, 
            'System.Space': {
                    "type":"integer"
            },
            'SystemCrashes.KernelCrashes': {
                    "type":"text"
            },
            'SystemReboots.DeviceReboots': {
                    "type":"text"
            },
            'TotalRunning.Processes': {
                    "type":"integer"
            }, 
            'Dashboard.PID': {
                    "type":"text"
            },
            'Dashboard.Process': {
                    "type":"text"
            },           
            'Dashboard.Cycles': {
                    "type":"text"
            },            
            'Dashboard.CPU': {
                    "type":"text"
            }, 
            'Dashboard.CPU%': {
                    "type":"integer"
            },            
            'Dashboard.MemoryKB': {
                    "type":"integer"
            },            
            'Dashboard.Memory%': {
                    "type":"integer"
            }, 
            'Dashboard.ProcessRestart': {
                    "type":"text"
            }, 
            'BytonLogger.PID': {
                    "type":"text"
            },
            'BytonLogger.Process': {
                    "type":"text"
            },           
            'BytonLogger.Cycles': {
                    "type":"text"
            },            
            'BytonLogger.CPU': {
                    "type":"text"
            }, 
            'BytonLogger.CPU%': {
                    "type":"integer"
            },            
            'BytonLogger.MemoryKB': {
                    "type":"integer"
            },            
            'BytonLogger.Memory%': {
                    "type":"integer"
            }, 
            'BytonLogger.ProcessRestart': {
                    "type":"text"
            }, 
            'Powerd.PID': {
                    "type":"text"
            },
            'Powerd.Process': {
                    "type":"text"
            },           
            'Powerd.Cycles': {
                    "type":"text"
            },            
            'Powerd.CPU': {
                    "type":"text"
            }, 
            'Powerd.CPU%': {
                    "type":"integer"
            },            
            'Powerd.MemoryKB': {
                    "type":"integer"
            },            
            'Powerd.Memory%': {
                    "type":"integer"
            }, 
            'Powerd.ProcessRestart': {
                    "type":"text"
            },  
            'Reserve1.PID': {
                    "type":"text"
            },
            'Reserve1.Process': {
                    "type":"text"
            },           
            'Reserve1.Cycles': {
                    "type":"text"
            },            
            'Reserve1.CPU': {
                    "type":"text"
            }, 
            'Reserve1.CPU%': {
                    "type":"text"
            },            
            'Reserve1.MemoryKB': {
                    "type":"text"
            },            
            'Reserve1.Memory%': {
                    "type":"text"
            }, 
            'Reserve1.ProcessRestart': {
                    "type":"text"
            },      
            'Reserve2.PID': {
                    "type":"text"
            },
            'Reserve2.Process': {
                    "type":"text"
            },           
            'Reserve2.Cycles': {
                    "type":"text"
            },            
            'Reserve2.CPU': {
                    "type":"text"
            }, 
            'Reserve2.CPU%': {
                    "type":"text"
            },            
            'Reserve2.MemoryKB': {
                    "type":"text"
            },            
            'Reserve2.Memory%': {
                    "type":"text"
            }, 
            'Reserve2.ProcessRestart': {
                    "type":"text"
            },                                
        }
    }
    
    if es.indices.exists(INDEX):
        PrintLog('clean exists mapping')
        es.indices.delete(INDEX)
    
    PrintLog('create a new mapping')
    es.indices.create(INDEX)
    es.indices.put_mapping(index=INDEX, doc_type=TYPE_DOC, body=mapping)
    PrintLog('new mapping created')

def PrintLog(strLog):
    print (time.strftime('%Y-%m-%d %H:%M:%S INFO: ', time.localtime()) + str(strLog))
 
def GetDateTime(item):
    item = item.replace('Date And Time: ', '')
    item = re.sub(r'\..*', '', item)
    strRet = item.split(' ')
    timeArray = time.strptime(item, "%Y-%m-%d %H:%M:%S")
    timeStamp = int(time.mktime(timeArray))
    return (strRet[0], strRet[1], str(timeStamp))

def SplitItem(item):
    listItem = re.split(r'\s+', item)
    return listItem

def GetCPUInfo(item):
    listItem = SplitItem(item)
    usage = float(listItem[2].replace('%', '')) + float(listItem[4].replace('%', ''))
    return int(usage), int(100-usage)

def GetMemoryInfo(item):
    listItem = SplitItem(item)
    return int(listItem[1].replace('M', '').replace('k', '')), int(listItem[3].replace('M', '').replace('k', ''))

def GetSpaceInfo(item):
    listItem = SplitItem(item)
    return int(listItem[4].replace('%', ''))

def GetProcessesInfo(item):
    listItem = SplitItem(item)
    return int(listItem[1])


def GetProcessInfo(listItem, itemIndex):
    if itemIndex == 1:
        return int(listItem[0]), int(listItem[5].replace('M', '').replace('k', '')), int(listItem[6].replace('%', ''))
    else:
        return int(listItem[2].replace('%', ''))

def GetOneAction(listRecord):
    CPUDashboardPercent = 0
    CPUBytonLoggerPercent = 0
    CPUPowerdPercent = 0
    OverallCPU = 0
    IdleCPU = 0
    OverallMemory = 0
    FreeMemory = 0
    Processes = 0
    for item in listRecord:
        if 'Date And Time' in item:
            strDate, strTime, LinuxTimestamp = GetDateTime(item)
        elif 'CPU states' in item:
            OverallCPU, IdleCPU = GetCPUInfo(item)
        elif 'Memory' in item:
            OverallMemory, FreeMemory = GetMemoryInfo(item)
        elif '/dev/disk/uda' in item:
            Space = GetSpaceInfo(item) 
        elif 'Processes' in item:
            Processes = GetProcessesInfo(item) 
        elif 'Dashboard' in item:
            listItem = SplitItem(item)
            itemIndex = listItem.index('Dashboard')
            if itemIndex == 1:
                pidDashboard, MemoryKBDashboard, MemoryDashboardPercent = GetProcessInfo(listItem, itemIndex)
            else:
                CPUDashboardPercent += GetProcessInfo(listItem, itemIndex)
        elif 'BytonLogger' in item:
            listItem = SplitItem(item)
            itemIndex = listItem.index('BytonLogger')
            if itemIndex == 1:
                pidBytonLogger, MemoryKBBytonLogger, MemoryBytonLoggerPercent = GetProcessInfo(listItem, itemIndex)
            else:
                CPUBytonLoggerPercent += GetProcessInfo(listItem, itemIndex)
        elif 'powerd' in item:
            listItem = SplitItem(item)
            itemIndex = listItem.index('powerd')
            if itemIndex == 1:
                pidPowerd, MemoryKBPowerd, MemoryPowerdPercent = GetProcessInfo(listItem, itemIndex)
            else:
                CPUPowerdPercent += GetProcessInfo(listItem, itemIndex)
    
    global countExist
    countExist += 1
    action = {"_index": INDEX,
                  "_type": TYPE_DOC,
                  "_id": countExist,
                  "_source": {
                                'DateTime.DateTime': strDate + " " + strTime,
#                                'DateTime.Time': strTime,
                                'TestSetup.Number': '',
                                'TestSetup.ControlIP': '192.168.11.69',
                                'TestSetup.DeviceType': '',
                                'TestSetup.DeviceIP': '192.168.11.10',
                                'TestSetup.RemoteAccessPort': '',
                                'TestSetup.Data1': '',
                                'TestSetup.Data2': '',
                                'TestBuild.Branch': '',
                                'TestBuild.Number': '',
                                'TestBuild.GitHash': '', 
                                'VNT.Number': '',
                                'TestBuild.HardwareVersion': '', 
                                'TestBuild.Milestone': '',
                                'TestBuild.ReleaseInfo': '', 
                                'Fstep': '',
                                'TestType.Type': '', 
                                'TestType.RuntimeHours': '',
                                'TestType.Scenario': '',
                                'TestType.Data1': '',
                                'System.LinuxTimestamp': LinuxTimestamp, 
                                'System.OverallCPU': OverallCPU,
                                'System.OverallMemory': OverallMemory,
                                'System.FreeMemory': FreeMemory,
                                'System.IdleCPU': IdleCPU, 
                                'System.Space': Space,
                                'SystemCrashes.KernelCrashes': '',
                                'SystemReboots.DeviceReboots': '',
                                'TotalRunning.Processes': Processes, 
                                'Dashboard.PID': pidDashboard,
                                'Dashboard.Process': 'Dashboard',           
                                'Dashboard.Cycles': '',            
                                'Dashboard.CPU': '', 
                                'Dashboard.CPU%': CPUDashboardPercent,            
                                'Dashboard.MemoryKB': MemoryKBDashboard,            
                                'Dashboard.Memory%': MemoryDashboardPercent, 
                                'Dashboard.ProcessRestart': '', 
                                'BytonLogger.PID': pidBytonLogger,
                                'BytonLogger.Process': 'BytonLogger',           
                                'BytonLogger.Cycles': '',            
                                'BytonLogger.CPU': '', 
                                'BytonLogger.CPU%': CPUBytonLoggerPercent,            
                                'BytonLogger.MemoryKB': MemoryKBBytonLogger,            
                                'BytonLogger.Memory%': MemoryBytonLoggerPercent, 
                                'BytonLogger.ProcessRestart': '', 
                                'Powerd.PID': pidPowerd,
                                'Powerd.Process': 'powerd',           
                                'Powerd.Cycles': '',            
                                'Powerd.CPU': '', 
                                'Powerd.CPU%': CPUPowerdPercent,            
                                'Powerd.MemoryKB': MemoryKBPowerd,            
                                'Powerd.Memory%': MemoryPowerdPercent, 
                                'Powerd.ProcessRestart': '',  
                                'Reserve1.PID': '',
                                'Reserve1.Process': '',           
                                'Reserve1.Cycles': '',            
                                'Reserve1.CPU': '', 
                                'Reserve1.CPU%': '',            
                                'Reserve1.MemoryKB': '',            
                                'Reserve1.Memory%': '', 
                                'Reserve1.ProcessRestart': '',      
                                'Reserve2.PID': '',
                                'Reserve2.Process': '',           
                                'Reserve2.Cycles': '',            
                                'Reserve2.CPU': '', 
                                'Reserve2.CPU%': '',            
                                'Reserve2.MemoryKB': '',            
                                'Reserve2.Memory%': '', 
                                'Reserve2.ProcessRestart': ''
                             }
                }
    return action

def GetRecord(strPath):
    try:
        f = open(strPath, 'rb')
    except Exception, msg:
        raise msg
    
    line = f.readline()
    listRecord = []
    while line: 
        line = line.strip().strip('\x1b[K')
        if "Date And Time:" in line:
            if listRecord:
                yield GetOneAction(listRecord)
                listRecord = []
            listRecord.append(line)
        elif "CPU states:" in line:
            listRecord.append(line)
        elif "Memory:" in line:
            listRecord.append(line)
        elif "/dev/disk/uda" in line:
            listRecord.append(line)
        elif "BytonLogger" in line:
            listRecord.append(line)
        elif "Dashboard" in line:
            listRecord.append(line)
        elif "powerd" in line:
            listRecord.append(line)
        elif "Processes:" in line:
            listRecord.append(line)
        
        line = f.readline()
    f.close()
    
def BulkData(strPath):
    PrintLog("starting process raw data")
    actions = list(GetRecord(strPath))
    PrintLog("starting bulk")
    helpers.bulk(es, actions)
    PrintLog("process completed")
    PrintLog("there are %s items in elasticsearch now" % countExist)
    
if __name__ == "__main__":
    usage = """Usage: 1. %s -i --- initiate elasticsearch database, this will clean all data and create mapping
                                 2. %s -b <path of ficMetrics.txt> --- bulk data into elasticsearch
        """ % (__file__, __file__)
        
    try:
        if sys.argv[1] == '-i':
            InitIndex()
        elif sys.argv[1] == '-b':
            BulkData(sys.argv[2])
        else:
            PrintLog(usage)
    except Exception, msg:
        PrintLog(msg)
        PrintLog(usage) 

    
    
