# -*- coding: utf-8 -*-
'''
Created on Dec 5, 2018

@author: cavin.rui
'''
import logging
import time
import os


def InitLogging():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO) 
    rq = time.strftime('%Y%m%d', time.localtime(time.time()))
    logPath = os.path.join(os.path.dirname(__file__), 'Logs')
    if not os.path.exists(logPath):
        os.mkdir(logPath)
    logFile = os.path.join(logPath, rq + '.log' )
        
    handler = logging.FileHandler(logFile, mode='a')
    handler.setLevel(logging.INFO)  
    formatter = logging.Formatter("%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logger.addHandler(console)
    
    return logger

