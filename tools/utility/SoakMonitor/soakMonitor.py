# -*- coding: utf-8 -*-
'''
Created on Dec 18, 2018
Introduction: for first running, it maybe meet exception:
requests.exceptions.ConnectionError: HTTPSConnectionPool(host='locomotive.lab.byton.io', port=443): Max retries exceeded with url: /indexction.VerifiedHTTPSConnection object at 0x75dee8f0>: Failed to establish a new connection: [Errno -2] Name or service not known',))

the solution is: 
step 1: sudo vi /etc/hosts
step 2: add 172.28.8.18     locomotive.lab.byton.io, and save it.

@author: cavin.rui
'''
from elasticsearch import Elasticsearch
import datetime
from testrail import TestRail
import logger
import time
import traceback
logging = logger.InitLogging()
logging.setLevel('WARN')
timedeltaHours = 2 

#--------------------------------------------------------------------
#configuration for test rail 
PLAN_OR_RUN_NAME = 'Automation Update - Performance and Reliability' # test plan/run name
ENTRY_NAME = 'FIC - Performance and Reliability' # test entry name
FIC_VERISON = '67'                         # FIC version
CGW_VERISON = '2783'                       # CGW version
DBC_VERISON = 'F35.04'                     # FBC version

user = 'tony.ni@byton.com'                        
password = "byton123"
urlTestRail='https://locomotive.lab.byton.io'
#--------------------------------------------------------------------

#--------------------------------------------------------------------
#configuration for Kibana
url = 'http://10.11.3.132:9200/'
INDEX = 'qnx_info_dashboard_design'
TYPE_DOC = "QNX_Record_dashboard_design"
#--------------------------------------------------------------------

es = Elasticsearch([url])

objTestRail = TestRail(url=urlTestRail, email=user, key=password)
customInfo = {'custom_fic_version_results': FIC_VERISON,
               'custom_cgw_version_results': CGW_VERISON,
               'custom_dbc_version_results': DBC_VERISON}

def GetESData():
    fromTime = (datetime.datetime.now() - datetime.timedelta(hours=timedeltaHours)).strftime('%Y-%m-%d %H:%M:%S')
    toTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    res = es.search(index=INDEX, body={"query": {"range": {  
                "DateTime.DateTime": {
                    "gte":fromTime, 
                    "lte":toTime,
                }}}, 'size':10000}) #max size is 10000
    return res['hits']['hits']

def CheckLimit(data, resouce):
    try:
        for hit in data:
            if float(hit['_source'][resouce]) > 30:
                strLog = "meet high %s %s at %s " % (resouce, hit['_source'][resouce], hit['_source']['DateTime.DateTime'])
                logging.error(strLog)
                return strLog
    except:
        logging.warn(traceback.format_exc())
        return False
    return True 

def ParseDashboardCPU(data):
    return CheckLimit(data, 'Dashboard.CPU%')

def ParseDashboardMemoryUsage(data):
    return CheckLimit(data, 'Dashboard.Memory%')

def ParseOverAllCPU(data):
    return CheckLimit(data, 'System.OverallCPU')

def ParseOverAllMemory(data):
    try:
        for hit in data:
            usedMemory = float(hit['_source']['System.OverallMemory']) - float(hit['_source']['System.FreeMemory'])
            if  usedMemory > 3000:
                strLog = "meet high System Memory %s at %s " % (usedMemory, hit['_source']['DateTime.DateTime'])
                logging.error(strLog)
                return strLog
    except:
        logging.error(traceback.format_exc())
        return False
    return True 

def ParseSpaceUtilization(data):
    try:
        for hit in data:
            if float(hit['_source']['System.Space']) < 50:
                strLog = "meet high System Space %s at %s " % (hit['_source']['System.Space'], hit['_source']['DateTime.DateTime'])
                logging.error(strLog)
                return strLog
    except:
        logging.error(traceback.format_exc())
        return False
    return True 

def ParseBytonLoggerCPU(data):
    return CheckLimit(data, 'BytonLogger.CPU%')

def ParseBytonLoggerMemoryUsage(data):
    return CheckLimit(data, 'BytonLogger.Memory%')

def ParsePowerdCPU(data):
    return CheckLimit(data, 'Powerd.CPU%')

def ParsePowerdMemoryUsage(data):
    return CheckLimit(data, 'Powerd.Memory%')

def ParseBytonCgwManageCPU(data):
    return CheckLimit(data, 'BytonCgwManage.CPU%')

def ParseBytonCgwManageUsage(data):
    return CheckLimit(data, 'BytonCgwManage.Memory%')

def GetPorjectID(projectName='ICE - Integrated QE'):
    for project in objTestRail.projects():
        if project.name == projectName:
            return project.id
    logging.error("no id found for project: %s" % projectName)
    return 0


def GetTestRun():
    for run in objTestRail.runs():
        if PLAN_OR_RUN_NAME in run.name:
            return list(objTestRail.tests(objTestRail.run(run.id)))
            
    return None    
        
def GetTests():
    objTestRail.set_project_id(GetPorjectID()) 
    for plan in objTestRail.plans():
        if PLAN_OR_RUN_NAME in plan.name:
            for entry in plan.entries:
                list(entry.runs)
                if entry.name == ENTRY_NAME:
                    return objTestRail.tests(entry.runs[0])
    listTest = GetTestRun()
    if listTest is None:
        logging.error("no test found for partial plan: %s entry: %s" % (PLAN_OR_RUN_NAME, ENTRY_NAME))
        return None
    else:
        return listTest 

def SleepForTimedeltaHours():
    logging.warn("waiting for %s hours" % timedeltaHours)
    time.sleep(3600 * timedeltaHours)

def UpdateTestStatus(test, status):
    try:
        if status is True:
            status = "Passed"
            comment = "Result added by Reliability Automation"
        else:
            comment = "Result added by Reliability Automation, " + str(status)
            status = "Failed"
            
        result = objTestRail.result()
        result._content = customInfo
        result.test = test
        result.status = objTestRail.status(status)
        result.comment = str(comment)
        objTestRail.add(result)
        logging.warn("update test result for test %s successful" % test.title)
    except:
        logging.warn(traceback.format_exc())
        logging.warn("update test result failed for test %s" % test.title)

def UpdateSingleTest(tests, isPassDashboardCPU, isPassDashboardMemoryUsage, isPassOverAllCPU, \
                     isPassOverAllMemory, isPassBytonLoggerCPU, isPassBytonLoggerMemoryUsage,\
                     isPassPowerdCPU, isPassPowerdMemoryUsage, isPassSpaceUtilization,\
                     isPassBytonCgwManageCPU, isPassBytonCgwManageUsage, hours):
    hours = 2 * hours
    
    for test in tests:
        if test.title == "CPU Performance - Dashboard %s hours" % hours:
            UpdateTestStatus(test, isPassDashboardCPU)
        elif test.title == "Memory Utilization - Dashboard %s hours" % hours:
            UpdateTestStatus(test, isPassDashboardMemoryUsage)
        elif test.title == "CPU Utilization - Overall System %s hours" % hours:
            UpdateTestStatus(test, isPassOverAllCPU)
        elif test.title == "Memory Utilization - Overall System %s hours" % hours:
            UpdateTestStatus(test, isPassOverAllMemory)
        elif test.title == "CPU Performance - BytonLogger %s hours" % hours:
            UpdateTestStatus(test, isPassBytonLoggerCPU)
        elif test.title == "Memory Utilization - BytonLogger %s hours" % hours:
            UpdateTestStatus(test, isPassBytonLoggerMemoryUsage)
        elif test.title == "CPU Performance - PowerD %s hours" % hours:
            UpdateTestStatus(test, isPassPowerdCPU)
        elif test.title == "Memory Utilization - PowerD %s hours" % hours:
            UpdateTestStatus(test, isPassPowerdMemoryUsage)
        elif test.title == "Monitor FIC Disk space %s hours" % hours:
            UpdateTestStatus(test, isPassSpaceUtilization)
        elif test.title == "CPU Performance - BytonCGWManager %s hours" % hours:
            UpdateTestStatus(test, isPassBytonCgwManageCPU)
        elif test.title == "Memory Utilization - BytonCGWManager %s hours" % hours:
            UpdateTestStatus(test, isPassBytonCgwManageUsage)
                         
        
            
            
            
def UpdateResultToTestRail():
    if not timedeltaHours == 2:
        logging.error("timedeltaHours should be 2 hours, otherwise, the test '2 Hour Reliability run - Sanity' can not get result")
        return
        
    listTests = GetTests()
    if listTests is None:
        return
    
    countIteration = 0
    performancePass = True
    while True:
        countIteration += 1
        SleepForTimedeltaHours()
        logging.warn("start %s times FIC information check" % countIteration)
        data = GetESData()
        isPassDashboardCPU = ParseDashboardCPU(data)
        isPassDashboardMemoryUsage = ParseDashboardMemoryUsage(data)
        isPassOverAllCPU = ParseOverAllCPU(data)
        isPassOverAllMemory = ParseOverAllMemory(data)
        isPassBytonLoggerCPU = ParseBytonLoggerCPU(data)
        isPassBytonLoggerMemoryUsage = ParseBytonLoggerMemoryUsage(data)
        isPassPowerdCPU = ParsePowerdCPU(data)
        isPassPowerdMemoryUsage = ParsePowerdMemoryUsage(data)
        isPassSpaceUtilization = ParseSpaceUtilization(data)
        isPassBytonCgwManageCPU = ParseBytonCgwManageCPU(data)
        isPassBytonCgwManageUsage = ParseBytonCgwManageUsage(data)
        
        allPass = isPassDashboardCPU is True and isPassDashboardMemoryUsage is True and isPassOverAllCPU is True and \
                  isPassOverAllMemory is True and isPassBytonLoggerCPU is True and isPassBytonLoggerMemoryUsage is True and \
                  isPassPowerdCPU is True and isPassPowerdMemoryUsage is True and isPassSpaceUtilization is True \
                  and isPassBytonCgwManageCPU is True and isPassBytonCgwManageUsage is True
        
        performancePass = performancePass and allPass    
        
        if countIteration == 1 or countIteration == 3 or countIteration == 6:
            UpdateSingleTest(listTests, isPassDashboardCPU, isPassDashboardMemoryUsage, isPassOverAllCPU, \
                         isPassOverAllMemory, isPassBytonLoggerCPU, isPassBytonLoggerMemoryUsage,\
                         isPassPowerdCPU, isPassPowerdMemoryUsage, isPassSpaceUtilization,\
                         isPassBytonCgwManageCPU, isPassBytonCgwManageUsage, countIteration)

        for test in listTests:
            if countIteration == 1:
                if test.title == "2 Hour Reliability run - Sanity":
                    UpdateTestStatus(test, performancePass)
            elif countIteration == 6:
                if test.title == "12 Hour SOAK RUN":
                    UpdateTestStatus(test, performancePass)
            elif countIteration == 12:
                if test.title == "24 Hour SOAK RUN":
                    UpdateTestStatus(test, performancePass)
            elif countIteration == 24:
                if test.title == "48 Hour SOAK RUN":
                    UpdateTestStatus(test, performancePass)
            elif countIteration == 36:
                if test.title == "72 Hour SOAK RUN":
                    UpdateTestStatus(test, performancePass)        

        if countIteration == 36:
            break

if __name__ == "__main__":
    UpdateResultToTestRail()

