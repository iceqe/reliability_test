# -*- coding: utf-8 -*-
'''
Created on Nov 13, 2018

@author: cavin.rui
'''

import os
import re
import urllib2
import commands

pathPackage = '/data/OTA'
synchUrl = 'http://10.13.0.133/Versions/ice-build-ap-stable/'
password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
password_mgr.add_password(None, synchUrl, 'developer', 'ICEChina')
handler = urllib2.HTTPBasicAuthHandler(password_mgr)
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)


def GetUrlContent(url):
    req = urllib2.Request(url)
    response = urllib2.urlopen(req)
    output = response.read()
    return output


def GetAvalibleUrl():
    output = GetUrlContent(synchUrl)
    return re.findall(r'<a href="(SCM-\d+/)"', output)

def GetPackageUrl(item):
    dictPackage = {}
    dictPackage[item] = {}
    output = GetUrlContent(synchUrl + item)
    listPackage = re.findall(r'<a href="(ICE[^\"]+)"', output)
    
    for package in listPackage:
        if 'DIAGS' in package:
            continue
        
        dictPackage[item][package] = synchUrl + item + "/" + package
    return dictPackage

def GetPackages():
    dictPackage = {}
    for item in GetAvalibleUrl():
        dictPackage.update(GetPackageUrl(item))
        
    return dictPackage


def DownloadFromUrl():
    dictPackage = GetPackages()
    
    for item in dictPackage.keys():
        for package in dictPackage[item].keys():
            pathItem = os.path.join(pathPackage, item)
            if not os.path.exists(pathItem):
                os.system("mkdir %s" % pathItem)
            listLocalPackages = os.listdir(pathItem)
            if package in listLocalPackages:
                continue
            command = 'wget -P %s --http-user=developer --http-password=ICEChina %s' % (pathItem, dictPackage[item][package])
            if os.system(command) == 0:
                if 'OTA' in package:
                    sha256 = commands.getoutput("sha256sum %s" % os.path.join(pathItem, package)).split(" ")[0]
                    os.system('echo %s >> %s.sha256 ' % (sha256, os.path.join(pathItem, package)))
                    os.system('echo %s >> %s.size ' % (os.stat(os.path.join(pathItem, package)).st_size, os.path.join(pathItem, package)))
                
            else:
                print "Error: can not download %s" % dictPackage[item][package]

    
DownloadFromUrl()
