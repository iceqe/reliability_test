#!/usr/bin/python

# random device settings code

import sys
import os
import time
import icePexpectConnection
import datetime
import signal
import iceElasticsearchdb
import iceBuildUpdate
import yaml
import iceFunctionalScenarioHandler
import iceReliabilityScenarioHandler
import logging
import threading

logger1 = None
deviceRuns = {}

# fic_ip = iceConfiguration.conf_Fic_Ip
# fic_user = iceConfiguration.conf_Fic_User
# fic_password = iceConfiguration.conf_Fic_password
# chain_device_ip = iceConfiguration.conf_Cars_Vm_Ip
stop_device_test = False
deviceConnection = None

logging.basicConfig(filename='iceSoak.log',level=logging.INFO)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def stop_test_execution():
    for key, handle in deviceRuns.items():
        if key == "Reliability_Handler":
            if handle is not None:
                handle.stop_reliability_scenarios()
        if key == "Functional_Handler":
            if handle is not None:
                handle.stop_functional_scenarios()
        if key == "Update_Handler":
            if handle is not None:
                handle.stop_all_build_update()


def signal_handler(signal, frame):
    print('Ctrl+C, exiting...')
    global stop_device_test
    stop_device_test = True # Stopping soak
    stop_test_execution() #Stopping all test execution
    time.sleep(3)
    exit(1)

def get_fic_connection_info(config_data):
    fic_connection_info = config_data["test_bench_info"]
    conf_fic_ip = fic_connection_info["conf_Fic_Ip"]
    conf_fic_user = fic_connection_info["conf_Fic_User"]
    conf_fic_password = fic_connection_info["conf_Fic_password"]
    conf_chain_device_ip = fic_connection_info["conf_Cars_Vm_Ip"]

    return conf_fic_ip, conf_fic_user, conf_fic_password, conf_chain_device_ip


def exit_soak():
    global stop_device_test
    stop_device_test = True
    time.sleep(3)
    exit(1)

def log(msg):
    if logger1:
        logger1.log(msg)
    else:
        print msg

def read_soak_startup_config(config_file_name):
    config = os.path.join(os.path.dirname(__file__), config_file_name)
    try:
        with open(config, "r") as f:
            config_data = yaml.load(f)
    except:
        log("%s config file not find, please perfrom a process exit" % config_file_name )
        return None
    return config_data


def check_connection(device_ip):
    response = os.system("ping -c 1 " + device_ip)
    connection = response == 0
    if connection:
        log("ping succeeded for %s" % device_ip)
    else:
        log("ping failed: cannot connect to device %s" % device_ip)
    return response == 0


def getUptime(self):
    command = "cat /proc/uptime"
    result = self.runCommandOnDevice(command)
    return result

def get_fic_data(device_ip, device_user, device_password, execute_command):
    global deviceConnection
    metrics_data = None

    if check_connection(device_ip):
        if not deviceConnection:
            try:
                deviceConnection = icePexpectConnection.coreConnection(device_ip, device_user, device_password, "#")
                deviceConnection.connectWithRetries(5)
            except:
                log("cannot ssh to %s, skipping uptime request" % device_ip)
                deviceConnection = None
                return None

            log("ssh successful to %s" % device_ip)

        try:
            metrics_data = deviceConnection.sendCommandWithResult(execute_command)
        except Exception as e:
            log("Got exception %r trying to get metrics" % e)
            deviceConnection = None
            return

    return metrics_data


def start_soak(device_ip, device_user, device_password, elk_handle, logger):
    global stop_device_test
    curr_date_time = time.time()
    log(curr_date_time)

    top_command = "top -i 1 b"
    hogs_command = "hogs -i 1"
    disk_space_command = "df -h | grep 'uda0.1B81' "
    device_uptime_command = "uptime"

    with open("ficMetrics.txt", "a+") as ficMetrics:

        while not stop_device_test:
            if check_connection(device_ip):
                try:
                    log_date_time = str("\nDate And Time: ") + str(datetime.datetime.now())
                    metrics_device_uptime = get_fic_data(device_ip, device_user, device_password, device_uptime_command)
                    metrics_data_top_info = get_fic_data(device_ip, device_user, device_password, top_command)
                    metrics_data_disk = get_fic_data(device_ip, device_user, device_password, disk_space_command)
                    metrics_data_hogs = get_fic_data(device_ip, device_user, device_password, hogs_command)


                    ficMetrics.writelines(str("\n") + str(log_date_time) + str("\n"))
                    ficMetrics.writelines(str("\n") + str(metrics_data_hogs))
                    ficMetrics.writelines(str("\n") + str(metrics_data_top_info))
                    ficMetrics.writelines(str("\n") + str(metrics_device_uptime + str("\n")))
                    ficMetrics.writelines( str("\n") + str(metrics_data_disk + str("\n")))
                    ficMetrics.flush()

                    metrics_data_top_info = metrics_data_top_info.split("\r\n")
                    metrics_data_hogs = metrics_data_hogs.split("\r\n")

                    # Send FIC metrics info to Elasticsearch db handler to create es packets and insert to es db
                    if elk_handle:
                        elk_handle.send_data_to_es_db(log_date_time, metrics_data_disk, metrics_data_top_info, metrics_data_hogs, metrics_device_uptime)

                    # No need to insert sleep below here, icePexpectConnection is already expect the data for 2 seconds.
                    # time.sleep(2)
                except Exception as e:
                    log("Got exception %r trying to get metrics" % e)
                    logger.error("Got exception %r trying to get metrics" % e)
                    ficMetrics.writelines(str("\n") + str("May be Disconnected") + str("\n"))
                    logger.error("May be Disconnected %r " % e)
                    pass
            else:
                logger.error("Device is not reachable, Can not ping %s" %device_ip)


# , 'ota-update': ' ota-update ', 'gsl_hab_server','procnto-smp-in',
# 'devb-sdmmc-msm','hab','devc-sermsm8x6', 'io-pkt-v6-hc','io-audio','audio_service'}

signal.signal(signal.SIGINT, signal_handler)


# def reliability_module_handler(type):
#
#     if type == "Update":
#
#     elif type =="FunctionalHandler":
#         func_handler = iceFunctionHandler.IceFunctionHandler()
#     elif type == "ReliabilityHandler":
#         reliability_handler = iceReliabiltyHandler.IceReliabilityHandler()

def handle_all_build_updates(config_data, logger): # getting all config data here incase we want to send some other info to update module
    log("handle_all_build_updates")

    build_config_data = config_data["build_update_info"]

    obj_build_update_info = {"milestone": build_config_data["conf_milestone"],
                        "jenkins_url": build_config_data["conf_jenkins_url"],
                        "jenkins_user": build_config_data["conf_jenkins_user"],
                        "jenkins_password": build_config_data["conf_jenkins_password"],
                        "ficBuildNumber": build_config_data["conf_ficBuildNumber"],
                        "ficBuildUpdate": build_config_data["conf_ficBuildUpdate"],
                        "sicBuildNumber": build_config_data["conf_sicBuildNumber"],
                        "sicBuildUpdate": build_config_data["conf_sicBuildUpdate"],
                        "cgwBuildNumber": build_config_data["conf_cgwBuildNumber"],
                        "cgwBuildUpdate": build_config_data["conf_cgwBuildUpdate"],
                        "atbaBuildNumber": build_config_data["conf_atbaBuildNumber"],
                        "atbaBuildUpdate": build_config_data["conf_atbaBuildUpdate"],
                        "ethernetUpdate": build_config_data["conf_ethernetUpdate"],
                        "otaUpdate": build_config_data["conf_otaUpdate"],
                        "otaBuildVersion": build_config_data["conf_otaBuildVersion"],
                        "server_package_for_ota": build_config_data["conf_server_package_for_ota"],
                        "cgw_atba_artifactory_path": build_config_data["conf_cgw_atba_artifactory_path"],
                        "cgw_atba_package": build_config_data["conf_cgw_atba_package"],
                        "cgw_atba_user": build_config_data["conf_cgw_atba_user"],
                        "cgw_atba_password": build_config_data["conf_cgw_atba_password"],
                        "cgw_atba_artifactory_or_local": build_config_data["conf_cgw_atba_artifactory_or_local"]
                        }

    update_handler = iceBuildUpdate.IceBuildUpdateHandler(obj_build_update_info, logger)
    update_results = update_handler.build_update_handler()

    start_soak_execution = True

    if obj_build_update_info["ethernetUpdate"] is True and update_results["ethernetUpdate"] != "Successful":
        start_soak_execution = False
        logger.error("Ethernet Update Failed, Soak will not start")
    if obj_build_update_info["otaUpdate"] is True and update_results["otaUpdate"] != "Successful":
        start_soak_execution = False
        logger.error("OTA Update Failed, Soak will not start")
    if obj_build_update_info["cgwBuildUpdate"] is True and update_results["cgwBuildUpdate"] != "Successful":
        start_soak_execution = False
        logger.error("CGW Update Failed, Soak will not start")
    if obj_build_update_info["atbaBuildUpdate"] is True and update_results["atbaBuildUpdate"] != "Successful":
        start_soak_execution = False
        logger.error("ATBA Update Failed, Soak will not start")

    deviceRuns["Update_Handler"] = update_handler

    return update_handler, start_soak_execution


def handle_all_functional_scenario(config_data, logger):
    log("handle_all_functional_scenario")
    functional_config_data = config_data["functional_scenario_info"]

    obj_functional_scenario_info = {}

    for count in xrange(len(functional_config_data.keys())):
        if 'conf_test%s'%count in functional_config_data.keys():
            test_key = 'conf_test%s'%count
            print('conf_test%s'%count)
            test_info = {"execute_test": functional_config_data[test_key]["conf_execute_test"],
                         "test_tag": functional_config_data[test_key]["conf_test_tag"],
                         "test_suite": functional_config_data[test_key]["conf_test_suite"],
                         "test_suite_path": functional_config_data[test_key]["conf_test_suite_path"],
                         "test_name": functional_config_data[test_key]["conf_test_name"],
                         "duration_or_iteration": functional_config_data[test_key]["conf_test_duration_or_iteration"]
                         }
            obj_functional_scenario_info['test%s' % count] = test_info

    func_handler = iceFunctionalScenarioHandler.IceFunctionalScenarioHandler(obj_functional_scenario_info, logger)

    deviceRuns["Functional_Handler"] = func_handler

    return func_handler


def handle_all_reliability_scenario(config_data, logger):
    log("handle_all_reliability_scenario")

    reliability_config_data = config_data["reliability_scenario_info"]

    obj_reliability_scenario_info = {}

    for count in xrange(len(reliability_config_data.keys())):
        if 'conf_test%s'%count in reliability_config_data.keys():
            test_key = 'conf_test%s'%count
            print('conf_test%s'%count)
            test_info = {"execute_test": reliability_config_data[test_key]["conf_execute_test"],
                         "test_name": reliability_config_data[test_key]["conf_test_name"],
                         "test_tag": reliability_config_data[test_key]["conf_test_tag"],
                         "test_suite": reliability_config_data[test_key]["conf_test_suite"],
                         "db_version": reliability_config_data[test_key]["conf_db_version"]
                         }
            obj_reliability_scenario_info['test%s' % count] = test_info

    #
    # obj_reliability_scenario_info = {"test_type": reliability_config_data["conf_test_type"],
    #                                  "test_suite": reliability_config_data["conf_test_suite"],
    #                                  "soak_simulation": reliability_config_data["conf_soak_simulation"],
    #                                  "test_case_number": reliability_config_data["conf_test_number"]
    #                                  }

    reliability_handler = iceReliabilityScenarioHandler.IceReliabilityScenarioHandler(obj_reliability_scenario_info,
                                                                                      logger)
    deviceRuns["Reliability_Handler"] = reliability_handler

    t = threading.Thread(target=reliability_handler.start_reliability_scenarios)
    t.daemon = True
    t.start()
    return t


def handle_elk_initialization(config_data, logger):
    logger.info("handle_elk_initialization")

    config_elk_info = config_data["elk_stack"]
    conf_test_bench_info = config_data["test_bench_info"]
    obj_elk_config_info = {"elk_url": config_elk_info["conf_elk_url"],
                           "elk_INDEX": config_elk_info["conf_elk_INDEX"],
                           "elk_TYPE_DOC": config_elk_info["conf_elk_TYPE_DOC"],
                           "testSetupNumber": conf_test_bench_info["conf_TestSetup_Number"],
                           "testBuildNumber": conf_test_bench_info["conf_TestBuild_Number"],
                           "vntNumber": conf_test_bench_info["conf_VNT_Number"],
                           "testSetupControlIp": conf_test_bench_info["conf_TestSetup_ControlIP"],
                           "carsVmIp": conf_test_bench_info["conf_Cars_Vm_Ip"],
                           "deviceHwType": conf_test_bench_info["conf_hw_type"],
                           "fStep": conf_test_bench_info["conf_FStep"]
                           }
    elk_handler = iceElasticsearchdb.IceElasticsearchDbHandler(obj_elk_config_info, logger)
    elk_init = elk_handler.init_index()

    if not elk_init:
        logger.error("elk initialization failed")

    return elk_handler


# log_device_metrics_to_file(fic_ip, process_names)

elk_handle = None
functional_handle = None
reliability_handle = None
update_handle = None
threadList = []

obj_config = read_soak_startup_config("reliabilityConfig.yaml")

if obj_config is None:
    logger.error("config file missing, exiting the soak...")
    exit_soak()

fic_ip, fic_user, fic_password, chain_device_ip = get_fic_connection_info(obj_config)

if obj_config["elk_stack"]["conf_elk_connect"]:
    elk_handle = handle_elk_initialization(obj_config, logger)


if obj_config["soak"]["conf_switchOff_all_Configuration_Flags"] is False:

    update_result_start_soak_execution = True
    if obj_config["build_update_info"]["conf_perform_update"]:
        update_handle, update_result_start_soak_execution = handle_all_build_updates(obj_config, logger)

    if obj_config["functional_scenario_info"]["conf_handle_functional_scenario"]:
        functional_thread = handle_all_functional_scenario(obj_config, logger)
        threadList.append(functional_thread)

    if obj_config["reliability_scenario_info"]["conf_handle_relibility_scenario"]:
        reliability_thread = handle_all_reliability_scenario(obj_config, logger)
        threadList.append(reliability_thread)
        print(threadList)

    if obj_config["soak"]["conf_start_soak"] and update_result_start_soak_execution:
        start_soak(fic_ip, fic_user, fic_password, elk_handle, logger)
    else:
        logger.error("Soak cannot start: reliabilityConfig->soak->conf_start_soak = %s, update_result_for_soak = %s"
                      %(obj_config["soak"]["conf_start_soak"], update_result_start_soak_execution))
else:
    if obj_config["soak"]["conf_start_soak"]:
        start_soak(fic_ip, fic_user, fic_password, elk_handle, logger)
    else:
        logger.error("Soak cannot start: reliabilityConfig->soak->conf_start_soak = %s"
                      % (obj_config["soak"]["conf_start_soak"]))


