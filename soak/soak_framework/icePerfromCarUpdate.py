'''
Created on Dec 14, 2018

@author: cavin.rui
'''
import iceConfiguration
import upgrade.cgwUpdate as cgwUpdate
import upgrade.Ethernet_update as Ethernet_update

def UpgradeFIC():
    if iceConfiguration.iceFicUpdate:
        pathBuild = Ethernet_update.GetPackageFromJenkins(iceConfiguration.iceFicBuildVerisonToUpdate)
        return Ethernet_update.EthernetUpdate(pathBuild, iceConfiguration.iceForceUpdate)
    
    return 1

def UpgradeCGWAndATBA():
    #The same process like UpgradeFIC()
    pass

if __name__ == '__main__':
    if not UpgradeFIC(): 
        Ethernet_update.logger()
        Ethernet_update.logger.warn("successful")
    else:
        Ethernet_update.logger.warn("failed")
    
    
        
