# -*- coding: utf-8 -*-
'''
Created on Nov 14, 2018

@author: cavin.rui

script introduce:
this script is for CGW/ATBA updating only, user should define the url where the package downloading.

usage:
1. make sure the machine that running this script can connect to CGW/ATBA(192.168.111.2/1), script puts package into /tmp/
'''
import traceback
import SSHLibrary
import re
import logging
import time
import os
import sys
from artifactory import ArtifactoryPath

user = 'byton'
password = 'byton!'

def GetPackageFromArtifactory(url):
    url = url.replace("/webapp/#/artifacts/browse/tree/General", '')
    objpathPackage = ArtifactoryPath(url, auth=('cavin_rui', 'cavin_rui'))
    pathPackage = os.path.join(os.getcwd(), os.path.basename(url))
    #return pathPackage
    logger.warn("start downloading package.swu from artifactory")
    with objpathPackage.open() as fd:
        with open(pathPackage, "wb") as out:
            out.write(fd.read())
    return pathPackage

def Login(host):
    try:
        objSSHLibrary = SSHLibrary.SSHLibrary()
        objSSHLibrary.open_connection(host=host, port=22)
        objSSHLibrary.login(user, password)
        return objSSHLibrary
    except:
        return None


def PackageUpdate(pathBuild, updateType):
    
    if updateType == 'CGW':
        host = '192.168.111.2'
    elif updateType == 'ATBA':
        host = '192.168.111.1'
        
    objSSHLibrary = Login(host)
    if objSSHLibrary is None:
        logger.error("%s is down" % updateType)
        return 1
    if not os.path.exists(pathBuild):
        logger.warn('local file %s does not exist' % pathBuild)
        return 1
    
    objSSHLibrary.execute_command("rm -rf /tmp/package.swu")   
    output = objSSHLibrary.execute_command('cat /etc/issue')
    currentBuildNum = re.search(r'Linux Byton\s+(\S+)\s+', output).group(1)
    logger.warn("start %s update" % updateType)
    logger.warn("current Build Number is: %s" % currentBuildNum)
    
    sizeLocal = os.stat(pathBuild).st_size
    
    logger.warn("upload build package to %s, please wait for a while" % updateType)        
    objSSHLibrary.put_file(pathBuild, '/tmp/package.swu')
    
    if not str(sizeLocal) in objSSHLibrary.execute_command("ls -l /tmp/package.swu"):
        logger.warn("upload build package to %s failed, please check the size both of local and remote" % updateType) 
        return 1
    
    logger.warn("execute updating: sudo dutch-update.sh -u /tmp/package.swu") 
    objSSHLibrary.write("sudo dutch-update.sh -u /tmp/package.swu")
    objSSHLibrary.set_client_configuration(timeout="180 seconds")
    objSSHLibrary.read_until("MD5s match")
    logger.warn("Check md5 over")
    logger.warn("Write image to /dev/mmcblk0p1 partition")
    objSSHLibrary.read_until("Rebooting the system")
    logger.warn("Rebooting the system")
    objSSHLibrary.execute_command('sudo reboot')
    objSSHLibrary.close_all_connections()
    time.sleep(5)
    timeOut = 500
    while timeOut:
        objSSHLibrary = Login(host)
        if objSSHLibrary:
            break
        else:
            logger.warn("waiting for %s rebooting" % updateType)
            timeOut -= 1
            time.sleep(1)
            
           
    output = objSSHLibrary.execute_command('cat /etc/issue')
    realBuildNum = re.search(r'Linux Byton\s+(\S+)\s+', output).group(1)
    logger.warn("Build Number after updating is: %s" % realBuildNum)
    objSSHLibrary.close_all_connections()
    if currentBuildNum == realBuildNum:
        logger.error("%s Update failed, build number not changed, current version is %s, expect is %s" % (updateType, currentBuildNum, realBuildNum))
        return 1
    else:
        logger.warn("%s update is successful" % updateType)
        return 0

def InitLogging():
    logger = logging.getLogger()
    logger.setLevel(logging.WARN) 
    rq = time.strftime('%Y%m%d', time.localtime(time.time()))
    logPath = os.path.join(os.path.dirname(__file__), 'Logs')
    if not os.path.exists(logPath):
        os.mkdir(logPath)
    logFile = os.path.join(logPath, rq + '.log' )
        
    handler = logging.FileHandler(logFile, mode='a')
    handler.setLevel(logging.INFO)  
    formatter = logging.Formatter("%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logger.addHandler(console)
    
    return logger

logger = InitLogging()
if __name__ == '__main__':
    try:
        usage = '''Usage: %s <url_for_package>
       url_for_package: download url link of package.swu
       For example: %s https://btnussfoart01.byton.com/artifactory/webapp/#/artifacts/browse/tree/General/ICE-firmware/doubleDutchman-AP/build/2520-21278266642f2f125973f7ac89f281a3f8b78a33/images/package.swu
        ''' % (__file__, __file__)
        if len(sys.argv) == 2:
            url = sys.argv[1]
        else:
            raise
        pathBuild = GetPackageFromArtifactory(url)
        PackageUpdate(pathBuild, 'CGW')
        PackageUpdate(pathBuild, 'ATBA')
    except Exception, msg:
        logger.error(traceback.format_exc())
        logger.warn(msg)
        logger.warn(usage)