# -*- coding: utf-8 -*-
'''
Created on Nov 22, 2018
Updated on Dec 25, 2018

@author: longfei.li

script introduce:
this script is used to compress file and send the file through mail.
'''
import smtplib
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.utils import formatdate
from email import Encoders
import os
from email.utils import formataddr
import zipfile
import time
import pandas as pd


class OtaMail():
    def __init__(self, smtp_server, smtp_port=25, verbose=False, debug_level=1, encoding="utf-8"):
        ''' Initiate the EmailSender.
        @param smtp_server: the Email SMTP server.
        @param smtp_port:   the Email SMTP server port, if use the default port(25), you can set it to 0 or 25.
        @param verbose:     show the processing information if set to 'True', default is 'False'.
        @param debug_level: set the smtplib debug level, if it's '0', will enable debug information.
        @param encoding:    the encoding or charset for email body text or attachment file name, default is "utf-8".
        '''
        self.server = smtp_server
        self.port = int(smtp_port)
        self.verbose = verbose
        self.debug_level = int(debug_level)
        self.encoding = encoding
        self.attachments = []
 
        #Create smtp instance
        self.smtp = smtplib.SMTP(self.server, self.port)
        self.smtp.set_debuglevel(self.debug_level)
        self.print_verbose("Init SMTP server successfully. server=%s, port=%d."%(self.server, self.port))
    
    def print_verbose(self, message):
        '''Print the verbose information.
        @param message: the message to be print if the verbose is "True".
        '''
        if self.verbose:
            print(message)
    
    def zipFile(self):
        """
        A function to compress file with zipfile method.
        @return: if zipfile successfully, return the zip file.
        """
        rq = time.strftime('%Y%m%d', time.localtime(time.time()))
        logpath = os.path.join(os.path.dirname(__file__), 'Logs')
        logFile = os.path.join(logpath, rq + '.log' )
        if not os.path.exists(logFile):
            print("There is no log file, please check your OTA update.")
            return
        else:
            os.chdir(os.path.split(logFile)[0])
            if os.path.exists(os.path.join(rq + '.log')):
                logName = os.path.join(rq + '.log')
                logzipfile = os.path.join(logName + '.zip')
                with zipfile.ZipFile(logzipfile, 'w', zipfile.ZIP_DEFLATED) as f:
                    f.write(logName)
        return logzipfile      
    
    def convertToHtml(self, result, title):
        """
        A function to create and return html page.
        @param reslut: different category in html table.
        @param title:  different title in html table.
        """
        d = {}
        index = 0
        for t in title:
            d[t]=result[index]
            index = index+1
        df = pd.DataFrame(d)
        df = df[title]
        self.h = df.to_html(index=False)
        return self.h
                
    def send_email(self, subject, from_addr, to_addrs, cc_addrs, content, attach, subtype="plain", charset=None):
        '''Send the email to the receivers.
        @param subject:    the email's subject(title).
        @param from_addr:  the sender address
        @param to_addrs:   the receivers' addresses, it's a list looks like ["user1@some_server.com", "user2@another_server.com"].
        @param cc_addrs:   the copy to receivers' addresses, has the same format as to_addrs.
        @param content:    the email message body, it can be plain text or HTML text, which depends on the parameter 'content_type'.
        @param subtype:    the content type, it can be "html" or "plain", default is "plain".
        @param charset:    the charset of message content, default is "None", use the same encoding as the initial function, which default is "utf-8".
        @return: if send successfully, return 'True', otherwise, return 'False'.
        '''
        charset = charset if charset is not None else self.encoding
        
        #Set the root information
        msg_root = MIMEMultipart("related")
        msg_root["Subject"] = subject
        msg_root["From"] = from_addr  # You can change it to any string
        msg_root["To"] = ",".join(to_addrs)
        msg_root["CC"] = ",".join(cc_addrs)
        msg_root["Date"] = formatdate(localtime=True)
        msg_root.preamble = "This is a multi-part message in MIME format."
 
        #Encapsulate the plain and HTML of the message body into an 'alternative' part,
        #so message agents can decide which they want to display.
        msg_alt = MIMEMultipart("alternative")
        #Set the message content
        msg_txt = MIMEText(content, subtype.lower(), charset)
        msg_alt.attach(msg_txt)
 
        #Add the alternative part to root part.
        msg_root.attach(msg_alt)
 
        #Add the attachment files
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(attach, 'rb').read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"'% os.path.basename(attach))
        msg_root.attach(part)
 
        #Extend the copy to addresses to to_addrs
        to_addrs.extend(cc_addrs)
 
        #Send the email
        try:
            self.smtp.sendmail(from_addr, to_addrs, msg_root.as_string())
            self.print_verbose("Send email successfully.")
        except Exception as ex:
            print("Send email failed. Error info:%s"%(str(ex)))
            return False
        return True
 
    def close(self):
        '''Quit from the SMTP.
        '''
        self.smtp.quit()
        self.print_verbose("Logout SMTP server successfully.")            


def test():
    smtp_server = "10.11.3.132"
    from_addr = "longfei.li@byton.com"
    to_addrs = ["15051802047@sohu.com", '287925873@qq.com']
    cc_addrs = []
    subject = "Email sending test"
    content ='Dear Friends,<p/><a href="http://blog.csdn.net/thomashtq"> Welcome to my CSDN blog!</a> <p/>Thanks a lot!'
    
    emailsender = OtaMail(smtp_server, verbose=True)
    attach_files = [r'mail_config.json']
    for attach in attach_files:
        emailsender.add_attachment(attach, "utf-8")
#     emailsender.send_email(subject, from_addr, to_addrs, cc_addrs, content, subtype="html", charset="utf-8")
    emailsender.close()
        
if __name__ == '__main__':
    test()
