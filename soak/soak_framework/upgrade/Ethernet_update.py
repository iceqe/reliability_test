# -*- coding: utf-8 -*-
'''
Created on Oct 30, 2018

@author: cavin.rui

script introduce:
this script is for Ethernet updating only, user should define the server where the package downloading.

usage:
1. make sure the machine that running this script can connect to QNX(192.168.111.10), script puts package into /data/
2. define the local build for Ethernet update,
'''
import traceback
import SSHLibrary
import re
import logging
import time
import os
import sys
from jenkinsapi import jenkins


def GetPackageFromJenkins(buildNum, mileStone='AP'):
    objJenkins = jenkins.Jenkins("http://10.11.3.125:8080/", username="qa", password="Bytoncar")
    job = objJenkins.get_job("ice-build-ap-stable")
    try:
        build = job.get_build(int(buildNum))
    except:
        logger.error("build #%s not found in job 'ice-build-ap-stable'" % buildNum)
        raise
    artifacts = build.get_artifact_dict()
    for artifact in artifacts.keys():
        if "OTA" in artifact and mileStone in artifact:
            text_artifact = artifacts[artifact]
            break
    pathArtifact = os.path.join(os.getcwd(), artifact)
    if not os.path.exists(pathArtifact):
        logger.warn("downloading package from jenkins, please wait")
        text_artifact.save_to_dir(os.getcwd())#, strict_validation=True)
    else:
        logger.warn("package exist, skip downloading")
    return pathArtifact

def LoginQNX():
    try:
        objSSHLibrary = SSHLibrary.SSHLibrary()
        objSSHLibrary.open_connection(host='192.168.111.10', port=22)
        objSSHLibrary.login('root', '')
        return objSSHLibrary
    except:
        return None


def EthernetUpdate(pathBuild, isForceUpdate=True):
    objSSHLibrary = LoginQNX()
    if objSSHLibrary is None:
        logger.error("QNX is down")
        return 1
    if not os.path.exists(pathBuild):
        logger.warn('local file %s does not exist' % pathBuild)
        return 1
    
    objSSHLibrary.execute_command("rm -rf /data/*")   
    match = re.search(r'ICE.*_S_(\d+)_(FIC.*)_OTA.tar.gz', pathBuild)
    expectBuildNum = match.group(1)
    expectBuildInfo = match.group(2)
    output = objSSHLibrary.execute_command('byton_info.sh')
    logger.warn("original byton info")
    logger.warn(output)
    realBuildNum = re.search(r'Byton Build Name \[[^\]]+_(\d+)\].*\n.*Byton Build Time \[([^\]]+)\]', output).group(1)
    
    updatePath = "%s -> %s" % (realBuildNum, expectBuildNum)
    logger.warn("start Ethernet update: %s" % updatePath)
    if realBuildNum == expectBuildNum and not isForceUpdate:
        logger.warn("The same build number, pass it")
        return 1
    
    sizeLocal = os.stat(pathBuild).st_size
    
    logger.warn("upload build package to FIC, please wait for a while")        
    objSSHLibrary.put_file(pathBuild, '/data/ice-ota-ethernet.tar.gz')
    
    if not str(sizeLocal) in objSSHLibrary.execute_command("ls -l /data/ice-ota-ethernet.tar.gz"):
        logger.warn("upload build package to FIC failed, please check the size both of local and remote") 
        return 1
    
    logger.warn("execute updating: ota-update /data/ice-ota-ethernet.tar.gz") 
    objSSHLibrary.write("ota-update /data/ice-ota-ethernet.tar.gz &")
    
    objSSHLibrary.set_client_configuration(timeout="600 seconds")
    objSSHLibrary.write("slog2info -w | grep OTA")
    objSSHLibrary.read_until("Extracting OTA image tar file")
    logger.warn("Extracting OTA image tar file")
    objSSHLibrary.read_until("Extracting OTA image finished.")
    logger.warn("Extracting OTA image finished.")
    objSSHLibrary.read_until("cp /lib64/libspmi_client.so /data")
    logger.warn("cp /lib64/libspmi_client.so /data")
    objSSHLibrary.read_until("swdl-powersafe -d /data/ice-ota-images -g main=gpt_main0.bin,backup=gpt_backup0.bin")
    logger.warn("swdl-powersafe -d /data/ice-ota-images -g main=gpt_main0.bin,backup=gpt_backup0.bin")
    objSSHLibrary.close_all_connections()
    timeOut = 500
    while timeOut:
        objSSHLibrary = LoginQNX()
        if objSSHLibrary:
            objSSHLibrary.close_connection()
        else:
            timeOut -= 1
            time.sleep(1)
            break
            
    timeOut = 500
    while timeOut:
        objSSHLibrary = LoginQNX()
        if objSSHLibrary:
            break
        else:
            logger.warn("waiting for QNX rebooting")
            timeOut -= 1
            time.sleep(1)
            
           
    output = objSSHLibrary.execute_command('byton_info.sh')
    logger.warn("updated byton info")
    logger.warn(output)
    match = re.search(r'Byton Build Name \[[^\]]+(FIC[^\_]+_.*)_S_(\d+)\].*\n.*Byton Build Time \[[^\]]+\]', output)
    realBuildNum = match.group(2)
    realBuildInfo = match.group(1)
    objSSHLibrary.close_all_connections()
    if not expectBuildNum == realBuildNum  or not realBuildInfo == expectBuildInfo:
        logger.error("Ethernet Update failed, current version is %s, expect is %s" % (realBuildNum, expectBuildNum))
        return 1
    else:
        logger.warn("Ethernet update %s is successful" % updatePath)
        return 0

def InitLogging():
    logger = logging.getLogger()
    logger.setLevel(logging.WARN) 
    rq = time.strftime('%Y%m%d', time.localtime(time.time()))
    logPath = os.path.join(os.path.dirname(__file__), 'Logs')
    if not os.path.exists(logPath):
        os.mkdir(logPath)
    logFile = os.path.join(logPath, rq + '.log' )
        
    handler = logging.FileHandler(logFile, mode='a')
    handler.setLevel(logging.INFO)  
    formatter = logging.Formatter("%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logger.addHandler(console)
    
    return logger

logger = InitLogging()
if __name__ == '__main__':
    usage = '''Usage: %s <build_number> <AP|ADP>
           build_number: jenkins build number
           For example: %s 60 AP
            ''' % (__file__, __file__)
    try:
        if len(sys.argv) == 3:
            buildNumber = sys.argv[1]
            mileStone = sys.argv[2]
        else:
            raise Exception("Raising intented exception - no exceptiopn as such here")
        pathBuild = GetPackageFromJenkins(buildNumber, mileStone)
        EthernetUpdate(pathBuild)
    except Exception, msg:
        logger.error(traceback.format_exc())
        logger.warn(msg)
        logger.warn(usage)