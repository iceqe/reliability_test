# -*- coding: utf-8 -*-
'''
Created on Oct 30, 2018

@author: cavin.rui

script introduce:
this script is for Ethernet updating only, user should define the server where the package downloading.

usage:
1. make sure the machine that running this script can connect to QNX(192.168.111.10), script would set the route to call http://10.13.0.133/Versions
2. define the builds for OTA update, all data need wrapped in variable "infoPackage", you need get package name, package size and sha256 value 
3. define loopCount, this parameter allows OTA update loop the defined times
'''

import SSHLibrary
import re
import logging
import time
import os
import sys
import traceback
import netifaces as ni
import urllib2

gws = ni.gateways()
gateway = gws['default'][gws['default'].keys()[0]][0]


infoPackage = ["ICE0.1_S_58_FIC820AP_CN_OTA.tar.gz", "ICE0.1_S_57_FIC820AP_CN_OTA.tar.gz"]                    
            # package information 


def LoginQNX():
    try:
        objSSHLibrary = SSHLibrary.SSHLibrary()
        objSSHLibrary.open_connection(host='192.168.111.10', port=22)
        objSSHLibrary.login('root', '')
        return objSSHLibrary
    except:
        return None


def OTAUpdate(version):
    objSSHLibrary = LoginQNX()
    if objSSHLibrary is None:
        logger.error("QNX is down")
        return 1
    
    objSSHLibrary.execute_command("rm -rf /data/*")   
    objSSHLibrary.execute_command("route -n add -net %s %s" % (re.search(r'://([^\:|^/]+)[:|/]', serverPackage).group(1), gateway))
    match = re.search(r'ICE.*_S_(\d+)_(FIC.*)_OTA.tar.gz', version)
    expectBuildNum = match.group(1)
    expectBuildInfo = match.group(2)
    output = objSSHLibrary.execute_command('byton_info.sh')
    realBuildNum = re.search(r'Byton Build Name \[[^\]]+_(\d+)\].*\n.*Byton Build Time \[([^\]]+)\]', output).group(1)
    
    updatePath = "%s -> %s" % (realBuildNum, expectBuildNum)
    logger.warn("start OTA update: %s" % updatePath)
#    if realBuildNum == expectBuildNum:
#        logger.warn("The same build number, pass it")
#        return
    urlPackage = "%s/SCM-%s/%s" % (serverPackage.strip('/'), expectBuildNum, version)
    req = urllib2.Request('%s.size' % urlPackage)
    response = urllib2.urlopen(req)
    size = response.read()
    req = urllib2.Request('%s.sha256' % urlPackage)
    response = urllib2.urlopen(req)
    sha256 = response.read()
    command = '''BytonCgwManager -d "%s" "%s" %s''' % (urlPackage, sha256, size)
    objSSHLibrary.set_client_configuration(timeout="600 seconds")
    objSSHLibrary.write(command)
    objSSHLibrary.read_until_regexp("100\s+\d+\w\s+100 ")
    logger.warn("package download completed")
    objSSHLibrary.write("slog2info -w | grep cgw")
    output = objSSHLibrary.read_until("Calculating SHA 256 for /data/ice-ota-image.tar.gz")
    logger.warn("Calculating SHA 256 for /data/ice-ota-image.tar.gz")
    output = objSSHLibrary.read_until("Successfully notified installer PPS")
    logger.warn("Successfully notified installer PPS")
    objSSHLibrary.close_all_connections()
    objSSHLibrary = LoginQNX()
    objSSHLibrary.set_client_configuration(timeout="600 seconds")
    objSSHLibrary.write("slog2info -w | grep OTA")
    output = objSSHLibrary.read_until("Extracting OTA image tar file")
    logger.warn("Extracting OTA image tar file")
    output = objSSHLibrary.read_until("Extract image successful")
    logger.warn("Extract image successful")
    output = objSSHLibrary.read_until("lib spmi copied")
    logger.warn("lib spmi copied")
    objSSHLibrary.close_all_connections()
    time.sleep(30)  #waiting for reboot 
    timeOut = 500
    while timeOut:
        objSSHLibrary = LoginQNX()
        if objSSHLibrary:
            objSSHLibrary.close_connection()
            break
        else:
            logger.warn("waiting for QNX rebooting")
            timeOut -= 1
            time.sleep(1)
            
        
    objSSHLibrary = LoginQNX()
    output = objSSHLibrary.execute_command('byton_info.sh')
    match = re.search(r'Byton Build Name \[[^\]]+(FIC[^\_]+_.*)_S_(\d+)\].*\n.*Byton Build Time \[[^\]]+\]', output)
    realBuildNum = match.group(2)
    realBuildInfo = match.group(1)
    objSSHLibrary.close_all_connections()
    if not expectBuildNum == realBuildNum or not realBuildInfo == expectBuildInfo:
        logger.error("OTA update failed, current version is %s, expect is %s" % (realBuildNum, expectBuildNum))
        return 1
    else:
        logger.warn("OTA update %s is successful" % updatePath)
        return 0

def InitLogging():
    logger = logging.getLogger()
    logger.setLevel(logging.WARN) 
    rq = time.strftime('%Y%m%d', time.localtime(time.time()))
    logPath = os.path.join(os.path.dirname(__file__), 'Logs')
    if not os.path.exists(logPath):
        os.mkdir(logPath)
    logFile = os.path.join(logPath, rq + '.log' )
        
    handler = logging.FileHandler(logFile, mode='a')
    handler.setLevel(logging.INFO)  
    formatter = logging.Formatter("%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logger.addHandler(console)
    
    return logger

logger = InitLogging()
if __name__ == '__main__':
    try:
        usage = '''Usage: %s <url_for_package_download> <loop_count>
       url_for_package_download: package server for downloading, the package should be under this url directly
       loop_count: loop times for OTA update                 
       For example: %s http://10.13.132.15:8080/download/ 100
        ''' % (__file__, __file__)
        if len(sys.argv) == 3:
            serverPackage = sys.argv[1]
        #serverPackage = "http://10.13.132.15:8080/download/" #package server for downloading, the package should be under this url directly
                                                        # for example: http://10.13.132.15:8080/download/
        
            loopCount = int(sys.argv[2])  # loop times for OTA update
        else:
            raise Exception("manually raising exception")
        for i in range(loopCount):
            for version in infoPackage:
                logger.warn("run %s times OTA update for AP: %s " % ((i+1), version))
                OTAUpdate(version)
    except Exception, msg:
        logger.error(traceback.format_exc())
        logger.warn(msg)
        logger.warn(usage)