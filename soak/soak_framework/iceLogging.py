"""
   Logging module
"""

import logging
import logging.handlers
import time
import json
import os

import iceVersion


class Logger(object):
    """
    Logger: provides a wrapper around standard logging.

    -  saves log output to file and/or sends output to stdout
    -  normally prefixes each line with metadata useful for parsing
        log format is: <event type>:<device ip>:<timestamp>:<log msg>
        Any of those field values can be set by caller.
    -  can also write lines without the metadata prefix
    -  can be shared across multiple threads using logger name

    methods:
        constructor
        log: outputs string with metadata prefix
        writeline: unadorned plain string, i.e. without a prefix
        close: flushes output
    """

    def __init__(self, logger_name, file_name=None,
                 stdout_output=True, device_ip=""):
        """
        specify deviceIp and
        specify whether to log to file, stdout, or both
        :param logger_name: (required) NOT the filename, this is
            an internal name. Multiple threads initializing a logger
            with the same name will reference the same logger.
        :param file_name: (optional) logfile name. Will not log to file
            if this is not ever specified.
        :param stdout_output: (boolean, default True). Specifies whether to
            send log output to stdout
        :param device_ip: (optional, defaults to empty string). DeviceIp
            to output as part of metadata for every line.
        :return: logger object.
        """

        logger = logging.getLogger(logger_name)
        logger.setLevel(logging.DEBUG)

        self.consoleLogHandler = None
        self.fileLogHandler = None
        self.logger = logger
        self.deviceIp = device_ip

        if stdout_output:
            self.consoleLogHandler = logging.StreamHandler()
            self.consoleLogHandler.setLevel(logging.DEBUG)
            logger.addHandler(self.consoleLogHandler)

        if file_name is not None:
            file_exists = os.path.isfile(file_name)
            self.fileLogHandler = logging.handlers.RotatingFileHandler(
                file_name, mode='a', maxBytes=10000000, backupCount=900)
            self.fileLogHandler.setLevel(logging.DEBUG)
            logger.addHandler(self.fileLogHandler)
            if file_exists:
                self.fileLogHandler.doRollover()
        # self.log(iceVersion.versionString(), self.deviceIp, "VERSION")

    def log(self, message, device_ip=None, event_type="ice", time_stamp=None):
        """
        log message prepended by metadata
        prefer over to writeline as the metadata simplifies parsing
        format: <event type>:<device ip>:<timestamp>:<log msg>

        :param message: string to log
        :param device_ip: optional.
            defaults to value specified in constructor
            prefer specifying in constructor, only use this if need
            multiple devices in single logfile
        :param eventType: optional, default = "ice")
        :param timestamp: optional. Best to leave as default value
            None will insert current time.
        """
        if not time_stamp:
            time_stamp = time.strftime("%Y%m%d_%H%M%S")

        if not device_ip:
            device_ip = self.deviceIp

        log_message = "%s:%s:%s: %s\n" % (event_type, device_ip,
                                          time_stamp, message)
        self.logger.info(log_message)

    def log_dict_value(self, dict_value, device_ip=None, event_type="ice", time_stamp=None):
        dictString = json.dumps(dict_value)
        self.log(dictString, device_ip, event_type, time_stamp)

    # logs plain msg with no metadata, sometimes useful for reporting.
    def write_line(self, message):
        """
        logs plain string with no metadata added, sometimes useful for
        creating clean looking reports

        :param message: string to log
        """
        self.logger.info(message)

    def close(self):
        """
        flushes all output streams.
        Not strictly necessary.
        """
        if self.consoleLogHandler is not None:
            self.consoleLogHandler.flush()

        if self.fileLogHandler is not None:
            self.fileLogHandler.close()