#!/usr/bin/python
#
# Author:Neeraj Kumar Singh, neeraj@Byton.com
# Config file to help defining elasticsearch db mappings through config

process_names_to_monitor = {'Dashboard':' Dashboard ',
                            'BytonLogger':' BytonLogger ',
                            'Powerd':' powerd ',
                            }


elk_setup_mapping_frame = str('''{{
        "properties": {{
                'DateTime.DateTime': {{
                    "type": "date",
                    "format": "yyyy-MM-dd-HH:mm:ss||yyyy-MM-dd||epoch_millis"
                }},
                'TestSetup.Number': {{
                    "type": "integer"
                }},
                'TestSetup.ControlIP': {{
                    "type": "ip"
                }},
                'TestSetup.Device_Hw_Type': {{
                    "type": "text"
                }},
                'TestSetup.DeviceType': {{
                    "type": "text"
                }},
                'TestSetup.DeviceIP': {{
                    "type": "ip"
                }},
                'TestSetup.RemoteAccessPort': {{
                    "type": "integer"
                }},
                'TestSetup.Data1': {{
                    "type": "text"
                }},
                'TestSetup.Data2': {{
                    "type": "text"
                }},
                'TestBuild.Branch': {{
                    "type": "text"
                }},
                'TestBuild.Number': {{
                    "type": "integer"
                }},
                'TestBuild.GitHash': {{
                    "type": "text"
                }},
                'VNT.Number': {{
                    "type": "integer"
                }},
                'TestBuild.HardwareVersion': {{
                    "type": "text"
                }},
                'TestBuild.Milestone': {{
                    "type": "text"
                }},
                'TestBuild.ReleaseInfo': {{
                    "type": "text"
                }},
                'Fstep': {{
                    "type": "text"
                }},
                'TestType.Type': {{
                    "type": "text"
                }},
                'TestType.RuntimeHours': {{
                    "type": "integer"
                }},
                'TestType.Scenario': {{
                    "type": "text"
                }},
                'TestType.Data1': {{
                    "type": "text"
                }},
                'System.LinuxTimestamp': {{
                    "type": "integer"
                }},
                'System.OverallCPU': {{
                    "type": "integer"
                }},
                'System.OverallMemory': {{
                    "type": "integer"
                }},
                'System.FreeMemory': {{
                    "type": "integer"
                }},
                'System.IdleCPU': {{
                    "type": "integer"
                }},
                'System.Space': {{
                    "type": "integer"
                }},
                'SystemCrashes.KernelCrashes': {{
                    "type": "integer"
                }},
                'SystemReboots.DeviceReboots': {{
                    "type": "integer"
                }},
                'TotalRunning.Processes': {{
                    "type": "integer"
                }},
                {0}
                }}
                }}''')

elk_process_list_mapping_frame = str(''''{0}.PID': {{ "type": "integer"}},
             '{0}.Process': {{ "type": "text"}},
             '{0}.Cycles': {{"type": "integer"}},
             '{0}.CPU': {{"type": "integer"}},
             '{0}.CPU%': {{"type": "integer"}},
             '{0}.MemoryKB': {{"type": "integer"}},
             '{0}.Memory%': {{"type": "integer"}},
             '{0}.ProcessRestart': {{"type": "integer"}},''')