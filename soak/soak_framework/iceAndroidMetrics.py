import os
import os.path as op
from adb import adb_commands
from adb import sign_m2crypto
import time
import re
import traceback
import dateutil.parser as parser
import logging

logging.basicConfig(filename='iceSoak.log',level=logging.INFO)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

class IceAndroidMetricsCollection(object):

    # connect at init time
    def __init__(self, user, password, process_list,  remote_address="192.168.111.11", port='5555', logger=None):

        # Target Device info:
        self.remote_address = remote_address
        self.port = port
        self.user = user
        self.password = password

        # Connection requirements:
        self.device_connection = None
        self.device_handle = None
        self.signer = None
        self.port_path = None
        self.serial = self.remote_address + str(':') + self.port
        self.rsa_keys = None

        # Commands to get android metrics
        self.top_command = "top -n 1 -b"
        self.disk_space_command = "df -h | grep  /data$ "
        self.device_uptime_command = 'uptime'
        self.device_date_command = 'date'

        # File holds all raw data locally
        self.logger = logger
        self.file_android_metrics = open("ficMetrics_android.txt", "a+")
        self.android_metrics_packet = {}

        # The list of process to monitor on the android system
        self.process_monitor_list = process_list

        # To handle system and process reboot counts
        self.previous_up_time = 0
        self.number_of_reboot_detected = 0
        self.device_process_reboot_handling = False
        self.process_pid = {}
        for key in self.process_monitor_list.keys():
            self.process_pid[key] = -1
        self.process_restart_count = {}
        for key in self.process_monitor_list.keys():
            self.process_restart_count[key] = -1

        self.init_device_connection()

    def __del__(self):
        self.file_android_metrics.close()

    def ping_check(self, device_ip):

        response = os.system("ping -c 1 " + device_ip)
        connection = response == 0
        if connection:
            self.logger.info("ping_check: ping succeeded for %s" % device_ip)
        else:
            self.logger.error("ping_check: ping failed: cannot ping to device %s" % device_ip)

        return response == 0

    def init_device_connection(self):

        if self.ping_check(self.remote_address):
            # KitKat+ devices require authentication
            self.signer = sign_m2crypto.M2CryptoSigner(op.expanduser('~/.android/adbkey'))
            self.rsa_keys = [self.signer]
            # Connect to the device
            self.device_connection = adb_commands.AdbCommands()
            self.device_handle = self.device_connection.ConnectDevice(port_path = self.port_path, serial = self.serial,
                                                                      default_timeout_ms=500, rsa_keys=[self.rsa_keys])

            if self.device_handle:
                self.logger.info("create_connection: Succeeded")
                return True
            else:
                self.logger.error("Failed to create connection to device %s" % self.remote_address)
                return False
        else:
            self.logger.error("In create_connection: ping failed: cannot ping to device %s" % self.remote_address)
            return False

    def check_device_connection(self, device_ip):

        if self.ping_check(self.remote_address):

            output = self.device_handle.GetState()
            print("In check_device_connection,         output = %s" %output)

            if output == 'device':
                return True
            else:
                self.logger.error("In check_device_connection: No Connection to Device, Output =%s" % output)
                return False
        else:
            self.logger.error("In check_device_connection: Unable to ping Device")
            return False

    def execute_adb_command_with_retries(self, command_to_execute, timeout=5):

        metrics_data = None

        for retries in range(0,5):
            if self.check_device_connection(self.remote_address):
                try:
                    self.device_handle = self.device_connection.ConnectDevice(port_path = self.port_path,
                                                                              serial = self.serial,
                                                                              default_timeout_ms=500,
                                                                              rsa_keys=[self.signer])
                    metrics_data = self.device_handle.Shell(command_to_execute).rstrip()
                    self.logger.info("In execute_adb_command_with_retries: Command executed successfully")
                except Exception as e:
                    self.logger.error("In execute_adb_command_with_retries: - Got exception %r trying to get metrics for command = %s " % (e, command_to_execute))

            if metrics_data is not None:
                return metrics_data
            else:
                time.sleep(1)

        return metrics_data

    def execute_adb_command(self, command_to_execute):

        metrics_data = None

        if self.check_device_connection(self.remote_address):
            try:
                self.device_handle = self.device_connection.ConnectDevice(port_path=self.port_path,
                                                                          serial=self.serial,
                                                                          rsa_keys=[self.rsa_keys])
                metrics_data = self.device_handle.Shell(command_to_execute).rstrip()
                self.logger.info("Command executed successfully")
            except Exception as e:
                self.logger.error("execute_adb_command_with_retries - Got exception %r trying to get metrics" % e)

        if metrics_data is None:
            metrics_data = self.execute_adb_command_with_retries(command_to_execute)

        return metrics_data

    def handle_up_time(self, up_time_result):
        print("Start handle_uptime...")
        if up_time_result is not None and "up" not in up_time_result:
            print("uptime not available the value is [%s]" % up_time_result)
            self.logger.error("uptime not available the value is [%s]" % up_time_result)
            return None

        days = 0
        hours = 0
        minutes = 0
        total_seconds = 0

        print("Start Handle_uptime, the uptime_result is %s ..." % up_time_result)

        try:
            extract_uptime = re.search("up (.*) user", up_time_result)
            extract_uptime = str(extract_uptime.group(1))

            factor_uptime = extract_uptime.split(',')

            for item in factor_uptime:
                if "day" in item:
                    item = item.strip()
                    days = item.split(' ')[0]
                if ":" in item:
                    item = item.strip()
                    hours = item.split(':')[0]
                    minutes = item.split(':')[1]
                if "min" in item:
                    item = item.strip()
                    minutes = item.split(' ')[0]

            total_seconds = int(days) * 24 * 60 * 60 + int(hours) * 60 * 60 + int(minutes) * 60

            print("##########    Start Handle_uptime, the uptime is %s ..." % total_seconds)

        except Exception as e:
            self.logger.error("handle_up_time - got exception: %r trying to convert uptime" % e)
            print("handle_up_time - got exception: %r trying to convert uptime" % e)
            total_seconds = None

        print("End handle_uptime...")
        return total_seconds

    def reboot_detection(self, up_time_result, deviceTime):

        up_time = self.handle_up_time(up_time_result)

        if up_time is not None and up_time < self.previous_up_time:
            self.number_of_reboot_detected = self.number_of_reboot_detected + 1
            self.device_process_reboot_handling = True

        # print("$$$$$$$$$$$$$$$$$$$$ \n\n\n\n     up_time = %s     and self.previous_uptime  = % s  "
        #       "and number_of_reboot_detected = %s \n\n\n\n $$$$$$$$$$$$$$$$$$$$" % (
        #           up_time, self.previous_up_time, self.number_of_reboot_detected))

        if up_time is not None and up_time >= 0:
            self.previous_up_time = up_time

        return self.number_of_reboot_detected

    def get_date_time_android(self, item):
        if item is None:
            return None
        item = item.replace('Date And Time: ', '')
        item = item.replace('\n', '')
        date = (parser.parse(item))
        item = date
        timeStamp = date.isoformat()
        item = re.sub(r'\..*', '', str(item))
        strRet = item.split(' ')
        return (strRet[0], strRet[1], str(timeStamp))

    def get_date_time(self, item):
        if item is None:
            return None
        item = item.replace('Date And Time: ', '')
        item = item.replace('\n', '')
        item = re.sub(r'\..*', '', item)
        strRet = item.split(' ')
        timeArray = time.strptime(item, "%Y-%m-%d %H:%M:%S")
        timeStamp = int(time.mktime(timeArray))
        return (strRet[0], strRet[1], str(timeStamp))

    def split_item(self, item):
        if item is None:
            return None
        listItem = re.split(r'\s+', item)
        return listItem

    def get_cpu_info_android(self, item):
        if item is None:
            return None
        # "400%cpu   4%user  11%nice  59%sys 326%idle   0%iow   0%irq   0%sirq   0%host"

        listItem = self.split_item(item)
        total_cpu = float(listItem[0].replace('%cpu', ''))
        user_cpu = float(listItem[1].replace('%user', ''))
        nice_cpu = float(listItem[2].replace('%nice', ''))
        sys_cpu = float(listItem[3].replace('%sys', ''))
        idle_cpu = float(listItem[4].replace('%idle', ''))
        return str(total_cpu), str(user_cpu), str(nice_cpu), str(sys_cpu), str(idle_cpu)

    def get_memory_info(self, item):
        if item is None:
            return None

        listItem = self.split_item(item)
        return listItem[1].replace('k', ''), listItem[7].replace('k', '')

    def get_space_info(self, item):
        if item is None:
            return None
        listItem = self.split_item(item)
        return listItem[4].replace('%', '')

    def get_processes_from_top_info(self, item):
        if item is None:
            return None
        listItem = self.split_item(item)
        return listItem[3]

    def get_process_info(self, listItem):
        if listItem is None:
            return None
        listItem = str(listItem).split()
        listItem[5] = str(listItem[5]).replace('M', '')
        listItem[5] = str(listItem[5]).replace('K', '')
        listItem[5] = str(listItem[5]).replace('m', '')
        listItem[5] = str(listItem[5]).replace('k', '')
        listItem[9] = str(listItem[9]).replace('%', '')

        return listItem[0], listItem[8], listItem[5], listItem[9], listItem[11]

    def parse_process_info(self, process_name, process_value, top_data_line):
        process_data = {}
        process_pid, process_cpu, MemoryKB, MemoryPercent , p_name_in_top_data_line = self.get_process_info(top_data_line)

        if p_name_in_top_data_line == process_value.lstrip():

            try:
                process_data['%s.PID' % process_name] = process_pid
                process_data['%s.Process' % process_name] = process_name
                process_data['%s.Cycles' % process_name] = ""  # todo
                process_data['%s.CPU' % process_name] = ""  # todo
                process_data['%s.CPU%%' % process_name] = float(process_cpu)
                process_data['%s.MemoryKB' % process_name] = float(MemoryKB) * float(1024)
                process_data['%s.Memory%%' % process_name] = float(MemoryPercent)
                #process_data['%s.ProcessRestart' % process_name] = ""  # todo

                if self.process_pid[process_name] != process_pid:
                    self.process_pid[process_name] = process_pid
                    if self.device_process_reboot_handling is False:    # increase the process reboot count if it is not a device reboot.
                        self.process_restart_count[process_name] += 1   # it is going to set back to false in process_info_packet

                process_data['%s.ProcessRestart' % process_name] = int(self.process_restart_count[process_name])  # todo

            except Exception as e:
                self.logger.error("In package_process_info - : %s " % e)

            return process_data

        else:
            return None

    def package_process_info(self, top_info_metrics, process_names):

        process_info_packet = {}

        top_info_metrics = str(top_info_metrics).splitlines(False)

        for top_data_line in top_info_metrics:
            for key, value in process_names.items():
                if value in top_data_line:
                    info_packet = self.parse_process_info(key, value, top_data_line)
                    if info_packet is not None:
                        process_info_packet.update(info_packet)

        if self.device_process_reboot_handling is True:
            self.device_process_reboot_handling = False  # device reboot handled in parse_process_info in above call, safe to set it back to False.

        return process_info_packet

    def package_system_info(self, date_time, disk_metrics, top_info_metrics, device_uptime_info):

        system_info_packet = None
        overall_cpu = 0.0
        idle_cpu = 0
        overall_memory = 0
        free_memory = 0
        processes = 0
        device_time = None
        no_of_reboots = 0
        space = None
        str_date = None
        str_time = None
        total_cpu = None
        date_time_kibana = None
        linux_timestamp = None

        no_of_reboots = self.reboot_detection(device_uptime_info, device_time)

        space = self.get_space_info(disk_metrics)
        if space is not None:
            int(self.get_space_info(disk_metrics))

        if date_time is not None:
            str_date, str_time, linux_timestamp = self.get_date_time_android(date_time)

        # Parsing top data
        for top_data_line in top_info_metrics:
            if '%cpu' in top_data_line and 'user' in top_data_line and 'idle' in top_data_line:
                total_cpu, user_cpu, nice_cpu, sys_cpu, idle_cpu = self.get_cpu_info_android(top_data_line)
                overall_cpu = float(total_cpu) - float(idle_cpu)
            elif 'Mem: ' in top_data_line:
                overall_memory, free_memory = self.get_memory_info(top_data_line)
            elif 'Tasks:' in top_data_line and 'running' in top_data_line:
                processes = self.get_processes_from_top_info(top_data_line)

        # create date and time as per Kibana requirement
        if str_date is not None and str_time is not None:
            date_time_kibana = str(str_date) + str(" ") + str(str_time)

        # Convert Overall CPU usage as per 100% scale instead of 800% which we get from device
        if overall_cpu is not None and total_cpu is not None:
            overall_cpu = (float(overall_cpu) / float(total_cpu)) * float(100)

        try:
            system_info_packet = {
                'DateTime.DateTime': date_time_kibana,
                'System.LinuxTimestamp': linux_timestamp,
                'System.OverallCPU': overall_cpu,
                'System.OverallMemory': overall_memory,
                'System.FreeMemory': int(free_memory),
                'System.IdleCPU': idle_cpu,
                'System.Space': space,
                'SystemCrashes.KernelCrashes': '',
                'SystemReboots.DeviceReboots': no_of_reboots,
                'TotalRunning.Processes': int(processes)
            }

        except Exception as e:
            self.logger.error("In package_setup_system_info - : %s " % e)

        return system_info_packet

    def package_android_metrics_info(self, process_names, date_time, disk_metrics, top_info_metrics, device_up_time_info):

        android_metrics_info = {} # android metrics packet is system_info_packet+process_info_packet
        sys_info = str(top_info_metrics).splitlines(False)
        process_info = top_info_metrics

        system_info_packet = self.package_system_info(date_time, disk_metrics, sys_info, device_up_time_info)
        process_info_packet = self.package_process_info(process_info, process_names)

        android_metrics_info.update(system_info_packet)
        android_metrics_info.update(process_info_packet)

        return android_metrics_info

    def get_android_metrics(self):

        if self.check_device_connection(self.remote_address):
            try:
                # metrics_date_time = str("\nDate And Time: ") + str(datetime.datetime.now())
                metrics_date_time = self.execute_adb_command(self.device_date_command)
                metrics_data_top_info = self.execute_adb_command(self.top_command)
                metrics_data_disk = self.execute_adb_command(self.disk_space_command)
                metrics_device_uptime = self.execute_adb_command(self.device_uptime_command)
                # metrics_device_uptime = None

                self.logger.info("Android commands executed successfully")

                self.file_android_metrics.writelines(str("\n") + str(metrics_date_time) + str("\n"))
                self.file_android_metrics.writelines(str("\n") + str(metrics_data_top_info))
                self.file_android_metrics.writelines(str("\n") + str(metrics_device_uptime) + str("\n"))
                self.file_android_metrics.writelines(str("\n") + str(metrics_data_disk) + str("\n"))
                self.file_android_metrics.flush()

                # print(metrics_data_top_info)

                self.android_metrics_packet = self.package_android_metrics_info(self.process_monitor_list,
                                                                                metrics_date_time,
                                                                                metrics_data_disk,
                                                                                metrics_data_top_info,
                                                                                metrics_device_uptime)
                self.logger.info("Packaged android metrics successfully")

            except Exception as e:
                self.logger.error("get_android_metrics - Got exception %r trying to get android metrics" % e)
                trace_back_output = traceback.print_exc()
                self.logger.error("Traceback : %s" % trace_back_output)
                return None
        else:
            self.logger.error("check_connection Failed")

        return self.android_metrics_packet


# process_names_to_watch = {'Audioserver':' audioserver'}
# process_names_to_watch = {'Audioserver':' audioserver', 'com.android.phone': ' com.android.phone'}
process_names_to_watch = {'Audioserver':' audioserver', 'com.android.phone': ' com.android.phone', 'com.byton.nav':' com.byton.nav'}

android_handler = IceAndroidMetricsCollection("Byton", "", process_names_to_watch, "192.168.111.11", "5555", logger)

for x in range(0,1000):
    print(android_handler.get_android_metrics())
    time.sleep(1)