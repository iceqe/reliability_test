
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__))+"/functional_module")

import functional_execution as func_module


class IceFunctionalScenarioHandler(object):

    def __init__(self, functional_scenario_info, logger):

        self.func_tests_info = functional_scenario_info

        self.logger = logger

    #def functional_scenario_handler(self):

    def stop_functional_scenarios(self):
        print("stopping functional scenarios...")


