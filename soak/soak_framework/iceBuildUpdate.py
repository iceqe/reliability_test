
import time
import os
from jenkinsapi import jenkins
import SSHLibrary
import re
import netifaces as ni
import urllib2
from artifactory import ArtifactoryPath


class IceBuildUpdateHandler(object):
    def __init__(self, build_update_info, logger):

        self.milestone = build_update_info["milestone"]
        self.jenkins_url = build_update_info["jenkins_url"]
        self.jenkins_user = build_update_info["jenkins_user"]
        self.jenkins_password = build_update_info["jenkins_password"]

        self.ficBuildNumber = build_update_info["ficBuildNumber"]
        self.ficBuildUpdate = build_update_info["ficBuildUpdate"] # True or False , whether we want to perform FIC update.

        self.sicBuildNumber = build_update_info["sicBuildNumber"]
        self.sicBuildUpdate = build_update_info["sicBuildUpdate"]  # True or False , whether we want to perform SIC update.

        self.cgwBuildNumber = build_update_info["cgwBuildNumber"]
        self.cgwBuildUpdate = build_update_info["cgwBuildUpdate"] # True or False , whether we want to perform CGW update.

        self.atbaBuildNumber = build_update_info["atbaBuildNumber"]
        self.atbaBuildUpdate = build_update_info["atbaBuildUpdate"] # True or False , whether we want to perform ATBA update.

        self.ethernetUpdate = build_update_info["ethernetUpdate"] # True or False , wether we want to perform this update.
        self.otaUpdate = build_update_info["otaUpdate"] # True or False , whether we want to perfrom this update.
        self.otaBuildVersion = build_update_info["otaBuildVersion"]
        self.server_package_for_ota = build_update_info["server_package_for_ota"] #"http://10.13.132.15:8080/download/" #package server for downloading, the package should be under this url directly"

        self.cgw_atba_artifactory_path = build_update_info["cgw_atba_artifactory_path"] #"https://btnussfoart01.byton.com/artifactory/webapp/#/artifacts/browse/tree/General/ICE-firmware/doubleDutchman-AP/build/2520-21278266642f2f125973f7ac89f281a3f8b78a33/images/package.swu"
        self.cgw_atba_package = build_update_info["cgw_atba_package"]
        self.cgw_atba_artifactory_or_local = build_update_info["cgw_atba_artifactory_or_local"]  # from where to get the build? local or artifactory
        self.cgw_atba_user = build_update_info["cgw_atba_user"]             #'byton'
        self.cgw_atba_password = build_update_info["cgw_atba_password"]     # 'byton!'

        self.logger = logger

        self.fic_ip = "192.168.111.10"
        self.cgw_ip = "192.168.111.1"
        self.atba_ip = "192.168.111.2"
        self.sshLibrary = None
        self.gws = ni.gateways()
        self.gateway = self.gws['default'][self.gws['default'].keys()[0]][0]

        self.update_results = {"ethernetUpdate": "Failed",
                               "otaUpdate": "Failed",
                               "cgwBuildUpdate": "Failed",
                               "atbaBuildUpdate": "Failed"
                               }

    def login_QNX(self):
        try:
            self.sshLibrary = SSHLibrary.SSHLibrary()

            self.sshLibrary.open_connection(host='192.168.111.10', port=22)
            self.sshLibrary.login('root', '')
            return self.sshLibrary

            # sshLibrary.open_connection(host='192.168.111.10', port=22)
            # ssshLibrary.login('root', '')
            # return sshLibrary
        except:
            return None

    def login_cgw_atb(self, host):
        try:
            self.sshLibrary = SSHLibrary.SSHLibrary()
            self.sshLibrary.open_connection(host=host, port=22)
            self.sshLibrary.login(self.cgw_atba_user, self.cgw_atba_password)
            return self.sshLibrary
        except:
            self.logger.error("login_QNX Failed")
            return None


    def cgw_atba_get_package_from_artifactory(self, url):
        url = url.replace("/webapp/#/artifacts/browse/tree/General", '')
        objpathPackage = ArtifactoryPath(url, auth=('cavin_rui', 'cavin_rui'))
        pathPackage = os.path.join(os.getcwd(), os.path.basename(url))
        # return pathPackage
        self.logger.warn("start downloading package.swu from artifactory")
        with objpathPackage.open() as fd:
            with open(pathPackage, "wb") as out:
                out.write(fd.read())
        return pathPackage

    def get_package_from_jenkins(self):
        artifact = None
        text_artifact = None

        objJenkins = jenkins.Jenkins(self.jenkins_url, self.jenkins_user, self.jenkins_password)
        job = objJenkins.get_job("ice-build-ap-stable")
        try:
            build = job.get_build(int(self.ficBuildNumber))
        except:
            self.logger.error("build #%s not found in job 'ice-build-ap-stable'" % self.ficBuildNumber)
            raise
        artifacts = build.get_artifact_dict()

        for artifact_key in artifacts.keys():
            if "OTA" in artifact_key and self.milestone in artifact_key:
                text_artifact = artifacts[artifact_key]
                artifact = artifact_key
                break
        path_artifact = os.path.join(os.getcwd(), artifact)
        if not os.path.exists(path_artifact):
            self.logger.warn("downloading package from jenkins, please wait")
            text_artifact.save_to_dir(os.getcwd())  # , strict_validation=True)
        else:
            self.logger.warn("package exist, skip downloading")
        return path_artifact

    def perform_ethernet_update(self, pathBuild, isForceUpdate=True):
        self.sshLibrary = self.login_QNX()
        if self.sshLibrary is None:
            self.logger.error("QNX is down")
            return 1
        if not os.path.exists(pathBuild):
            self.logger.warn('local file %s does not exist' % pathBuild)
            return 1

        self.sshLibrary.execute_command("rm -rf /data/*")
        match = re.search(r'ICE.*_S_(\d+)_(FIC.*)_OTA.tar.gz', pathBuild)
        expectBuildNum = match.group(1)
        expectBuildInfo = match.group(2)
        output = self.sshLibrary.execute_command('byton_info.sh')
        self.logger.warn("original byton info")
        self.logger.warn(output)
        realBuildNum = re.search(r'Byton Build Name \[[^\]]+_(\d+)\].*\n.*Byton Build Time \[([^\]]+)\]', output).group(
            1)

        updatePath = "%s -> %s" % (realBuildNum, expectBuildNum)
        self.logger.warn("start Ethernet update: %s" % updatePath)
        if realBuildNum == expectBuildNum and not isForceUpdate:
            self.logger.warn("The same build number, pass it")
            return 1

        sizeLocal = os.stat(pathBuild).st_size

        self.logger.warn("upload build package to FIC, please wait for a while")
        self.sshLibrary.put_file(pathBuild, '/data/ice-ota-ethernet.tar.gz')

        if not str(sizeLocal) in self.sshLibrary.execute_command("ls -l /data/ice-ota-ethernet.tar.gz"):
            self.logger.warn("upload build package to FIC failed, please check the size both of local and remote")
            return 1

        self.logger.warn("execute updating: ota-update /data/ice-ota-ethernet.tar.gz")
        self.sshLibrary.write("ota-update /data/ice-ota-ethernet.tar.gz &")

        self.sshLibrary.set_client_configuration(timeout="600 seconds")
        self.sshLibrary.write("slog2info -w | grep OTA")
        self.sshLibrary.read_until("Extracting OTA image tar file")
        self.logger.warn("Extracting OTA image tar file")
        self.sshLibrary.read_until("Extracting OTA image finished.")
        self.logger.warn("Extracting OTA image finished.")
        self.sshLibrary.read_until("cp /lib64/libspmi_client.so /data")
        self.logger.warn("cp /lib64/libspmi_client.so /data")
        self.sshLibrary.read_until("swdl-powersafe -d /data/ice-ota-images -g main=gpt_main0.bin,backup=gpt_backup0.bin")
        self.logger.warn("swdl-powersafe -d /data/ice-ota-images -g main=gpt_main0.bin,backup=gpt_backup0.bin")
        self.sshLibrary.close_all_connections()
        timeOut = 500
        while timeOut:
            self.sshLibrary = self.login_QNX()
            if self.sshLibrary:
                self.sshLibrary.close_connection()
            else:
                timeOut -= 1
                time.sleep(1)
                break

        timeOut = 500
        while timeOut:
            self.sshLibrary = self.login_QNX()
            if self.sshLibrary:
                break
            else:
                self.logger.warn("waiting for QNX rebooting")
                timeOut -= 1
                time.sleep(1)

        output = self.sshLibrary.execute_command('byton_info.sh')
        self.logger.warn("updated byton info")
        self.logger.warn(output)
        match = re.search(r'Byton Build Name \[[^\]]+(FIC[^\_]+_.*)_S_(\d+)\].*\n.*Byton Build Time \[[^\]]+\]', output)
        realBuildNum = match.group(2)
        realBuildInfo = match.group(1)
        self.sshLibrary.close_all_connections()
        if not expectBuildNum == realBuildNum or not realBuildInfo == expectBuildInfo:
            self.logger.error("Ethernet Update failed, current version is %s, expect is %s" % (realBuildNum, expectBuildNum))
            return 1
        else:
            self.logger.warn("Ethernet update %s is successful" % updatePath)
            return 0

    def perform_ota_update(self, version):

        self.sshLibrary = self.login_QNX()
        if self.sshLibrary is None:
            self.logger.error("QNX is down")
            return 1

        self.sshLibrary.execute_command("rm -rf /data/*")
        self.sshLibrary.execute_command(
            "route -n add -net %s %s" % (re.search(r'://([^\:|^/]+)[:|/]', self.server_package_for_ota).group(1), self.gateway))
        match = re.search(r'ICE.*_S_(\d+)_(FIC.*)_OTA.tar.gz', version)
        expectBuildNum = match.group(1)
        expectBuildInfo = match.group(2)
        output = self.sshLibrary.execute_command('byton_info.sh')
        realBuildNum = re.search(r'Byton Build Name \[[^\]]+_(\d+)\].*\n.*Byton Build Time \[([^\]]+)\]',
                                 output).group(1)

        updatePath = "%s -> %s" % (realBuildNum, expectBuildNum)
        self.logger.warn("start OTA update: %s" % updatePath)
        #    if realBuildNum == expectBuildNum:
        #        self.logger.warn("The same build number, pass it")
        #        return
        urlPackage = "%s/SCM-%s/%s" % (self.server_package_for_ota.strip('/'), expectBuildNum, version)
        req = urllib2.Request('%s.size' % urlPackage)
        response = urllib2.urlopen(req)
        size = response.read()
        req = urllib2.Request('%s.sha256' % urlPackage)
        response = urllib2.urlopen(req)
        sha256 = response.read()
        command = '''BytonCgwManager -d "%s" "%s" %s''' % (urlPackage, sha256, size)
        self.sshLibrary.set_client_configuration(timeout="600 seconds")
        self.sshLibrary.write(command)
        self.sshLibrary.read_until_regexp("100\s+\d+\w\s+100 ")
        self.logger.warn("package download completed")
        self.sshLibrary.write("slog2info -w | grep cgw")
        output = self.sshLibrary.read_until("Calculating SHA 256 for /data/ice-ota-image.tar.gz")
        self.logger.warn("Calculating SHA 256 for /data/ice-ota-image.tar.gz")
        output = self.sshLibrary.read_until("Successfully notified installer PPS")
        self.logger.warn("Successfully notified installer PPS")
        self.sshLibrary.close_all_connections()
        self.sshLibrary = self.login_QNX()
        self.sshLibrary.set_client_configuration(timeout="600 seconds")
        self.sshLibrary.write("slog2info -w | grep OTA")
        output = self.sshLibrary.read_until("Extracting OTA image tar file")
        self.logger.warn("Extracting OTA image tar file")
        output = self.sshLibrary.read_until("Extract image successful")
        self.logger.warn("Extract image successful")
        output = self.sshLibrary.read_until("lib spmi copied")
        self.logger.warn("lib spmi copied")
        self.sshLibrary.close_all_connections()
        time.sleep(30)  # waiting for reboot
        timeOut = 500
        while timeOut:
            self.sshLibrary = self.login_QNX()
            if self.sshLibrary:
                self.sshLibrary.close_connection()
                break
            else:
                self.logger.warn("waiting for QNX rebooting")
                timeOut -= 1
                time.sleep(1)

        self.sshLibrary = self.login_QNX()
        output = self.sshLibrary.execute_command('byton_info.sh')
        match = re.search(r'Byton Build Name \[[^\]]+(FIC[^\_]+_.*)_S_(\d+)\].*\n.*Byton Build Time \[[^\]]+\]',
                          output)
        realBuildNum = match.group(2)
        realBuildInfo = match.group(1)
        self.sshLibrary.close_all_connections()
        if not expectBuildNum == realBuildNum or not realBuildInfo == expectBuildInfo:
            self.logger.error("OTA update failed, current version is %s, expect is %s" % (realBuildNum, expectBuildNum))
            return 1
        else:
            self.logger.warn("OTA update %s is successful" % updatePath)
            return 0

    def perform_cgw_atba_update(self, pathBuild, updateType):

        host = None
        if updateType == 'CGW':
            host = self.cgw_ip      #'192.168.111.2'
        elif updateType == 'ATBA':
            host = self.atba_ip      #'192.168.111.1'

        self.sshLibrary = self.login_cgw_atb(host)
        if self.sshLibrary is None:
            self.logger.error("%s is down" % updateType)
            return 1
        if not os.path.exists(pathBuild):
            self.logger.warn('local file %s does not exist' % pathBuild)
            return 1

        self.sshLibrary.execute_command("rm -rf /tmp/package.swu")
        output = self.sshLibrary.execute_command('cat /etc/issue')
        currentBuildNum = re.search(r'Linux Byton\s+(\S+)\s+', output).group(1)
        self.logger.warn("start %s update" % updateType)
        self.logger.warn("current Build Number is: %s" % currentBuildNum)

        sizeLocal = os.stat(pathBuild).st_size

        self.logger.warn("upload build package to %s, please wait for a while" % updateType)
        self.sshLibrary.put_file(pathBuild, '/tmp/package.swu')

        if not str(sizeLocal) in self.sshLibrary.execute_command("ls -l /tmp/package.swu"):
            self.logger.warn("upload build package to %s failed, please check the size both of local and remote" % updateType)
            return 1

        self.logger.warn("execute updating: sudo dutch-update.sh -u /tmp/package.swu")
        self.sshLibrary.write("sudo dutch-update.sh -u /tmp/package.swu")
        self.sshLibrary.set_client_configuration(timeout="180 seconds")
        self.sshLibrary.read_until("MD5s match")
        self.logger.warn("Check md5 over")
        self.logger.warn("Write image to /dev/mmcblk0p1 partition")
        self.sshLibrary.read_until("Rebooting the system")
        self.logger.warn("Rebooting the system")
        self.sshLibrary.execute_command('sudo reboot')
        self.sshLibrary.close_all_connections()
        time.sleep(5)
        timeOut = 500
        while timeOut:
            self.sshLibrary = self.login_cgw_atb(host)
            if self.sshLibrary:
                break
            else:
                self.logger.warn("waiting for %s rebooting" % updateType)
                timeOut -= 1
                time.sleep(1)

        output = self.sshLibrary.execute_command('cat /etc/issue')
        realBuildNum = re.search(r'Linux Byton\s+(\S+)\s+', output).group(1)
        self.logger.warn("Build Number after updating is: %s" % realBuildNum)
        self.sshLibrary.close_all_connections()
        if currentBuildNum == realBuildNum:
            self.logger.error("%s Update failed, build number not changed, current version is %s, expect is %s" % (
            updateType, currentBuildNum, realBuildNum))
            return 1
        else:
            self.logger.warn("%s update is successful" % updateType)
            return 0

    def build_update_handler(self):

        # perform fic update
        self.update_results = {"ethernetUpdate": "Failed",
                               "otaUpdate": "Failed",
                               "cgwBuildUpdate": "Failed",
                               "atbaBuildUpdate": "Failed"
                               }

        if self.ethernetUpdate is True:
            if self.ficBuildUpdate is True and self.ficBuildNumber is not None:
                local_build_path = self.get_package_from_jenkins()
                if self.perform_ethernet_update(local_build_path) == 0:
                    self.update_results["ethernetUpdate"] = "Successful"

            else:
                self.logger.error("Not enough info to download the build from Jenkins, please check the config file")
        elif self.otaUpdate is True:
            if self.ficBuildNumber is not None:
                if self.perform_ota_update(self.otaBuildVersion) == 0:
                    self.update_results["otaUpdate"] = "Successful"
            else:
                self.logger.error(" OTA Update: Not enough info to perform OTA update, please check the config file")
        else:
            self.logger.error(" Ethernet or OTA Update info is not provided, please check config file")

        # Perform CGW update

        cgw_atba_path = None
        if self.cgwBuildUpdate is True and self.cgwBuildNumber is not None:

            if self.cgw_atba_artifactory_or_local == "artifactory":
                cgw_atba_path = self.cgw_atba_get_package_from_artifactory(self.cgw_atba_artifactory_path)
            elif self.cgw_atba_artifactory_or_local == "local":
                cgw_atba_path = os.path.join(os.getcwd(), self.cgw_atba_package)

            ret_val = self.perform_cgw_atba_update(cgw_atba_path, "CGW")

            if ret_val == 0:
                self.logger.info("CGW update successful")
                self.update_results["cgwBuildUpdate"] = "Successful"
        else:
            self.logger.error(" CGW Update: Not enough info for CGW update please check the config file")

        # Perform ATB update
        cgw_atba_path = None
        if self.atbaBuildUpdate is True and self.atbaBuildNumber is not None:
            if self.cgw_atba_artifactory_or_local == "artifactory":
                cgw_atba_path = self.cgw_atba_get_package_from_artifactory(self.cgw_atba_artifactory_path)
            elif self.cgw_atba_artifactory_or_local == "local":
                cgw_atba_path = os.path.join(os.getcwd(), self.cgw_atba_package)

            ret_val = self.perform_cgw_atba_update(cgw_atba_path, "ATBA")
            if ret_val == 0:
                self.logger.info("ATBA update successful")
                self.update_results["atbaBuildUpdate"] = "Successful"
        else:
            self.logger.error(" ATB Update: Not enough info for ATB update please check the config file")

        return self.update_results

    def stop_all_build_update(self):
        print("Stopping all build updates...")