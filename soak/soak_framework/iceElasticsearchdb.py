
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import re
import time
import datetime
import iceElkMappingsConfig


class IceElasticsearchDbHandler(object):
    def __init__(self, obj_elk_info, logger):

        self.dynamic_mapping = None

        self.dynamic_mapping_frame = iceElkMappingsConfig.elk_setup_mapping_frame
        self.process_mapping_packet = iceElkMappingsConfig.elk_process_list_mapping_frame
        self.process_monitor_list = iceElkMappingsConfig.process_names_to_monitor

        self.dynamic_elk_index = None
        self.today_elk_index = None
        self.url = obj_elk_info["elk_url"]
        self.INDEX = obj_elk_info["elk_INDEX"]
        self.TYPE_DOC = obj_elk_info["elk_TYPE_DOC"]
        self.testSetupNumber = obj_elk_info["testSetupNumber"]
        self.testBuildNumber = obj_elk_info["testBuildNumber"]
        self.vntNumber = obj_elk_info["vntNumber"]
        self.testSetupControlIp = obj_elk_info["testSetupControlIp"]
        self.carsVmIp = obj_elk_info["carsVmIp"]
        self.device_hw_type = obj_elk_info["deviceHwType"]
        self.fStep = obj_elk_info["fStep"]

        self.logger = logger

        self.previous_uptime = 0
        self.number_of_reboot_detected = 0
        self.device_process_reboot_handling = False

        self.process_pid = {}
        for key in self.process_monitor_list.keys():
            self.process_pid[key] = -1

        self.process_restart_count = {}
        for key in self.process_monitor_list.keys():
            self.process_restart_count[key] = -1

        self.es = Elasticsearch([self.url])
        try:
            self.countExist = self.es.count(self.INDEX, self.TYPE_DOC)['count']
        except:
            self.countExist = 0

    def define_mappings(self):
        process_mapping_packet = ""

        for key in self.process_monitor_list.keys():
            p_name_mapping_packet = self.process_mapping_packet.format(key)

            process_mapping_packet = process_mapping_packet + "\n" + p_name_mapping_packet

        final_mapping = self.dynamic_mapping_frame.format(process_mapping_packet)
        self.dynamic_mapping = re.sub(r"\s+", "", final_mapping)

    def handle_uptime(self, uptimeResult):
        self.logger.info("Start handle_uptime...")
        if uptimeResult is not None and "up" not in uptimeResult:
            self.logger.error("uptime not available the value is [%s]" % uptimeResult)
            return None

        days = 0
        hours = 0
        minutes = 0
        total_seconds = 0

        self.logger.info("Start Handle_uptime, the uptime_result is %s ..." %uptimeResult)

        try:
            extract_uptime = re.search("up (.*) user", uptimeResult)
            extract_uptime = str(extract_uptime.group(1))

            factor_uptime = extract_uptime.split(',')

            for item in factor_uptime:
                if "day" in item:
                    item = item.strip()
                    days = item.split(' ')[0]
                if ":" in item:
                    item = item.strip()
                    hours = item.split(':')[0]
                    minutes = item.split(':')[1]
                if "min" in item:
                    item = item.strip()
                    minutes = item.split(' ')[0]

            total_seconds = int(days) * 24 * 60 * 60 + int(hours) * 60 * 60 + int(minutes) * 60
        except Exception as e:
            self.logger.error("got exception: %r trying to convert uptime" % e)
            total_seconds = None

        self.logger.info("End handle_uptime...")
        return total_seconds

    def reboot_detection(self, uptimeResult, deviceTime):

        uptime = self.handle_uptime(uptimeResult)

        if uptime is not None and uptime < self.previous_uptime:
            self.number_of_reboot_detected = self.number_of_reboot_detected + 1
            self.device_process_reboot_handling = True

        if uptime is not None and uptime >= 0:
            self.previous_uptime = uptime

        return self.number_of_reboot_detected

    def GetDateTime(self, item):
        self.logger.info("Start GetDateTime 0...")
        item = item.replace('Date And Time: ', '')
        item = item.replace('\n', '')
        item = re.sub(r'\..*', '', item)
        strRet = item.split(' ')
        timeArray = time.strptime(item, "%Y-%m-%d %H:%M:%S")
        timeStamp = int(time.mktime(timeArray))
        return (strRet[0], strRet[1], str(timeStamp))

    def SplitItem(self, item):
        listItem = re.split(r'\s+', item)
        return listItem

    def GetCPUInfo(self, item):
        listItem = self.SplitItem(item)
        usage = float(listItem[2].replace('%', '')) + float(listItem[4].replace('%', ''))
        return str(usage), str(100-usage)

    def GetMemoryInfo(self, item):
        listItem = self.SplitItem(item)
        return listItem[1].replace('M', ''), listItem[3].replace('M', '')

    def GetSpaceInfo(self, item):
        listItem = self.SplitItem(item)
        return listItem[4].replace('%', '')

    def GetProcesses_from_Top_Info(self, item):
        listItem = self.SplitItem(item)
        return listItem[1]

    def GetProcessInfo(self, listItem):
        listItem = str(listItem).translate(None, "%Kk")
        listItem = str(listItem).split()
        return listItem[0], listItem[3], listItem[5], listItem[6]



    def package_process_info(self, process_name, process_value, hogs_data_line):
        process_data = {}
        process_pid, process_cpu, MemoryKB, MemoryPercent = self.GetProcessInfo(hogs_data_line)

        try:
            process_data['%s.PID' % process_name] = process_pid
            process_data['%s.Process' % process_name] = process_name
            process_data['%s.Cycles' % process_name] = ""  # todo
            process_data['%s.CPU' % process_name] = ""  # todo
            process_data['%s.CPU%%' % process_name] = int(process_cpu)
            process_data['%s.MemoryKB' % process_name] = int(MemoryKB)
            process_data['%s.Memory%%' % process_name] = int(MemoryPercent)

            if self.process_pid[process_name] != process_pid:
                self.process_pid[process_name] = process_pid
                if self.device_process_reboot_handling is False:  # increase the process reboot count if it is not a device reboot.
                    self.process_restart_count[process_name] += 1  # it is going to set back to false in process_info_packet

            process_data['%s.ProcessRestart' % process_name] = int(self.process_restart_count[process_name])  # todo

        except Exception as e:
            self.logger.error("In package_process_info - : %s " % e)

        return process_data

    def package_setup_system_info(self, date_time, disk_metrics, top_info_metrics, metrics_device_uptime):
        self.logger.info("Start package_setup_system_info...")

        source_packet = {}
        OverallCPU = 0
        IdleCPU = 0
        OverallMemory = 0
        FreeMemory = 0
        Processes = 0
        no_of_reboots = 0
        device_time = None


        no_of_reboots = self.reboot_detection(metrics_device_uptime, device_time)


        Space = self.GetSpaceInfo(disk_metrics)
        strDate, strTime, LinuxTimestamp = self.GetDateTime(date_time)

        # Parsing top data
        for top_data_line in top_info_metrics:
            if 'CPU states' in top_data_line:
                OverallCPU, IdleCPU = self.GetCPUInfo(top_data_line)
            elif 'Memory' in top_data_line:
                OverallMemory, FreeMemory = self.GetMemoryInfo(top_data_line)
            elif 'Processes' in top_data_line:
                Processes = self.GetProcesses_from_Top_Info(top_data_line)
        try:
            source_packet = {
                'DateTime.DateTime': strDate + "-" + strTime,
                # 'DateTime.Time': strTime,
                'TestSetup.Number': self.testSetupNumber,
                'TestSetup.ControlIP': self.testSetupControlIp,
                'TestSetup.Device_Hw_Type': self.device_hw_type,
                'TestSetup.DeviceType': '',
                'TestSetup.DeviceIP': '192.168.11.10',
                'TestSetup.RemoteAccessPort': '',
                'TestSetup.Data1': '',
                'TestSetup.Data2': '',
                'TestBuild.Branch': '',
                'TestBuild.Number': self.testBuildNumber,
                'TestBuild.GitHash': '',
                'VNT.Number': self.vntNumber,
                'TestBuild.HardwareVersion': '',
                'TestBuild.Milestone': '',
                'TestBuild.ReleaseInfo': '',
                'Fstep': self.fStep,
                'TestType.Type': '',
                'TestType.RuntimeHours': '',
                'TestType.Scenario': '',
                'TestType.Data1': '',
                'System.LinuxTimestamp': LinuxTimestamp,
                'System.OverallCPU': OverallCPU,
                'System.OverallMemory': OverallMemory,
                'System.FreeMemory': int(FreeMemory),
                'System.IdleCPU': IdleCPU,
                'System.Space': Space,
                'SystemCrashes.KernelCrashes': '',
                'SystemReboots.DeviceReboots': no_of_reboots,
                'TotalRunning.Processes': int(Processes)
            }

        except Exception as e:
            self.logger.error("In package_setup_system_info - : %s " % e)

        self.logger.info("End package_setup_system_info...")
        return source_packet

    def package_process_info_packet(self, hog_info_metrics, process_names):

        process_info_packet = {}

        for hogs_data_line in hog_info_metrics:
            for key, value in process_names.items():
                if value in hogs_data_line:
                    info_packet = self.package_process_info(key, value, hogs_data_line)
                    process_info_packet.update(info_packet)

        if self.device_process_reboot_handling is True:
            self.device_process_reboot_handling = False  # device reboot handled in parse_process_info in above call, safe to set it back to False.

        return process_info_packet

    def create_es_packet(self, process_names, date_time, disk_metrics, top_info_metrics, hog_info_metrics, metrics_device_uptime):

        self.countExist += 1

        base_packet = self.package_setup_system_info(date_time, disk_metrics, top_info_metrics, metrics_device_uptime)
        process_info_packet = self.package_process_info_packet(hog_info_metrics, process_names)

        # source packet in es_packet will be base_packet+process_info_packet
        base_packet.update(process_info_packet)

        es_packet = [{"_index": self.today_elk_index,
                      "_type": self.TYPE_DOC,
                      "_id": self.countExist,
                      "_source": base_packet
                      }]

        return es_packet

    def send_data_to_es_db(self, date_time, disk_metrics, top_info_metrics, hog_info_metrics, metrics_device_up_time):
        #global es

        actions = self.create_es_packet(self.process_monitor_list, date_time, disk_metrics, top_info_metrics, hog_info_metrics, metrics_device_up_time)
        self.logger.error(actions)
        print(actions)

        try:
            helpers.bulk(self.es, actions)
        except Exception as e:
            self.logger.error("ElasticSearch: Problem inserting data using bulk api: %s " % e)

    def create_dynamic_index(self):
        return str(self.INDEX) + "-" + str(datetime.date.today())

    def init_index(self):
        initialization = False
        self.today_elk_index = self.create_dynamic_index()

        # Initialize the mappings
        self.define_mappings()

        mapping = eval(str(self.dynamic_mapping))

        try:
            if self.es.indices.exists(self.today_elk_index):
                self.logger.info('Mapping Exist No need to create')
                self.es.indices.put_mapping(index=self.today_elk_index, doc_type=self.TYPE_DOC, body=mapping)
                get_mapp = self.es.indices.get_mapping(index=self.INDEX, doc_type=self.TYPE_DOC)
                initialization = True
            else:
                self.es.indices.create(self.today_elk_index)
                self.es.indices.put_mapping(index=self.today_elk_index, doc_type=self.TYPE_DOC, body=mapping)
                self.logger.info('new mapping created')
                initialization = True
        except Exception as e:
            self.logger.error("ElasticSearch: init_index() %s " % e)
            initialization = False

        return initialization


    def init_index1(self):
        initialization = False

        # Initialize the mappings
        self.define_mappings()

        mapping = eval(str(self.dynamic_mapping))

        try:
            if self.es.indices.exists(self.INDEX):
                self.logger.info('Mapping Exist No need to create')
                self.es.indices.put_mapping(index=self.INDEX, doc_type=self.TYPE_DOC, body=mapping)
                get_mapp = self.es.indices.get_mapping(index=self.INDEX, doc_type=self.TYPE_DOC)
                initialization = True
            else:
                self.es.indices.create(self.INDEX)
                self.es.indices.put_mapping(index=self.INDEX, doc_type=self.TYPE_DOC, body=mapping)
                self.logger.info('new mapping created')
                initialization = True
        except Exception as e:
            self.logger.error("ElasticSearch: init_index() %s " % e)
            initialization = False

        return initialization