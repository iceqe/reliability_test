{\rtf1\ansi\ansicpg1252\cocoartf1671\cocoasubrtf200
{\fonttbl\f0\fswiss\fcharset0 Helvetica;\f1\fnil\fcharset0 HelveticaNeue;}
{\colortbl;\red255\green255\blue255;\red0\green0\blue0;}
{\*\expandedcolortbl;;\cssrgb\c0\c0\c0;}
\margl1440\margr1440\vieww22300\viewh9200\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0

\f0\fs28 \cf0 1. Get all the files under soak_framework directory\
2. Just run following command: python iceSoak.py\
3. it will start soak script to get the metrics from FIC and start pushing it in to text file \'93ficMetrics.txt\'94.\
\
Note: \
1. Make sure python 2.7 is installed on the Ubuntu/MAC where you are running this script, Windows execution is not verified yet.\
 2. Python 3.x is not supported right now, going to change code for 3.x\
\
Packages needed.\
Please install following packages before starting the soak.\cf2 \expnd0\expndtw0\kerning0
\outl0\strokewidth0 \strokec2 \
\pard\pardeftab720\sl320\sa280\partightenfactor0
\cf2 sudo pip install pexpect\
\pard\pardeftab720\sl320\sa280\partightenfactor0
\cf2 \outl0\strokewidth0 sudo pip install elasticsearch\
\pard\pardeftab720\sl320\sa280\partightenfactor0
\cf2 sudo pip install robotframework-sshlibrary\cf2 \outl0\strokewidth0 \strokec2 \
\pard\pardeftab720\sl320\sa280\partightenfactor0
\cf2 sudo pip install jenkins\
sudo pip install jenkinsapi\
sudo pip install netifaces\
sudo pip install artifactory
\f1 \
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0

\f0\fs24 \cf0 \kerning1\expnd0\expndtw0 \outl0\strokewidth0 \
}