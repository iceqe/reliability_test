import importlib
import os

from general import get_test_case_settings

tester = None

def basic_sceanrio(version):
    global tester
    test_info = get_test_case_settings(version)
    RT = importlib.import_module(test_info['utils'])

    dbc_path = os.path.dirname(os.path.abspath(__file__)) + "/" + test_info['DBC']

    tester = RT.ReliabilityTester(dbc_path)

    # Manual Unlock then Power Mode ON
    tester.set_front_doors(1)
    tester.set_hv_enable(1)
    tester.set_gear('drive')
    tester.set_speedometer(5)
    tester.set_front_doors(0)
    # tester.vehicle_power_management_on()

    funcs = {tester.loop_odometer: (0, 1000000, 1000, 1.25,),
             tester.loop_speedometer: (0, 360, 1, 1,),
             tester.loop_range_and_battery: (0, 1000, 10, 1.5,),
             tester.loop_warnings: (5,)}

    tester.multithread(funcs)


def stop_basic_scenario():
    global tester
    print("stoping the test")

if __name__ == '__main__':
    import sys

    basic_sceanrio(sys.argv[1])
