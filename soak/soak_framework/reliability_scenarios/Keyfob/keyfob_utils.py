import YoctoDio
from time import sleep

serial_number = "MAXIIO02-D7FE2"


def keyfob_on():
    # yocto = YoctoDio.YoctoDio(serial, True)
    yocto = YoctoDio.YoctoDio(True)
    yocto.set_pin(0, 1)
    sleep(3)


# def keyfob_button(action, serial_number):
def keyfob_button(action):
    yocto = YoctoDio.YoctoDio(True)
    keyfob_on()
    if action.upper() == "LOCK":
        yocto.set_multiple_pins(13)
        print "lock"
    elif action.upper() == "UNLOCK":
        yocto.set_multiple_pins(11)
        print "unlock"
    elif action.upper() == "TRUNK":
        yocto.set_multiple_pins(7)
        print "trunk"
    else:
        print "Invalid selection. Please choose from LOCK, UNLOCK, or TRUNK."
    sleep(3)

#
# def lock(serial_number):
#     print "Locking car."
#     yocto = YoctoDio.YoctoDio(serial_number, True)
#     keyfob_on(serial_number)
#     yocto.set_multiple_pins(13)
#     sleep(3)
#
#
# def unlock(serial_number):
#     print "Unlocking car."
#     yocto = YoctoDio.YoctoDio(serial_number, True)
#     keyfob_on(serial_number)
#     yocto.set_multiple_pins(11)
#     sleep(3)
#
#
# def trunk_open(serial_number):
#     print "Opening trunk."
#     yocto = YoctoDio.YoctoDio(serial_number, True)
#     keyfob_on(serial_number)
#     yocto.set_multiple_pins(7)
#     sleep(3)


if __name__ == '__main__':

    # keyfob_button("lock", serial_number)
    # keyfob_button("uNlOck", serial_number)
    # keyfob_button("TRUNK", serial_number)
    keyfob_button("lock")
    keyfob_button("uNlOck")
    keyfob_button("loCK")
    keyfob_button("TRUNK")
    keyfob_button("lck")
