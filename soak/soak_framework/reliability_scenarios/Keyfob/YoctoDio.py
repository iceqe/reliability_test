#!/usr/bin/python
from yoctopuce.yocto_digitalio import *


class YoctoDio:
    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self, keyfob=False):
    # def __init__(self, serial, keyfob=False):
        self.keyfob = keyfob
        """
        Initialize based on the specific serial number of the Yocto device.  The open_
        :param serial: serial of the yocto device. Can be found in dmesg.
        :param keyfob: set to True if connected to keyfob.
        """
        # Setup the API to use local USB devices
        errmsg = YRefParam()
        if YAPI.RegisterHub("usb", errmsg) != YAPI.SUCCESS:
            raise Exception("Yocto init error: {}".format(errmsg.value))

        # self.io = YDigitalIO.FindDigitalIO(serial + '.digitalIO')
        self.io = YDigitalIO.FirstDigitalIO()

        if not (self.io.isOnline()):
            raise Exception("Yocto DIO device not connected. Check connection and verify correct serial number.")
        else:
            #self.io.set_outputVoltage(YDigitalIO.OUTPUTVOLTAGE_EXT_V)
            self.io.set_outputVoltage(YDigitalIO.OUTPUTVOLTAGE_USB_3V)
            self.io.set_portPolarity(0)  # polarity set to regular
            self.io.set_portOpenDrain(0)  # No open drain

            if keyfob is True:
                self.io.set_portDirection(0xFF)  # Hardcode all pins as output
                self.io.set_portState(0x0E)  # Set default port state
            else:
                self.io.set_portDirection(0x0F)  # Hardcode 0..3 as output, 4..7 as input
                self.io.set_portState(0x00)  # Set default port state

    def set_pin(self, pin_number, state):
        """
        Set the output state of a pin.
        :param pin_number: The pin number to set.
        :param state: The state to set the pin (0 or 1)
        """
        if self.keyfob is False:
            if int(pin_number) < 0 or int(pin_number) > 3:
                raise Exception("Only pins 0..3 are configured as outputs")
        self.io.set_bitState(int(pin_number), int(state))

    def set_multiple_pins(self, states):
        """
        Set the output state of the port at one time.
        :param state: The state to set the port (8-bit number where bit 0 represents pin 0...etc)
        """
        self.io.set_portState(int(states))

    def get_pin(self, pin_number):
        """
        Get the state of a pin.
        :param pin_number: The pin number to get.
        :return: state: The state of the pin (0 or 1)
        """
        if self.keyfob is False:
            if int(pin_number) < 4 or int(pin_number) > 7:
                raise Exception("Only pins 4..7 are configured as inputs")
        return self.io.get_bitState(int(pin_number))


if __name__ == '__main__':
    serial_number = "MAXIIO02-D7FD1"
    yocto = YoctoDio(serial_number,True)

    print(yocto.get_pin(5))
    yocto.set_pin(2,0)
    print(yocto.get_pin(3))