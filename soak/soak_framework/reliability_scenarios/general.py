import yaml
from time import sleep
import threading
import sys
from binascii import hexlify
import imp
import os


def get_test_case_settings(version):
    setting_file = None

    if os.path.exists("./settings.yml"):
        setting_file = "settings.yml"
    elif os.path.exists("./reliability_scenarios/settings.yml"):
        setting_file = "./reliability_scenarios/settings.yml"

    if setting_file:
        with open(setting_file, 'r') as ymlfile:
            cfg = yaml.load(ymlfile)
        if version.upper() == 'VP':
            return cfg['VP']
        elif version.upper() == 'PVP':
            return cfg['PVP']
        elif version.upper() == 'AP':
            return cfg['AP']
        else:
            print 'Invalid milestone'
            return None
    else:
        print ("settings file not found")
        return None


def signal_handler(signal, frame):
    print('Ctrl+C, exiting...')
    global stop_device_test
    stop_device_test = True
    sleep(3)
    exit(1)

# signal.signal(signal.SIGINT, signal_handler)
