import random
import threading
from binascii import hexlify
from time import sleep

import cantools

import pcanoperations as pcan


class ReliabilityTester:
    def __init__(self, path):
        self.pcan_bus = pcan.PCANKeywords()
        self.pcan_bus.pcan_create('usbbus1', '500k', 'message_filter')

        # self.body2 = cantools.database.load_file('DBC/Body2.dbc')
        # self.thermal = cantools.database.load_file('DBC/Thermal.dbc')
        # self.edrive = cantools.database.load_file('DBC/eDrive.dbc')

        self.body1 = cantools.database.load_file(path+'Body.dbc')
        self.chassis1 = cantools.database.load_file(path+'Chassis1.dbc')
        self.chassis2 = cantools.database.load_file(path+'Chassis2.dbc')

    def select_dbc(self, domain):
        """
        Select correct DBC file based on given domain.

        :param domain: String representing the name of a valid domain (ie. body1, BODY2, ChasSiS2, etc.)

        :return: Appropriate database loaded for PCAN message execution
        """
        if domain.upper() == 'BODY1':
            return self.body1
        elif domain.upper() == 'BODY2':
            return self.body2
        elif domain.upper() == 'CHASSIS1':
            return self.chassis1
        elif domain.upper() == 'CHASSIS2':
            return self.chassis2
        elif domain.upper() == 'THERMAL':
            return self.thermal
        elif domain.upper() == 'EDRIVE':
            return self.edrive
        else:
            print "Not a valid domain"
            return None

    def get_pcan_message(self, domain, message_name, **kwargs):
        """
        Generates PCAN message with given values to send to bus.

        :param domain: String representing domain of message.
        :param message_name: Name of the desired message as it appears in the DBC files.
        :param kwargs: A dictionary of singal name to set value (ie. {'turnIndicator_Lt_FBCM': 1}

        :return: Encoded PCAN message, ready to send.
        """
        can_msg_tmpl = {}
        dbc = self.select_dbc(domain)
        can_msg = dbc.get_message_by_name(message_name)
        for sig in can_msg.signals:
            try:
                choice_key = sig.choices.keys()[-1]
                if sig.unit == '-' and sig.minimum < choice_key < sig.maximum:
                    can_msg_tmpl[sig.name] = choice_key
                else:
                    if sig.minimum is None:
                        raise ValueError
                    can_msg_tmpl[sig.name] = sig.minimum
            except Exception:
                if not sig.minimum:
                    if isinstance(sig.scale, float):
                        can_msg_tmpl[sig.name] = 0.0
                    else:
                        can_msg_tmpl[sig.name] = 0
                else:
                    can_msg_tmpl[sig.name] = sig.minimum
        for key, val in kwargs.items():
            target_type = type(can_msg_tmpl[key])
            can_msg_tmpl[key] = target_type(val)
        encoded_can_message = hexlify(dbc.encode_message(message_name, can_msg_tmpl))
        arb_id = hex(can_msg.frame_id)[2:].upper()
        message = ['0x' + arb_id, "PCAN_MESSAGE_STANDARD", str(len(encoded_can_message) / 2)]
        data = ["0x" + encoded_can_message[i:i + 2] for i in range(0, len(encoded_can_message), 2)]
        message.extend(data)
        return message

    def multithread(self, function_dict):  # {function_name: (arg1, arg2, agr3,)}
        """
        Multithread a set of given functions with their parameters. Avoid multithreading with the same signals/functions.

        :param function_dict: Dictionary of function name to arguments (ie. {'loop_speedometer': (0, 360, 1, 0.1,)

        :return: No return, just starts threads.
        """
        threads = []
        for func, vals in function_dict.iteritems():
            print 'Adding %s thread' % func
            t = threading.Thread(target=func, args=vals)
            threads.append(t)
        for t in threads:
            print 'Starting Thread'
            t.daemon = True
            t.start()
        for t in threads:
            t.join()
        print 'All Threads Joined.'

    def vehicle_power_management_on(self):
        """
        Set VPH to ON mode.

        :return: No return, just turns VPH to ON.
        """
        # try:
        #     kf.keyfob_button("unlock")
        # except AttributeError:
        #     return "Yoctopuce not connected."
        self.set_front_doors(1)
        sleep(3)
        self.set_hv_enable(1)
        sleep(3)
        self.set_gear('Drive')
        sleep(3)
        self.set_front_doors(0)
        sleep(3)
        print 'Turn Car On'

    def set_hv_enable(self, on_off):
        """
        Enable or Disable High Voltage Status.

        :param on_off: Integer (1 or 0) to set HV status on or off.

        :return: No return, just sends the message.
        """
        data = {}
        if on_off:
            data['highVoltageStatus_CDI'] = 4
        else:
            data['highVoltageStatus_CDI'] = 1
        hv_msg = self.get_pcan_message('Chassis2', 'CDI_HVsystemActiveStatus', **data)
        self.pcan_bus.pcan_write(hv_msg)

    def set_gear(self, gear):
        """
        Set vehicle gear

        :param gear: String or integer input to select vehicle gear. (* Not handled)
        *0 = Initializing
        1 = Park
        2 = Reverse
        3 = Neutral
        4 = Drive
        *5,6 = Reserved
        *7 =  Error


        :return: No return, just sends the message.
        """
        data = {}
        if gear.upper() == 'PARK' or gear == 1:
            data['powerTrainActualGearStatus_CDI'] = 1
        elif gear.upper() == 'REVERSE' or gear == 2:
            data['powerTrainActualGearStatus_CDI'] = 2
        elif gear.upper() == 'NEUTRAL' or gear == 3:
            data['powerTrainActualGearStatus_CDI'] = 3
        elif gear.upper() == 'DRIVE' or gear == 4:
            data['powerTrainActualGearStatus_CDI'] = 4
        else:
            print 'Invalid selection, choose from Park, Reverse, Neutral, Drive.'
        gear_msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainDriveStatus', **data)
        self.pcan_bus.pcan_write(gear_msg)

    def set_turn_signal(self, side, on_off):
        """
        Activates turn signal for left or right side.

        :param side: STRING representing which side to manipulate. (ie. 'Left' or 'Right')
        :param on_off: INTEGER to turn ON (1) or OFF (0).

        :return: No return, just sends the message.
        """
        data = {}
        if side.upper() == 'LEFT' and on_off:
            data['turnIndicatorLt_FBCM'] = 1
        elif side.upper() == 'LEFT' and not on_off:
            data['turnIndicatorLt_FBCM'] = 0
        elif side.upper() == 'RIGHT' and on_off:
            data['turnIndicatorRt_FBCM'] = 1
        elif side.upper() == 'RIGHT' and not on_off:
            data['turnIndicatorRt_FBCM'] = 0
        else:
            print "Invalid choice, Choose Left or Right and 1 or 0"
        speedometer_msg = self.get_pcan_message('Body1', 'FBCM_VehInfo1', **data)
        self.pcan_bus.pcan_write(speedometer_msg)

    def flashing_left_turn_signal(self, count):
        """
        Flash LEFT turn signal with standard delay of 400ms on, 400ms off.

        :param count: INTEGER representing how many times to have the turn signal flash.

        :return: No return, just sends the message.
        """
        for i in xrange(count):
            self.set_turn_signal('left', 1)
            sleep(0.4)
            self.set_turn_signal('left', 0)
            sleep(0.4)

    def flashing_right_turn_signal(self, count):
        """
        Flash RIGHT turn signal with standard delay of 400ms on, 400ms off.

        :param count: INTEGER representing how many times to have the turn signal flash.

        :return: No return, just sends the message.
        """
        for i in xrange(count):
            self.set_turn_signal('right', 1)
            sleep(0.4)
            self.set_turn_signal('right', 0)
            sleep(0.4)

    def set_headlights(self, h_or_l, light_val):
        """
        Turn on or off the high or low beam headlights.

        :param h_or_l: STRING to select HIGH or LOW beam headlight.
        :param light_val: INTEGER to turn selected headlight ON (1) or OFF (0).

        :return: No return, just sends the message.
        """
        sig = ''
        if h_or_l.upper() == "H" or h_or_l.upper() == "HIGH":
            sig = 'highBeamOn_FBCM'
        elif h_or_l.upper() == "L" or h_or_l.upper() == "LOW":
            sig = 'lowBeamOn_FBCM'
        data = {sig: light_val}
        speedometer_msg = self.get_pcan_message('Body1', 'FBCM_VehInfo1', **data)
        self.pcan_bus.pcan_write(speedometer_msg)

    def set_speedometer(self, km_val):
        """
        Set speedometer value.

        :param km_val: Pass speedometer value in KPH, gets converted to M/S.

        :return: No return, just sends the message.
        """
        data = {'vehSpeed_ESP': min(km_val * 0.277777, 100)}
        speedometer_msg = self.get_pcan_message('Chassis2', 'ESP_iBoostData1', **data)
        self.pcan_bus.pcan_write(speedometer_msg)

    def set_odometer(self, km_val):
        """
        Set odometer value.

        :param km_val: Pass odometer value in KM.

        :return: No return, just sends the message.
        """
        data = {'powerTrainHMIOdometerInfo_CDI': km_val}
        odo_msg = self.get_pcan_message('Chassis2', 'CDI_OdometerInfo', **data)
        self.pcan_bus.pcan_write(odo_msg)

    def set_range(self, range):
        """
        Set battery range value.

        :param range: Pass range value in KM.

        :return: No return, just sends the message.
        """
        data = {'powerTrainHMIAvaDrvRange_CDI': range}
        range_msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHVbattStatus1', **data)
        self.pcan_bus.pcan_write(range_msg)

    def set_battery(self, battery_percent):
        """
        Set battery charge percentage icon.

        :param battery_percent: Pass battery life INTEGER value in factor of 100.
        (ie. empty is 0, quarter is 25, half full is 50, full is 100)

        :return: No return, just sends the message.
        """
        data = {'powerTrainHMIHVBatSoc_CDI': battery_percent}
        range_msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHVbattStatus1', **data)
        self.pcan_bus.pcan_write(range_msg)

    def set_range_and_battery(self, range):
        """
        Combination of battery and range functions to tie the two elements together.

        :param range: Specify range in KM, it will use this value as a percentage of 1000 to set the battery icon as well.

        :return: No return, just sends the message.
        """
        data = {'powerTrainHMIAvaDrvRange_CDI': range, 'powerTrainHMIHVBatSoc_CDI': float(range) / 10}
        range_msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHVbattStatus1', **data)
        self.pcan_bus.pcan_write(range_msg)

    def set_front_doors(self, door_val):
        """
        Sets front door ajar values based on integer passed.

        :param door_val: INTEGER value representing state of doors.
        0: Driver Door CLOSED , Passenger Door CLOSED
        1: Driver Door OPEN, Passenger Door CLOSED
        2: Driver Door CLOSED, Passenger Door OPEN
        3: Driver Door OPEN, Passenger Door OPEN

        :return: No return, just sends the message.
        """
        data = {}
        if door_val == 0:
            data = {'doorAjarD_FBCM': 0, 'doorAjarP_FBCM': 0}
        elif door_val == 1:
            data = {'doorAjarD_FBCM': 1, 'doorAjarP_FBCM': 0}
        elif door_val == 2:
            data = {'doorAjarD_FBCM': 0, 'doorAjarP_FBCM': 1}
        elif door_val == 3:
            data = {'doorAjarD_FBCM': 1, 'doorAjarP_FBCM': 1}
        else:
            print "Invalid selection, choose from 0-3."
        front_door_msg = self.get_pcan_message('Body1', 'FBCM_VehInfo1', **data)
        self.pcan_bus.pcan_write(front_door_msg)

    def set_rear_doors(self, door_val):
        """
        Sets rear door ajar values based on integer passed.

        :param door_val: INTEGER value representing state of doors.
        0: Rear Driver Door CLOSED , Rear Passenger Door CLOSED
        1: Rear Driver Door OPEN, Rear Passenger Door CLOSED
        2: Rear Driver Door CLOSED, Rear Passenger Door OPEN
        3: Rear Driver Door OPEN, Rear Passenger Door OPEN

        :return: No return, just sends the message.
        """
        data = {}
        if door_val == 0:
            data = {'doorAjarRR_RBCM': 0, 'doorAjarRL_RBCM': 0}
        elif door_val == 1:
            data = {'doorAjarRR_RBCM': 1, 'doorAjarRL_RBCM': 0}
        elif door_val == 2:
            data = {'doorAjarRR_RBCM': 0, 'doorAjarRL_RBCM': 1}
        elif door_val == 3:
            data = {'doorAjarRR_RBCM': 1, 'doorAjarRL_RBCM': 1}
        else:
            print "Invalid selection, choose from 0-3."
        rear_door_msg = self.get_pcan_message('Body1', 'RBCM_VehInfo1', **data)
        self.pcan_bus.pcan_write(rear_door_msg)

    def set_seat_belts(self, seat_vals):  # TODO: Add seat belt logic for other seats later
        """
        Sets the state of seat belts within the vehicle.

        :param seat_vals: INTEGER value representing state of seat belts for all seats.

        :return: No return, just sends the message.
        """
        data = {'drivSeatBeltBcklState_ACM': seat_vals}
        msg = self.get_pcan_message('Body1', 'ACM_AirbagInfo1', **data)
        self.pcan_bus.pcan_write(msg)

    def set_abs_warning(self, abs_val):
        """
        Sets the state of ABS warning.

        :param abs_val: INTEGER value, ON (1) or OFF (0).

        :return: No return, just sends the message.
        """
        data = {'actvABS_ESP': abs_val}
        msg = self.get_pcan_message('Chassis2', 'ESP_iBoostData3', **data)
        self.pcan_bus.pcan_write(msg)

    def set_brake_failure(self, brake_val):
        """
        Sets state of brake system failure warning.

        :param brake_val: INTEGER value, ON (1) or OFF (0).

        :return: No return, just sends the message.
        """
        data = {'brakeSysWarn_ESP': brake_val}
        msg = self.get_pcan_message('Chassis1', 'ESP_BrakeSystemStatus1', **data)
        self.pcan_bus.pcan_write(msg)

    def set_parking_brake(self, brake_val):
        """
        Sets various states of parking brake.

        :param brake_val: INTEGER value to set on of the following 8 states, (1 and 2 primarily used).
        0 = Available Unknown
        1= Available Applied
        2= Available Released
        3= Request in Progress
        4= Unavailable Unknown
        5= Unavailable Applied
        6= Unavilable Applied
        7= Dynamic Apply

        :return: No return, just sends the message.
        """
        data = {'parkBrakeStatus_ESP': brake_val}
        msg = self.get_pcan_message('Chassis1', 'ESP_BrakeSystemStatus1', **data)
        self.pcan_bus.pcan_write(msg)

    def set_electric_motor_failure(self, motor_val):
        """
        Sets value for electric motor failure warning.

        :param motor_val: INTEGER value to set warning ON (1) or OFF (0).

        :return: No return, just sends the message.
        """
        data = {'powerTrainHMIDriveMotOvrTemp_CDI': motor_val}
        msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
        self.pcan_bus.pcan_write(msg)

    def set_hazard(self, on_off):
        """
        Sets value for hazard signal.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        data = {'hazardModeStatus_FBCM': on_off}
        msg = self.get_pcan_message('Body1', 'FBCM_VehInfo1', **data)
        self.pcan_bus.pcan_write(msg)

    def set_fog_light(self, on_off):
        """
        Sets value for fog lights.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        data = {'fogLampsOnRR_RBCM': on_off}
        msg = self.get_pcan_message('Body1', 'RBCM_VehInfo1', **data)
        self.pcan_bus.pcan_write(msg)

    def set_coolant_temp_over(self, on_off):
        """
        Sets value for coolant temp warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        data = {'powTrainHMIHVBatCoolOvrTemp_CDI': on_off}
        msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
        self.pcan_bus.pcan_write(msg)

    def set_battery_failure(self, on_off):
        """
        Sets value for battery failure warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        data = {'powTrainHMIHVBatCoolOvrTemp_CDI': on_off}
        msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
        self.pcan_bus.pcan_write(msg)

    def set_charge_status(self, charge_val):
        """
        Sets value for various states of charging.

        :param charge_val: INTEGER representing state of vehicle charge.
        0 = Initializing
        1 = Off Plug
        2 = Plug Detected
        3 = Charging Standby
        4 = AC Charging
        5 = DC Charging
        6 = Charging Completed
        7 = Fault

        :return: No return, just sends the message.
        """
        data = {'chrgSystemStatus_CDI': charge_val}
        msg = self.get_pcan_message('Chassis2', 'CDI_PlugInChrgInfo1', **data)
        self.pcan_bus.pcan_write(msg)

    def set_is_charging(self, on_off):
        """
        Sets value for charging indicator.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        data = {'highVoltageActPlugInCharging_CDI': on_off}
        msg = self.get_pcan_message('Chassis2', 'CDI_HVsystemActiveStatus', **data)
        self.pcan_bus.pcan_write(msg)

    def set_airbag_deployed(self, airbag_val):
        """
        Set value for airbag deployed incident.

        :param airbag_val: INTEGER value representing state of airbag deployment.
        0 = No Deployment on Front Airbags
        1 = Only Driver Airbag Deployed
        2 = Only Passenger Airbag Deployed
        3 = Driver and Passenger Airbags Deployed

        :return: No return, just sends the message.
        """
        data = {'crashAirbagStatus_ACM': airbag_val}
        msg = self.get_pcan_message('Chassis2', 'ACM_AirbagInfo4', **data)
        self.pcan_bus.pcan_write(msg)

    def set_airbag_failure(self, airbag_val):
        """
        Sets value for airbag system failure warnings.

        :param airbag_val:  INTEGER value representing state of airbag failure.
        0 = RIL Request Off
        1 = Reserved
        2 = RIL Request Plant Mode
        3 = RIL Requested ON

        :return: No return, just sends the message.
        """
        data = {'requestRILStatus_ACM': airbag_val}
        msg = self.get_pcan_message('Chassis2', 'ACM_AirbagInfo2', **data)
        self.pcan_bus.pcan_write(msg)

    def set_stability_control(self, dsc_val):
        """
        Sets value for DSC system.

        :param dsc_val: INTEGER value for different states.
        0 = Available
        1 = Active
        2 = Unavailable

        :return: No return, just sends the message.
        """
        data = {'statusDSC_ESP': dsc_val}
        msg = self.get_pcan_message('Chassis1', 'ESP_BrakeSystemStatus1', **data)
        self.pcan_bus.pcan_write(msg)

    def set_steering_wheel_failure(self, on_off):
        """
        Sets value for steering wheel failure warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends PCAN message.
        """
        data = {'primSysFault_EPAS1': on_off}
        msg = self.get_pcan_message('Chassis1', 'EPAS1_PwrSteeringInfo1', **data)
        self.pcan_bus.pcan_write(msg)
        data = {'secSysFault_EPAS2': on_off}
        msg = self.get_pcan_message('Chassis2', 'EPAS2_PwrSteeringInfo2', **data)
        self.pcan_bus.pcan_write(msg)

    def set_vehicle_ready(self, ready_val):
        """
        Sets value for READY signal.

        :param ready_val: INTEGER for different vehicle ready states.
        0 = Initializing
        1 = Drive Inhibited
        2 = Drive Ready
        3 = Error

        :return: No return, just sends the message.
        """
        data = {'powerTrainDriveReadiness_CDI': ready_val}
        msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainDriveStatus', **data)
        self.pcan_bus.pcan_write(msg)

    def set_limp_mode(self, on_off):
        """
        Sets value for vehicle limp mode warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        data = {'powerTrainHMILimpHome_CDI': on_off}
        msg = self.get_pcan_message('Chassis2', 'CDI_PwrtrainHMIsignals', **data)
        self.pcan_bus.pcan_write(msg)

    def set_tpms(self, wheel, tpms_val):
        """
        Sets value for each TPMS warning state.

        :param wheel: INTEGER representing wheel to set.
        1 = FL wheel
        2 = FR wheel
        3 = RL wheel
        4 = RR wheel
        :param tpms_val: INTEGER value representing following TPMS states.
        0 = Normal
        1 = Low
        2 = Reserved
        3 = High
        4 = Not Available
        5, 6, 7 = Reserved

        :return: No return, just sends the message.
        """
        data = {}
        if wheel == 1:
            data = {'singleTyrePressStateFL_TPMS': tpms_val}
        elif wheel == 2:
            data = {'singleTyrePressStateFR_TPMS': tpms_val}
        elif wheel == 3:
            data = {'singleTyrePressStateRL_TPMS': tpms_val}
        elif wheel == 4:
            data = {'singleTyrePressStateRR_TPMS': tpms_val}
        else:
            print 'Invalid wheel selection'
        msg = self.get_pcan_message('Body1', 'TPMS_StatusTPMS', **data)
        self.pcan_bus.pcan_write(msg)

    def set_washer_fluid_low(self, on_off):
        """
        Sets value for window washer fluid low warning.

        :param on_off: INTEGER value for ON (1) or OFF (0)

        :return: No return, just sends the message.
        """
        data = {'washerFluidLow_FBCM': on_off}
        msg = self.get_pcan_message('Body1', 'FBCM_DiagInfoInputs', **data)
        self.pcan_bus.pcan_write(msg)

    def loop_speedometer(self, min_val=0, max_val=360, step=1, delay=0.1):
        """
        Uses set_speedometer function to increment and decrement speedometer value indefinitely.

        :param min_val: Minimum speedometer value to display (Default: 0)
        :param max_val: Maximum speedometer value to display (Default: 360)
        :param step: Gap between two message values (Default: 1)
        :param delay: How often to send next PCAN message (Default: 0.1)

        :return: No return, just sends the message.
        """
        while True:
            for i in xrange(min_val, max_val, step):
                self.set_speedometer(i)
                sleep(delay)
            for i in xrange(max_val, min_val, -step):
                self.set_speedometer(i)
                sleep(delay)

    def loop_odometer(self, min_val=0, max_val=1000000, step=1000, delay=0.1):
        """
        Uses set_odometer function to increment and decrement odometer value indefinitely.

        :param min_val: Minimum odometer value to display (Default: 0)
        :param max_val: Maximum odometer value to display (Default: 1000000)
        :param step: Gap between two message values (Default: 1000)
        :param delay: How often to send next PCAN message (Default: 0.1)

        :return: No return, just sends the message
        """
        while True:
            for i in xrange(min_val, max_val, step):
                self.set_odometer(i)
                sleep(delay)
            for i in xrange(max_val, min_val, -step):
                self.set_odometer(i)
                sleep(delay)

    def loop_range_and_battery(self, min_val=0, max_val=1000, step=10, delay=0.1):
        """
        Uses set_range_and_battery function to increment and decrement range value and battery icon indefinitely.

        :param min_val: Minimum range value to display (Default: 0)
        :param max_val: Maximum range value to display (Default: 1000)
        :param step: Gap between two message values (Default: 10)
        :param delay: How often to send next PCAN message (Default: 0.1)

        :return: No return, just sends the message
        """
        while True:
            for i in xrange(min_val, max_val, step):
                self.set_range_and_battery(i)
                sleep(delay)
            for i in xrange(max_val, min_val, -step):
                self.set_range_and_battery(i)
                sleep(delay)

    def random_warnings(self, signal_list):
        func_call = random.choice(signal_list)
        if func_call == self.set_headlights:
            light = random.choice(['H', 'L'])
            self.set_headlights(light, 1)
            sleep(3)
            self.set_headlights(light, 0)
            sleep(1)
        elif func_call == self.flashing_left_turn_signal or func_call == self.flashing_right_turn_signal:
            func_call(5)
            sleep(2)
        elif func_call == self.set_tpms:
            self.set_tpms(1, 1)
            sleep(3)
            self.set_tpms(1, 0)
        else:
            func_call(1)
            sleep(3)
            func_call(0)
            sleep(1)

    def loop_warnings(self):
        signal_list = [self.set_stability_control, self.set_steering_wheel_failure, self.set_parking_brake,
                       self.set_washer_fluid_low, self.set_limp_mode, self.set_coolant_temp_over,
                       self.set_electric_motor_failure, self.set_hazard, self.set_brake_failure, self.set_fog_light,
                       self.set_headlights, self.set_abs_warning, self.flashing_left_turn_signal,
                       self.flashing_right_turn_signal, self.set_tpms]
        while True:
            self.random_warnings(signal_list)

    # def bt_call(self, duration, contact):
    #     while duration:
    #         call_robot_function('BT', contact)
    #
    # def navigate(self, start, end):
    #     while duration:
    #         call_robot_function('maps', contact)

if __name__ == '__main__':
    test = ReliabilityTester('DBC/F39.04/')
    # print "Gear"
    # test.set_gear('park')
    # test.set_gear('reverse')
    # test.set_gear('neutral')
    # test.set_gear('drive')
    # sleep(1)
    # print "Front Door"
    # test.set_front_doors(3)
    # sleep(1)
    # print "HV Enable"
    # test.set_hv_enable(1)
    # sleep(1)
    # print "Rear Door"
    # test.set_rear_doors(2)
    # sleep(1)
    # print "Speed"
    # test.set_speedometer(5)
    # sleep(1)
    # print " Headlights"
    # test.set_headlights('h', 1)
    # sleep(2)
    # test.set_headlights('l', 1)
    # sleep(1)
    # print "Odometer"
    # test.set_odometer(35600)
    # sleep(1)
    # print "Range and Battery"
    # test.set_range_and_battery(478)
    # sleep(1)
    # print "ABS"
    # test.set_abs_warning(0)
    # sleep(1)
    # print "Battery Fail"
    # test.set_battery_failure(0)
    # sleep(1)
    # print "Brake Fail"
    # test.set_brake_failure(0)
    # sleep(1)
    # print "Airbag Deploy"
    # test.set_airbag_deployed(1)
    # sleep(1)
    # print "Airbag Fail"
    # test.set_airbag_failure(1)
    # sleep(1)
    # print "Charge Status"
    # test.set_charge_status(2)
    # sleep(1)
    # print "Is Charging"
    # test.set_is_charging(1)
    # sleep(1)
    # print "Hazards"
    # test.set_hazard(1)
    # sleep(1)
    # print "Motor Fail"
    # test.set_electric_motor_failure(1)
    # sleep(1)
    # print "TPMS"
    # test.set_tpms(1, 1)
    # sleep(2)
    # test.set_tpms(1, 0)
    # sleep(2)
    # test.set_tpms(2, 1)
    # sleep(2)
    # test.set_tpms(2, 0)
    # sleep(2)
    # test.set_tpms(3, 1)
    # sleep(2)
    # test.set_tpms(3, 0)
    # sleep(2)
    # test.set_tpms(4, 1)
    # sleep(2)
    # test.set_tpms(4, 0)
    # sleep(2)
    # sleep(1)
    # print "Seat Belts"
    # test.set_seat_belts(0)
    # sleep(1)
    # print "Limp Mode"
    # test.set_limp_mode(1)
    # sleep(1)
    # print "Coolant Temp"
    # test.set_coolant_temp_over(1)
    # sleep(1)
    # print "Parking Brake"
    # test.set_parking_brake(2)
    # sleep(1)
    # print "Washer Fluid"
    # test.set_washer_fluid_low(1)
    # sleep(1)
    # print "READY"
    # test.set_vehicle_ready(2)
    # sleep(1)
    # print "Steering Fail"
    # test.set_steering_wheel_failure(1)
    # sleep(1)
    # print "DSC"
    # test.set_stability_control(1)
    # sleep(1)
    #
    # print "Multithread"
    # funcs = {test.loop_odometer: (0,1000, 1, 0.1,), test.loop_speedometer: (0,360,1,0.1,)}
    # test.multithread(funcs)
