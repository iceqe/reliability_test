# Copyright 2018-     BYTON
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Author: Sheng Zhao <sheng.zhao@byton.com>

from robot.api import logger

from PCAN import PCAN
from PCANBasic import *


class PCANKeywords(object):
    BUS = {'usbbus1': PCAN_USBBUS1, 'usbbus2': PCAN_USBBUS2, 'usbbus3': PCAN_USBBUS3, 'usbbus4': PCAN_USBBUS4,
           'usbbus5': PCAN_USBBUS5, 'usbbus6': PCAN_USBBUS6, 'usbbus7': PCAN_USBBUS7, 'usbbus8': PCAN_USBBUS8,
           'usbbus9': PCAN_USBBUS9, 'usbbus10': PCAN_USBBUS10, 'usbbus11': PCAN_USBBUS11, 'usbbus12': PCAN_USBBUS12,
           'usbbus13': PCAN_USBBUS13, 'usbbus14': PCAN_USBBUS14, 'usbbus15': PCAN_USBBUS15,
           'usbbus16': PCAN_USBBUS16}

    BAUD = {'5k': PCAN_BAUD_5K, '10k': PCAN_BAUD_10K, '20k': PCAN_BAUD_20K, '33k': PCAN_BAUD_33K, '47k': PCAN_BAUD_47K,
            '50k': PCAN_BAUD_50K, '83k': PCAN_BAUD_83K, '95k': PCAN_BAUD_95K, '100k': PCAN_BAUD_100K,
            '125k': PCAN_BAUD_125K,
            '250k': PCAN_BAUD_250K, '500k': PCAN_BAUD_500K, '800k': PCAN_BAUD_800K, '1m': PCAN_BAUD_1M}

    PARAMETERS = {'message_filter': PCAN_MESSAGE_FILTER}

    def __init__(self):
        self._pcan = None
        self._serial = None
        self.message = None

    def pcan_create(self, bus, baud, parameters=None):
        self._pcan = PCAN(PCANKeywords.BUS[bus.lower()], PCANKeywords.BAUD[baud.lower()],
                          PCANKeywords.PARAMETERS[parameters.lower()])
        self._pcan.init_pcan_usbbus()
        return self._pcan

    def pcan_close(self):
        if self._pcan:
            self._pcan.uninit_usbbus()
        else:
            error_msg = "PCAN has not been initialized, no need to close."
            logger.error(error_msg)
            raise Exception(error_msg)

    def pcan_write(self, msg):
        if self._pcan:
            print(msg)
            self._pcan.write_request_messages(msg)
        else:
            error_msg = "PCAN has not been initialized, should be initialized firstly."
            logger.error(error_msg)
            raise Exception(error_msg)

    def pcan_read(self):
        if self._pcan:
            return self._pcan.read_respond_messages()
        else:
            error_msg = "PCAN has not been initialized, should be initialized firstly."
            logger.error(error_msg)
            raise Exception(error_msg)

    def pcan_reset(self):
        if self._pcan:
            self._pcan.reset_pcan()
        else:
            error_msg = "PCAN has not been initialized, should be initialized firstly."
            logger.error(error_msg)
            raise Exception(error_msg)

    def pcan_getstatus(self):
        if self._pcan:
            return self._pcan.get_canstatus()
        else:
            error_msg = "PCAN has not been initialized, should be initialized firstly."
            logger.error(error_msg)
            raise Exception(error_msg)

    def send_msg_in_patch(pcan_kw, msggrp, waittime):
        for msg in msggrp:
            pcan_kw.pcan_write(msg)
            sleep(waittime)


if __name__ == '__main__':
    from time import sleep, ctime
    import threading

    def send_msg_in_patch(pcan_kw, msggrp, waittime):
        for msg in msggrp:
            pcan_kw.pcan_write(msg)
            sleep(waittime)

    pcan_kw_bus01 = PCANKeywords()
    pcan_kw_bus02 = PCANKeywords()
    pcan_kw_bus03 = PCANKeywords()

    msg1 = ['0x500', "PCAN_MESSAGE_STANDARD", "8", '0x00', '0x00', '0x00', '0x00', '0x10', '0x00', '0x00', '0x00']
    msg2 = ["0x500", "PCAN_MESSAGE_STANDARD", "8", "0x08", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    msg3 = ["0x500", "PCAN_MESSAGE_STANDARD", "8", "0x02", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    msg4 = ["0x508", "PCAN_MESSAGE_STANDARD", "8", "0x00", "0x02", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    msg5 = ["0x508", "PCAN_MESSAGE_STANDARD", "8", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    msg6 = ["0x500", "PCAN_MESSAGE_STANDARD", "8", "0x00", "0x80", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    msg7 = ["0x500", "PCAN_MESSAGE_STANDARD", "8", "0x00", "0x40", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    msg8 = ["0x500", "PCAN_MESSAGE_STANDARD", "8", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    msg9 = ["0x500", "PCAN_MESSAGE_STANDARD", "8", "0x00", "0xc0", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    msg10 = ["0x500", "PCAN_MESSAGE_STANDARD", "8", "0x00", "0x80", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    
    chas2_msg1 = ["0x24", "PCAN_MESSAGE_STANDARD", "6", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"]
    chas2_msg2 = ["0x24", "PCAN_MESSAGE_STANDARD", "6", "0x20", "0x00", "0x00", "0x00", "0x00", "0x00"]
    chas2_msg3 = ["0x24", "PCAN_MESSAGE_STANDARD", "6", "0x40", "0x00", "0x00", "0x00", "0x00", "0x00"]
    chas2_msg4 = ["0x24", "PCAN_MESSAGE_STANDARD", "6", "0x60", "0x00", "0x00", "0x00", "0x00", "0x00"]
    chas2_msg5 = ["0x24", "PCAN_MESSAGE_STANDARD", "6", "0x80", "0x00", "0x00", "0x00", "0x00", "0x00"]
    chas2_msg6 = ["0x20F", "PCAN_MESSAGE_STANDARD", "8", "0x00", "0x00", "0x00", "0x63", "0x00", "0x00", "0x00", "0x00"]
    chas2_msg7 = ["0x407", "PCAN_MESSAGE_STANDARD", "5", "0x00", "0x00", "0x00", "0x00", "0x00"]
    chas2_msg8 = ["0x602", "PCAN_MESSAGE_STANDARD", "3", "0x00", "0x00", "0x32"]
    chas2_msg9 = ["0x38B", "PCAN_MESSAGE_STANDARD", "4", "0x00", "0x00", "0x20", "0x00"]
    chas2_msg10 = ["0x38B", "PCAN_MESSAGE_STANDARD", "4", "0x00", "0x00", "0x46", "0x00"]
    
    chas1_msg1 = ["0x21A", "PCAN_MESSAGE_STANDARD", "8", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00", "0x01"]
    chas1_msg2 = ["0x20C", "PCAN_MESSAGE_STANDARD", "6", "0x31", "0x00", "0x10", "0x20", "0x00", "0x00"]
    chas1_msg3 = ["0x20D", "PCAN_MESSAGE_STANDARD", "3", "0x10", "0x00", "0x00"]
    chas1_msg4 = ["0x24", "PCAN_MESSAGE_STANDARD", "6", "0x20", "0x00", "0x00", "0x00", "0x00", "0x00"]
    
    body1_msgs = [msg1, msg2, msg3, msg4, msg5, msg6, msg7, msg8, msg9, msg10]
    chas1_msgs = [chas1_msg1, chas1_msg2, chas1_msg3]
    chas2_msgs = [chas2_msg1, chas2_msg2, chas2_msg3, chas2_msg4, chas2_msg5, chas2_msg6, chas2_msg7, chas2_msg8, chas2_msg9, chas2_msg10]
    
    pcan_kw_bus01.pcan_create('usbbus1', '500k', 'message_filter')
    

    while True:
        threads = []
        t1 = threading.Thread(target=send_msg_in_patch, args=(pcan_kw_bus01, body1_msgs, 0.1,))
        threads.append(t1)
    
#    while True:
        for t in threads:
            t.setDaemon(True)
            t.start()
    
            t.join()

            print "all over %s" % ctime()
