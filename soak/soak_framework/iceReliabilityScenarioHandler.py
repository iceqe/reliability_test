
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__))+"/reliability_scenarios")

print(__file__)
print("abs path:")
print(os.path.dirname(os.path.abspath(__file__)))

import iceSoakSimulation
import basic_scenario as basic_s
import extended_basic_scenario1 as ex_basic_s1
import extended_basic_scenario2 as ex_basic_s2


class IceReliabilityScenarioHandler(object):
    def __init__(self, reliability_scenario_info, logger):

        self.reliability_scenario_info = reliability_scenario_info
        self.logger = logger

        # self.test_type = reliability_scenario_info["test_type"]
        # self.run_soak_simulation = reliability_scenario_info["soak_simulation"]
        # if self.run_soak_simulation:
        #     self.obj_soak_simulation = iceSoakSimulation.IceSoakSimulation(logger)
        self.obj_basic_scenario = basic_s
        self.basic_scenario_running = False
        self.obj_basic_scenario_1 = ex_basic_s1
        self.basic_scenario1_running = False
        self.obj_basic_scenario_2 = ex_basic_s2
        self.basic_scenario2_running = False

    def start_reliability_scenarios(self):

        for tests in self.reliability_scenario_info.values():
            if tests["test_name"] == "basic_scenario" and tests["execute_test"] is True:
                self.obj_basic_scenario.basic_sceanrio(tests["db_version"])
                self.basic_scenario_running = True
                self.logger.info("Reliability Basic Scenario started...")

            if tests["test_name"] == "extended_basic_scenario_1" and tests["execute_test"] is True:
                self.obj_basic_scenario_1.extended_basic_scenario1(tests["db_version"])
                self.basic_scenario1_running = True
                self.logger.info("Reliability Extended Basic Scenario1 started...")

            if tests["test_name"] == "extended_basic_scenario_2" and tests["execute_test"] is True:
                self.obj_basic_scenario_2.extended_basic_scenario2(tests["db_version"])
                self.basic_scenario2_running = True
                self.logger.info("Reliability Extended Basic Scenario2 started...")

    def stop_reliability_scenarios(self):
        if self.basic_scenario_running:
            self.logger.info("Reliability Basic Scenario stopping....")
            self.obj_basic_scenario.stop_basic_scenario()
            self.logger.info("Reliability Basic Scenario stopped.")

        if self.basic_scenario1_running:
            self.logger.info("Reliability Extended Basic Scenario1 stopping.....")
            self.obj_basic_scenario_1.stop_basic_scenario()
            self.logger.info("Reliability Extended Basic Scenario1 Stopped.")

        if self.basic_scenario2_running:
            self.logger.info("Reliability Extended Basic Scenario2 stopping....")
            self.obj_basic_scenario_2.stop_basic_scenario()
            self.logger.info("Reliability Extended Basic Scenario2 stopped")






