#
# Author: Neeraj Kumar Singh
#
# connection to the device to run commands and get results back
# note that this is for general commands only,
# uses ssh over pexpect for the connection
#
# For now: raise exception and set connection status to None when timeout
#
#
# NOT THREADSAFE!!! Do not send commands from different threads.
#
#
import time
import os
import pexpect

# not for public consumption, this is used internally as the core connection
class coreConnection(object):

    # connect at init time
    def __init__(self, remoteAddress, user, password, expectedPrompt, logger = None):

        self.remoteAddress = remoteAddress
        self.user = user
        self.password = password
        self.expectedPrompt = expectedPrompt
        self.logger = logger

        self.prompt = "TASPROMPT:"
        self.child = None

    def log(self, msg):
        if self.logger:
            self.logger.log(msg)
        else:
            print msg


    # caller has to catch and handle all exceptions
    # maybe switch to using regular expect?
    # readline returns newline when get eof instead of an exception.
    def readline(self, timeout = 300):
        if not self.child:
            raise Exception("no connection")
        endTime = time.time() + timeout
        while time.time() < endTime:
            response = self.child.expect(["\r\n", pexpect.TIMEOUT, pexpect.EOF])
            if response == 1:
                break
            if response == 2:
                self.close()
                raise Exception("no connection")

            line = self.child.before.strip()
            return line

        raise Exception("TIMEOUT")


    def interruptCurrentCommand(self):
        if not self.child is None:
            # send control c
            self.child.sendline('\003')
            # do not use our expect wrapper here, may not need to re-connect when interrupt
            time.sleep(3)
            self.child.expect(self.prompt)


    def interruptWithoutPrompt(self):
        if not self.child is None:
            # send control c
            self.child.sendline('\003')


    def close(self):
        self.interruptWithoutPrompt()
        time.sleep(1)
        if not self.child is None:
            self.child.close()
            self.child = None


    def connect(self, timeout=None):
        self.connectSsh(timeout)


    def connectSsh(self, timeout=None):
            # disable display so does not pop up UI if ssh fails
            if os.environ.get("DISPLAY"):
                    del(os.environ["DISPLAY"])

            sshCommand = 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no %s@%s' % (self.user, self.remoteAddress)

            self.child = pexpect.spawn(sshCommand, ignore_sighup=False, timeout=timeout)
            os.environ['COLUMNS'] = "200"
            self.child.setwinsize(200,120)

            response = self.child.expect(['.*assword:', '#', pexpect.EOF])
            if response == 0:
                    # normal case, send password
                    self.child.sendline(self.password)
                    self.child.expect(self.expectedPrompt)
            elif response == 1:
                    # no password needed, maybe ssh key access
                    self.log("[%r]pexpect: no password needed" % self.remoteAddress)
            else:
                    raise Exception("error trying to ssh into device")

            # change prompt to make it more foolproof when looking for prompt back
            self.child.sendline('PS1=%s' % self.prompt)

            # just this time, expect prompt back twice as first one will match our command above
            self.child.expect(self.prompt)
            self.child.expect(self.prompt)
            return True



    def connectWithRetries(self, timeout = None, maxTries=5, waitTimeBetweenTries=4):
        connected = False
        numberOfTries = 0
        while (not connected):
            try:
                self.connectSsh(timeout)
                connected = True
            except Exception as e:
                numberOfTries += 1
                self.log("timeout connecting ssh/pexpect or try %d/%d" % (numberOfTries, maxTries))
                if numberOfTries > maxTries:
                    raise
                time.sleep(waitTimeBetweenTries)


    # wrapper around pexpect expect to handle timeouts
    def expect(self, expectedString, timeout=None):
        if not self.child:
            raise Exception("no connection")

        try:
            if timeout:
                response = self.child.expect([expectedString, pexpect.TIMEOUT, pexpect.EOF], timeout=timeout)
            else:
                response = self.child.expect([expectedString, pexpect.TIMEOUT, pexpect.EOF])

            if response == 1:
                # timeout! what to do?
                # for now, assume timeouts are disconnects. Dangerous as could
                # be a long running command but keep it simple for now
                self.close()
                raise Exception("pexpect timeout")
            if response == 2:
                self.close()
                raise Exception("no connection")

        except Exception as e:
            raise

    # need to wrap in a try block, first figure out what can and should happen
    def sendCommand(self, command):
        if not self.child:
            raise Exception("no connection")

        self.child.sendline(command)

        # eat the echo of the command
        charsExpected = len(command)
        # the terminal wraps long commands into separate lines, so have to check we got it all
        while charsExpected > 0:
            self.expect(".*\r\n")
            received = self.child.after
            charsExpected = charsExpected - len(received) + 2


    def sendCommandWithResult(self, command, timeout=20):
        try:
            self.sendCommand(command)
            self.expect(self.prompt, timeout)
            result = self.child.before.strip()
        except Exception as e :
            result = None
            self.close()
            raise
        return result